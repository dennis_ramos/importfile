﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Server;
using System.IO;
using r = ConXApiDtoModels.DataCreationRequest;
using ImportV2.ApiMapper;
using ConXApiRepository;

namespace ImportTester
{
    class Program
    {
        static void Main(string[] args)
        {
           var filePath = Console.ReadLine();
            if( File.Exists(filePath) )
            {
                ProcessOneFile(filePath).Wait();
            }
            else
            {
                Console.WriteLine("File or Path couldn't be found, press any key to close application");    
            }
            
            Console.ReadKey();
        }

        static async Task ProcessOneFile(string filePath)
        {
            Mapper mapper = new Mapper(filePath, "WELLS", "dramos", "dennis.ramos@e-mergedata.com");
            //bool success = await mapper.ProcessFile();
        }

    }
}
