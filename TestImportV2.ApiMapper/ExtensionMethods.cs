﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportV2.ApiMapper
{
    public static class ExtensionMethods
    {
        public static DateTime? ToDateValue(this string dateString)
        {
            if (string.IsNullOrWhiteSpace(dateString)) return null;

            
            CultureInfo provider = new CultureInfo("en-NZ");

            //DateTime.TryParse(dateString, provider, DateTimeStyles.AssumeLocal, 

            try
            {
                return DateTime.Parse(dateString, provider);
            }
            catch
            {
                try
                {
                    return DateTime.ParseExact(dateString, "dd/MM/yyyy HH:mm:ss", provider);
                }
                catch
                {
                    return null;
                }
            }

        }

        public static int ToIntValue(this string intString)
        {
            int returnInt = 0;
            int.TryParse(intString, out returnInt);

            return returnInt;
        }

        public static int? ToNullableIntValue(this string intString)
        {
            int returnValue = 0;

            if (int.TryParse(intString, out returnValue))
            {
                return returnValue;
            }
            else
            {
                return null;
            }
                

        }
    }
}
