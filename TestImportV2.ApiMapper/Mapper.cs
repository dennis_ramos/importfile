﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExcelReader;
using ExcelReader.ImportV2.SheetEntity;
using ConXApiDtoModels.DataCreationRequest;
using ConXApiMapper;
using log4net;
using System.Reflection;
using System.Globalization;

namespace ImportV2.ApiMapper
{
    public class Mapper
    {
        private string _fileName;
        private string _fsp;
        private string _requestedBy;
        private ExcelV2Content excelContent;
        private List<string> _errorList;
        private StringBuilder _sb;
        private Request _apiRequest;
        private string _mailToCC = "emerge.developers@e-mergedata.com";
        private string _mailTo = "emerge.developers@e-mergedata.com";
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public Mapper(string fileName, string fsp, string requestedBy, string emailReceipient)
        {
            _fileName = fileName;
            _fsp = fsp;
            _requestedBy = requestedBy;
            excelContent = new ExcelV2Content();
            _errorList = new List<string>();
            _sb = new StringBuilder();
            _apiRequest = new Request { RequestType = "Create Job", RequestedBy = requestedBy, FSP = fsp, JobRequests = new List<JobRequest>() };
            _mailTo = emailReceipient;

            if (string.IsNullOrWhiteSpace(Properties.Settings.Default.EmailCC))
            {
                _mailToCC = "";
            }
            else
            {
                _mailToCC = Properties.Settings.Default.EmailCC;
            }
        }

        public async Task<bool> ProcessFile()
        {
            bool success = true;
            WriteProcessLog("Process Log Start");
            try
            {
                ExcelReader.ImportV2.ExcelReader excelV2Reader = new ExcelReader.ImportV2.ExcelReader(excelContent, _fileName, _errorList, _sb, _fsp);
                await excelV2Reader.ReadExcelFile();
                WriteLocalLogFile(CurrentDate() + " " + _sb.ToString());
                WriteProcessLog("Validating Spreadsheet Contents");

                if (!ExcelHasErrorCheck())
                {
                    SendErrorEmail();
                    return false;
                }

                _log.Info("Compose the Request object");
                CreateJobRequests();
                if (_errorList.Count > 0)
                {
                    SendErrorEmail();
                    return false;
                }

                //Call the Create Job Library
                WriteProcessLog("Start Creating Jobs");
                await CreateJobs().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                LogException(ex);
                SendErrorEmail();
                success = false;
            }
            return success;
        }


        private async Task<bool> CreateJobs()
        {


            JobCreation jobCreation = new JobCreation();
            var response = jobCreation.ProcessRequest(_apiRequest, _fsp, _requestedBy);
            StringBuilder createJobLogs = new StringBuilder();

            bool hasCreationErrors = false;

            if (response == null || response.RequestStatus == "Error")
            {
                //send error email
                hasCreationErrors = true;
                if (response == null)
                {
                    _errorList.Add("There was an error while creating Job Creation request record. Please contact e-Merge support.");
                }
                else
                {
                    _errorList.Add("There was an error while creating Job Creation request record. Please contact e-Merge support. Error was : " + response.Notes);
                }
                SendErrorEmail();
                return false;

            }
            else
            {
                try
                {
                    if (_apiRequest.JobRequests.Count > 1)
                    {
                        bool process = true;
                        bool isNew = true;
                        while (process)
                        {
                            process = await jobCreation.ProcessRequestIdAsyc(response.RequestID, isNew).ConfigureAwait(false);
                            if (process)
                                jobCreation = new JobCreation();

                            isNew = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogException(ex);
                    SendErrorEmail();
                    return false;
                }
            }


            createJobLogs = jobCreation.GetProcessLog(response.RequestID, out hasCreationErrors);
            WriteJobCreationLog(createJobLogs.ToString());
            WriteProcessLog("Import Process Completed; FSP : " + _fsp + " File Name : " + _fileName);
            WriteProcessLog("Exiting Import Process");
            WriteProcessLog("Process Log End:");


            StringBuilder emailBody = new StringBuilder();
            emailBody.AppendLine("Import Process Completed: FSP: " + _fsp + " File Name :" + _fileName);

            emailBody.AppendLine("");
            if (!hasCreationErrors)
                emailBody.AppendLine("All rows were imported successfully. Please see details");
            else
                emailBody.AppendLine("Some rows weren't successfully processed. Please see details");

            emailBody.AppendLine("");
            emailBody.AppendLine("");
            emailBody.AppendLine("Regards");
            emailBody.AppendLine("");
            emailBody.AppendLine("Con-X Support Team");
            emailBody.AppendLine("");
            emailBody.AppendLine("Import Details:");

            emailBody.Append(_sb.ToString());

            string emailSubject = "";
            string server = Properties.Settings.Default.ServerConfig;
            emailSubject = server.Contains("P") ? "" : "[" + server + "] Server ";
            emailSubject += "Job Generation Details";
            SendEmail(emailSubject, emailBody.ToString());
            return true;

        }

        private void SendErrorEmail()
        {
            WriteProcessLog("Data Validation Error(s) found, sending email");
            WriteProcessLog("Import Process Failed");
            WriteProcessLog("Exiting Import Process");
            WriteProcessLog("");
            WriteProcessLog("Generation Errors:");

            StringBuilder emailBody = new StringBuilder();
            emailBody.AppendLine("FileName:" + _fileName + ", emailTo:" + _mailTo);
            emailBody.Append(_sb.ToString());

            // Loop through the errorlist and add to the log file
            if (_errorList.Count > 0)
            {
                foreach (string error in _errorList)
                {
                    WriteProcessLog(error);
                    emailBody.Append(error);
                }
            }

            WriteProcessLog("Process Log End:");
            emailBody.Append("Process Log End:");

            //Send email
            string server = Properties.Settings.Default.ServerConfig;
            string emailSubject = "";
            emailSubject = server.Contains("P") ? "" : "[" + server + "] Server ";
            emailSubject += "Excel Import Mapping Errors";

            SendEmail(emailSubject, emailBody.ToString());
        }

        private void CreateJobRequests()
        {
            excelContent.ImportJobDataList.AsParallel().WithDegreeOfParallelism(2).ForAll(CreateJobRequest);
        }

        private void CreateJobRequest(ImportJobData jobSheetData)
        {
            try
            {
                JobRequest jobRequest = PopulateJobRequest(jobSheetData);
                jobRequest.JobAssets = CreateJobAsset(jobSheetData);
                jobRequest.NonAssetBasedTaskPrePopulations = CreateNonAssetBasedPrepopulation(excelContent.JobTaskItemDataList.Where(a => a.JobRef == jobSheetData.JobRef && a.AssetValue.Length == 0).ToList());
                CreateJobSchedule(jobRequest, excelContent.JobSchedulingDataList.FirstOrDefault(a => a.JobRef == jobSheetData.JobRef));

                jobRequest.JobContacts = CreateJobContacts(excelContent.JobContactDataList.Where(a => a.JobRef == jobSheetData.JobRef).ToList());
                jobRequest.JobAddresses = CreateJobAddresses(excelContent.JobAddressDataList.Where(a => a.JobRef == jobSheetData.JobRef).ToList());
                jobRequest.JobNotes = CreateJobNotes(excelContent.JobNoteDataList.Where(a => a.JobRef == jobSheetData.JobRef && string.IsNullOrWhiteSpace(a.NoteAssetValue) && string.IsNullOrWhiteSpace(a.NoteTaskName)).ToList());
                jobRequest.JobTaskNotes = CreateJobTaskNotes(excelContent.JobNoteDataList.Where(a => a.JobRef == jobSheetData.JobRef && string.IsNullOrWhiteSpace(a.NoteAssetValue) && !string.IsNullOrWhiteSpace(a.NoteTaskName)).ToList());
                jobRequest.JobAttributes = CreateJobAttributes(
                                                                excelContent.JobAttributeDataList.Where(a => a.JobRef == jobSheetData.JobRef).ToList()
                                                                , excelContent.JobSchedulingDataList.FirstOrDefault(a => a.JobRef == jobSheetData.JobRef));

                _apiRequest.JobRequests.Add(jobRequest);
            }
            catch (Exception ex)
            {
                LogException(ex);
            }
        }

        private List<JobContact> CreateJobContacts(List<JobContactData> jobContactList)
        {
            _log.Info("Creating JobContact Objects");
            List<JobContact> jobContacts = new List<JobContact>();
            List<JobContactDetail> jobContactDetails = new List<JobContactDetail>();
            JobContactDetail jobContactDetail;
            string firstName = "";
            string lastName = "";
            string description = "";
            foreach (var contact in jobContactList.OrderBy(a => a.JobRef))
            {
                jobContactDetail = new JobContactDetail();
                jobContactDetail.ContactType = contact.ContactType;
                jobContactDetail.ContactValue = contact.ContactValue;

                if (!string.IsNullOrWhiteSpace(contact.ContactType))
                    jobContactDetails.Add(jobContactDetail);

                firstName = contact.FirstName;
                lastName = contact.LastName;
                description = contact.ContactDescription;
            }

            jobContacts.Add(new JobContact
            {
                FirstName = firstName,
                LastName = lastName,
                ContactDescription = description,
                JobContactDetails = jobContactDetails
            });

            return jobContacts;
        }

        private List<JobAddress> CreateJobAddresses(List<JobAddressData> jobAddressList)
        {
            _log.Info("Creating JobAddress Objects");

            List<JobAddress> jobAddresses = new List<JobAddress>();

            foreach (var address in jobAddressList)
            {
                double? lat = null;
                double? lon = null;
                if (address.lat.Length > 0) lat = double.Parse(address.lat);
                if (address.lon.Length > 0) lon = double.Parse(address.lon);

                jobAddresses.Add(new JobAddress
                {
                    PrimaryAddress = address.isPrimaryAddress.ToUpper().Contains("Y") ? true : false,
                    FlatAppartment = address.StreetUnit,
                    AddressDescription = address.AddressDescription,
                    StreetNumber = address.StreetNumber,
                    StreetName = address.StreetName,
                    Suburb = address.Suburb,
                    City = address.City,
                    Region = address.Region,
                    Latitude = lat,
                    Longitude = lon

                });
            }

            return jobAddresses;
        }

        private List<Note> CreateJobNotes(List<JobNoteData> jobNotesList)
        {
            _log.Info("Creating JobNote Objects");
            List<Note> jobNotes = new List<Note>();

            foreach (var note in jobNotesList)
            {
                jobNotes.Add(new Note
                {
                    NoteType = note.NoteType,
                    NoteMessage = note.Note
                });
            }

            return jobNotes;
        }

        private List<TaskNote> CreateJobTaskNotes(List<JobNoteData> jobNotesList)
        {
            _log.Info("Creating Task Note Objects");
            List<TaskNote> jobTaskNotes = new List<TaskNote>();

            foreach (var note in jobNotesList)
            {
                jobTaskNotes.Add(new TaskNote
                {
                    NoteType = note.NoteType,
                    NoteMessage = note.Note,
                    TaskName = note.NoteTaskName,
                    ParentTaskName = note.ParentTaskName
                });
            }

            return jobTaskNotes;
        }

        private List<JobAttribute> CreateJobAttributes(List<JobAttributeData> jobAttributeList, JobSchedulingData jobScheduleSource)
        {
            _log.Info("Creating JobAttribute Objects");
            List<JobAttribute> jobAttributes = new List<JobAttribute>();

            foreach (var attribute in jobAttributeList)
            {
                jobAttributes.Add(new JobAttribute
                {
                    AttributeName = attribute.AttributeType,
                    AttributeValue = attribute.AttributeValue
                });
            }

            if (jobScheduleSource != null)
            {
                if (!string.IsNullOrEmpty(jobScheduleSource.PreferredUsername))
                    jobAttributes.Add(new JobAttribute { AttributeName = "Preferred User", AttributeValue = jobScheduleSource.PreferredUsername });
                if (!string.IsNullOrEmpty(jobScheduleSource.PlannedFromDate))
                    jobAttributes.Add(new JobAttribute { AttributeName = "Initial Scheduled Date", AttributeValue = jobScheduleSource.PlannedFromDate });
                if (!string.IsNullOrEmpty(jobScheduleSource.Frequency))
                    jobAttributes.Add(new JobAttribute { AttributeName = "Frequency", AttributeValue = jobScheduleSource.Frequency });
                if (!string.IsNullOrEmpty(jobScheduleSource.ContractModel))
                    jobAttributes.Add(new JobAttribute { AttributeName = "Contract Model", AttributeValue = jobScheduleSource.ContractModel });
            }


            return jobAttributes;
        }

        private List<JobAsset> CreateJobAsset(ImportJobData jobSheetData)
        {
            _log.Info("Creating JobAsset Objects");
            List<JobAsset> assets = new List<JobAsset>();
            //get the root level assets for the job ref
            var listAssets = excelContent.JobAssetDataList
                 .Where(a => a.JobRef == jobSheetData.JobRef
                        && a.ParentAssetValue == "")
                 .GroupBy(x => new { x.JobRef, x.AssetType, x.AssetValue }).ToList();

            foreach (var asset in listAssets)
            {
                assets.Add(

                    CreateAsset(
                            excelContent.JobAssetDataList
                            .Where(a => a.JobRef == asset.Key.JobRef && a.AssetType == asset.Key.AssetType && a.AssetValue == asset.Key.AssetValue)
                            .OrderBy(o => o.ParentAssetValue)
                            .ThenBy(x => x.AssetType)
                            .ThenBy(y => y.AssetValue).ToList())

                         );
            }

            return assets;

        }

        private List<NonAssetBasedTaskPrePopulation> CreateNonAssetBasedPrepopulation(List<JobTaskItemData> jobtaskItemDataList)
        {
            _log.Info("Creating NonAssetBasedTaskPrePopulation Objects");
            List<NonAssetBasedTaskPrePopulation> list = new List<NonAssetBasedTaskPrePopulation>();
            foreach (var data in jobtaskItemDataList.GroupBy(a => a.TaskName).ToList())
            {
                NonAssetBasedTaskPrePopulation obj = new NonAssetBasedTaskPrePopulation { TaskName = data.Key, JobTaskItems = new List<JobTaskItem>() };
                foreach (var taskData in jobtaskItemDataList.Where(a => a.TaskName == data.Key))
                {
                    obj.JobTaskItems.Add(new JobTaskItem { TaskItemValue = taskData.TaskItemValue, TaskItemOrder = taskData.TaskItemOrder.ToIntValue() });
                }

                list.Add(obj);
            }
            return list;
        }

        private JobAsset CreateAsset(List<JobAssetData> assetSourceList)
        {
            var source = assetSourceList.First();
            JobAsset asset = new JobAsset
            {
                AssetValueOrSerialNo = source.AssetValue,
                AssetType = source.AssetType,
                PopulateTaskDescriptionWithAssetValue = source.AppendAssetValueToNewTask.Contains("Y") ? true : false,
                AssetAttributes = new List<JobAssetAttribute>(),
                SubAssets = new List<JobAsset>(),
                Notes = new List<Note>(),
                JobTaskItemPrePopulations = new List<JobTaskItem>()
            };

            if (excelContent.JobTaskItemDataList.Count > 0)
            {
                asset.Notes = excelContent.JobNoteDataList.Where(a =>
                        a.JobRef == source.JobRef &&
                        a.NoteTaskName.ToUpper().Trim() == source.NewTaskNameInWorkflow.ToUpper().Trim() &&
                        a.NoteAssetValue.ToUpper().Trim() == source.AssetValue.ToUpper().Trim() &&
                        a.ParentAssetValue.ToUpper().Trim() == source.ParentAssetValue.ToUpper().Trim()
                        ).Select(x => new Note { NoteType = x.NoteType, NoteMessage = x.Note }).ToList();

                if (asset.Notes == null) asset.Notes = new List<Note>();
            }

            List<JobAssetAttribute> jobAssetAttribs = new List<JobAssetAttribute>();
            jobAssetAttribs.Add(new JobAssetAttribute { AttributeName = "Append asset value to new task", AttributeValue = source.AppendAssetValueToNewTask });
            if (!string.IsNullOrEmpty(source.ParentTaskName))
            {
                jobAssetAttribs.Add(new JobAssetAttribute { AttributeName = "Parent Task Name", AttributeValue = source.ParentTaskName });
            }

            jobAssetAttribs.AddRange(assetSourceList.Where(a => a.AssetAttribute.Length > 0 && a.AssetAttributeValue.Length > 0).Select(x => new JobAssetAttribute { AttributeName = x.AssetAttribute, AttributeValue = x.AssetAttributeValue, TaskItemOrderToPrePopulate = null }).ToList());

            asset.AssetAttributes = jobAssetAttribs;

            if (excelContent.JobTaskItemDataList.Count > 0)
            {
                asset.JobTaskItemPrePopulations = excelContent.JobTaskItemDataList.Where(
                            a => a.JobRef == source.JobRef &&
                                a.TaskName.ToUpper().Trim() == source.NewTaskNameInWorkflow.ToUpper().Trim() &&
                                a.AssetValue == source.AssetValue &&
                                a.ParentAssetValue == source.ParentAssetValue &&
                                a.ParentTaskName == source.ParentTaskName).Select(x => new JobTaskItem { TaskItemOrder = x.TaskItemOrder.ToIntValue(), TaskItemValue = x.TaskItemValue }).ToList();

                if (asset.JobTaskItemPrePopulations == null) asset.JobTaskItemPrePopulations = new List<JobTaskItem>();
            }

            var listAssets = excelContent.JobAssetDataList
                .Where(a => a.JobRef == source.JobRef
                       && a.ParentAssetValue == source.AssetValue
                       && a.ParentTaskName.ToUpper().Trim() == source.NewTaskNameInWorkflow.ToUpper().Trim())
                .GroupBy(x => new { x.JobRef, x.AssetType, x.AssetValue, x.ParentAssetValue, x.ParentTaskName }).ToList();

            foreach (var assetGroupKey in listAssets)
            {
                asset.SubAssets.Add(
                    CreateAsset(
                            excelContent.JobAssetDataList
                            .Where(
                                a => a.JobRef == assetGroupKey.Key.JobRef &&
                                a.AssetType == assetGroupKey.Key.AssetType
                                && a.AssetValue == assetGroupKey.Key.AssetValue
                                && a.ParentAssetValue == assetGroupKey.Key.ParentAssetValue
                                && a.ParentTaskName == assetGroupKey.Key.ParentTaskName)
                            .OrderBy(o => o.ParentAssetValue)
                            .ThenBy(x => x.AssetType)
                            .ThenBy(y => y.AssetValue).ToList())
                               );
            }


            return asset;
        }

        private void CreateJobSchedule(JobRequest jobRequest, JobSchedulingData jobScheduleSource)
        {
            _log.Info("Creating JobSchedule Objects");
            if (jobScheduleSource == null) return;

            jobRequest.JobSchedule = new JobSchedule
            {
                AssignToDevice = jobScheduleSource.AssignToDevice.Contains("Y"),
                CreateMultipleInstances = jobScheduleSource.NumberOfOccurrences.ToNullableIntValue() == null ? false : jobScheduleSource.NumberOfOccurrences.ToNullableIntValue() > 1,
                Frequency = jobScheduleSource.Frequency.ToNullableIntValue(),
                NumberOfOccurrences = jobScheduleSource.NumberOfOccurrences.ToNullableIntValue(),
                PlannedFrom = (jobScheduleSource.PlannedFromDate + " " + jobScheduleSource.PlannedFromTime).ToDateValue(),
                PlannedTo = (jobScheduleSource.PlannedToDate + " " + jobScheduleSource.PlannedToTime).ToDateValue(),
                RequiresAppointment = jobScheduleSource.AppointmentNeeded.Contains("Y"),
                ScheduleType = jobScheduleSource.ScheduleType,
                TechUserName = jobScheduleSource.PreferredUsername
            };

            return;

        }

        public JobRequest PopulateJobRequest(ImportJobData jobSheetData)
        {
            _log.Info("Creating JobRequest Object");
            var address = excelContent.JobAddressDataList.FirstOrDefault(a => a.JobRef == jobSheetData.JobRef);
            string region = "-Unknown-";
            int? jobSequence = null;
            int seq = 0;
            if (int.TryParse(jobSheetData.JobSeq, out seq))
            {
                jobSequence = seq;
            }

            if (address != null)
                if (!string.IsNullOrWhiteSpace(address.Region))
                    region = address.Region;

            return new JobRequest
            {
                Description = jobSheetData.JobDescription,
                Group1 = jobSheetData.Group1,
                Group2 = jobSheetData.Group2,
                Group3 = jobSheetData.Group3,
                JobReference = jobSheetData.ConXJobReference,
                Region = region,
                WorkStream = jobSheetData.WorkStreamName,
                JobType = jobSheetData.JobType,
                ImportFileJobRef = jobSheetData.JobRef,
                JobSequence = jobSequence,
                JobAddresses = new List<JobAddress>(),
                JobAssets = new List<JobAsset>(),
                JobAttributes = new List<JobAttribute>(),
                JobContacts = new List<JobContact>(),
                JobNotes = new List<Note>(),
                JobSchedule = new JobSchedule(),
                NonAssetBasedTaskPrePopulations = new List<NonAssetBasedTaskPrePopulation>()
            };
        }

        private bool ExcelHasErrorCheck()
        {
            bool success = true;

            if (_errorList.Count > 0)
            {
                success = false;
                //to do: add the error to the log (_sb)
                //send email
            }

            if (
                excelContent.ImportJobDataList.Count(a => a.HasError == true) > 0 ||
                excelContent.JobAddressDataList.Count(a => a.HasError == true) > 0 ||
                excelContent.JobAssetDataList.Count(a => a.HasError == true) > 0 ||
                excelContent.JobAttributeDataList.Count(a => a.HasError == true) > 0 ||
                excelContent.JobContactDataList.Count(a => a.HasError == true) > 0 ||
                excelContent.JobNoteDataList.Count(a => a.HasError == true) > 0 ||
                excelContent.JobSchedulingDataList.Count(a => a.HasError == true) > 0 ||
                excelContent.JobTaskItemDataList.Count(a => a.HasError == true) > 0
                )
            {
                success = false;
            }

            if (!success)
            {
                // Iterate through each object and get the job reference number and error StringBuilder
                // detail. Helps in checking which record has an error during investigation.
                foreach (ImportJobData importJob in excelContent.ImportJobDataList.Where(a => a.HasError == true))
                {
                    _errorList.Add("Error in JobRef : " + importJob.JobRef + " Detail : " + importJob.er.ToString());
                }

                foreach (JobAddressData address in excelContent.JobAddressDataList.Where(a => a.HasError == true))
                {
                    _errorList.Add("Error in JobRef : " + address.JobRef + " Detail : " + address.er.ToString());
                }

                foreach (JobAssetData asset in excelContent.JobAssetDataList.Where(a => a.HasError == true))
                {
                    _errorList.Add("Error in JobRef : " + asset.JobRef + " Detail : " + asset.er.ToString());
                }

                foreach (JobAttributeData attribute in excelContent.JobAttributeDataList.Where(a => a.HasError == true))
                {
                    _errorList.Add("Error in JobRef : " + attribute.JobRef + " Detail : " + attribute.er.ToString());
                }

                foreach (JobContactData contact in excelContent.JobContactDataList.Where(a => a.HasError == true))
                {
                    _errorList.Add("Error in JobRef : " + contact.JobRef + " Detail : " + contact.er.ToString());
                }

                foreach (JobNoteData note in excelContent.JobNoteDataList.Where(a => a.HasError == true))
                {
                    _errorList.Add("Error in JobRef : " + note.JobRef + " Detail : " + note.er.ToString());
                }

                foreach (JobSchedulingData schedule in excelContent.JobSchedulingDataList.Where(a => a.HasError == true))
                {
                    _errorList.Add("Error in JobRef : " + schedule.JobRef + " Detail : " + schedule.er.ToString());
                }

                foreach (JobTaskItemData taskItem in excelContent.JobTaskItemDataList.Where(a => a.HasError == true))
                {
                    _errorList.Add("Error in JobRef : " + taskItem.JobRef + " Detail : " + taskItem.er.ToString());
                }

            }

            return success;
        }

        private void WriteJobCreationLog(string logEvent)
        {

            _sb.Append(logEvent);
            WriteLocalLogFile(logEvent);
        }

        private void WriteProcessLog(string logEvent)
        {
            _sb.AppendLine(CurrentDate() + " " + logEvent);
            WriteLocalLogFile(CurrentDate() + " " + logEvent);

        }

        // Sending email functionality
        public void SendEmail(string emailSubject, string emailBody)
        {
            //try
            //{
            //    this.WriteLocalLogFile(CurrentDate() + " " + "Ready to send email... emailSubject [ " + emailSubject + " ]");
            //    System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient();
            //    System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            //    mail.To.Add(new System.Net.Mail.MailAddress(_mailTo));

            //    if (!string.IsNullOrEmpty(_mailToCC))
            //        mail.CC.Add(new System.Net.Mail.MailAddress(_mailToCC));

            //    mail.Subject = emailSubject;
            //    mail.Body = emailBody;
            //    smtpClient.Send(mail);
            //    this.WriteLocalLogFile(CurrentDate() + " " + "email sent successfully.");

            //}
            //catch (Exception ex)
            //{
            //    this.WriteLocalLogFile(CurrentDate() + " " + "failed to send email.");
            //    this.WriteLocalLogFile(ex.ToString());
            //}
        }

        // For local testing purposes only
        private void WriteLocalLogFile(string content)
        {
            if (Properties.Settings.Default.WriteLocalLogFile == false) return;

            string logFileName = this._fileName;
            string ph = Properties.Settings.Default.LocalLogFileDirectory;

            try
            {
                if (File.Exists(ph + ".txt"))
                    File.Delete(ph + ".txt");
            }
            catch { }

            if (string.IsNullOrWhiteSpace(logFileName) == true)
                return;


            try
            {

                if (Directory.Exists(ph) == false)
                    Directory.CreateDirectory(ph);

                StreamWriter sw = new StreamWriter(ph + Path.GetFileNameWithoutExtension(logFileName) + ".txt", true);
                sw.WriteLine(content);
                sw.Close();
                sw.Dispose();
            }
            catch (Exception ex)
            {
                LogException(ex);
            }

        }

        private string CurrentDate()
        {
            return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }

        private void LogException(Exception ex)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Exception Caught:");
            sb.AppendLine("Exception = " + ex.Message);
            _errorList.Add("Exception :" + ex.Message);

            if (ex.InnerException != null)
            {
                sb.AppendLine("Inner Exception = " + ex.InnerException.Message);
                _errorList.Add("Inner Exception :" + ex.InnerException.Message);
            }
            sb.AppendLine("Stack Trace: " + ex.StackTrace);
            _log.Error(sb.ToString());
            _errorList.Add("Stack Trace:" + ex.StackTrace);

        }
    }
}
