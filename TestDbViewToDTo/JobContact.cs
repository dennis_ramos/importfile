namespace ConXApiDtoRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobContactDTO")]
    public partial class JobContact
    {
        public JobContact()
        {
            JobContactDetails = new HashSet<JobContactDetail>();
        }

        [Key]
        [Column(Order = 0)]
        public Guid JobContactID { get; set; }

        [Column(Order = 1)]
        public Guid JobID { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string MiddleName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(15)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        [StringLength(50)]
        public string ContactSource { get; set; }

        public virtual Job Job { get; set; }

        public ICollection<JobContactDetail> JobContactDetails { get; set; }


    }
}
