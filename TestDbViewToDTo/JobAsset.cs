namespace ConXApiDtoRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobAssetDTO")]
    public partial class JobAsset
    {
        public JobAsset()
        {
            JobAssetAttributes = new HashSet<JobAssetAttribute>();
            JobTasks = new HashSet<JobTask>();
        }

        [Key]
        [Column(Order = 0)]
        public Guid JobAssetID { get; set; }

        [Column(Order = 1)]
        public Guid JobID { get; set; }

        [StringLength(50)]
        public string AssetType { get; set; }

        [StringLength(50)]
        public string AssetValueOrSerialNo { get; set; }

        public DateTime? ModifiedOn { get; set; }

        [StringLength(15)]
        public string ModifiedBy { get; set; }

        public virtual Job Job { get; set; }

        public ICollection<JobAssetAttribute> JobAssetAttributes { get; set; }
        public ICollection<JobTask> JobTasks { get; set; }
    }
}
