namespace ConXApiDtoRepository
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class JobDtoModel : DbContext
    {
        public JobDtoModel()
            : base("name=JobDtoModel")
        {
        }
         
        public virtual DbSet<Job> Jobs { get; set; }
        public virtual DbSet<JobAddress> JobAddresses { get; set; }
        public virtual DbSet<JobAssetAttribute> JobAssetAttributes { get; set; }
        public virtual DbSet<JobAsset> JobAssets { get; set; }
        public virtual DbSet<JobAttribute> JobAttributes { get; set; }
        public virtual DbSet<JobSchedule> JobSchedules { get; set; }
        public virtual DbSet<JobContactDetail> JobContactDetails { get; set; }
        public virtual DbSet<JobContact> JobContacts { get; set; }
        public virtual DbSet<JobGPS> JobGPSs { get; set; }
        public virtual DbSet<JobNote> JobNotes { get; set; }
        public virtual DbSet<JobPhoto> JobPhotos { get; set; }
        public virtual DbSet<JobTask> JobTasks { get; set; }
        public virtual DbSet<JobTaskItem> JobTaskItems { get; set; }
        public virtual DbSet<JobTaskNote> JobTaskNotes { get; set; }
        public virtual DbSet<JobTaskPhoto> JobTaskPhotos { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Job>()
                .Property(e => e.JobNo)
                .IsUnicode(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.FSP)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.COReferenceNo)
                .IsUnicode(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.WorkStream)
                .IsUnicode(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.OrganisationName)
                .IsUnicode(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.DeviceName)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.JobStatus)
                .IsUnicode(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.StatusBy)
                .IsUnicode(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.CompletedBy)
                .IsUnicode(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.WorkFlowName)
                .IsUnicode(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.JobType)
                .IsUnicode(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.TechUserName)
                .IsUnicode(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.Region)
                .IsUnicode(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.Group1)
                .IsUnicode(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.Group2)
                .IsUnicode(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.Group3)
                .IsUnicode(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.FieldStatus)
                .IsUnicode(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.Outcome)
                .IsUnicode(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.OutcomeDescription)
                .IsUnicode(false);

            modelBuilder.Entity<Job>()
                .Property(e => e.JobQualifier)
                .IsUnicode(false);

            modelBuilder.Entity<JobAddress>()
                .Property(e => e.AddressDescription)
                .IsUnicode(false);

            modelBuilder.Entity<JobAddress>()
                .Property(e => e.StreetNumber)
                .IsUnicode(false);

            modelBuilder.Entity<JobAddress>()
                .Property(e => e.FlatAppartment)
                .IsUnicode(false);

            modelBuilder.Entity<JobAddress>()
                .Property(e => e.StreetName)
                .IsUnicode(false);

            modelBuilder.Entity<JobAddress>()
                .Property(e => e.Suburb)
                .IsUnicode(false);

            modelBuilder.Entity<JobAddress>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<JobAddress>()
                .Property(e => e.Region)
                .IsUnicode(false);

            modelBuilder.Entity<JobAddress>()
                .Property(e => e.Latitude)
                .IsUnicode(false);

            modelBuilder.Entity<JobAddress>()
                .Property(e => e.Longitude)
                .IsUnicode(false);

            modelBuilder.Entity<JobAssetAttribute>()
                .Property(e => e.Attribute)
                .IsUnicode(false);

            modelBuilder.Entity<JobAssetAttribute>()
                .Property(e => e.AttributeValue)
                .IsUnicode(false);

            modelBuilder.Entity<JobAssetAttribute>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobAsset>()
                .Property(e => e.AssetType)
                .IsUnicode(false);

            modelBuilder.Entity<JobAsset>()
                .Property(e => e.AssetValueOrSerialNo)
                .IsUnicode(false);

            modelBuilder.Entity<JobAsset>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobAttribute>()
                .Property(e => e.Attribute)
                .IsUnicode(false);

            modelBuilder.Entity<JobAttribute>()
                .Property(e => e.AttributeValue)
                .IsUnicode(false);

            modelBuilder.Entity<JobAttribute>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobSchedule>()
                .Property(e => e.TechUserName)
                .IsUnicode(false);

            modelBuilder.Entity<JobContactDetail>()
                .Property(e => e.ContactType)
                .IsUnicode(false);

            modelBuilder.Entity<JobContactDetail>()
                .Property(e => e.ContactValue)
                .IsUnicode(false);

            modelBuilder.Entity<JobContactDetail>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobContact>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<JobContact>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<JobContact>()
                .Property(e => e.MiddleName)
                .IsUnicode(false);

            modelBuilder.Entity<JobContact>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<JobContact>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobContact>()
                .Property(e => e.ContactSource)
                .IsUnicode(false);

            modelBuilder.Entity<JobGPS>()
                .Property(e => e.TableName)
                .IsUnicode(false);

            modelBuilder.Entity<JobGPS>()
                .Property(e => e.TableColName)
                .IsUnicode(false);

            modelBuilder.Entity<JobNote>()
                .Property(e => e.NoteType)
                .IsUnicode(false);

            modelBuilder.Entity<JobNote>()
                .Property(e => e.Note)
                .IsUnicode(false);

            modelBuilder.Entity<JobNote>()
                .Property(e => e.NoteBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobNote>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobPhoto>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<JobPhoto>()
                .Property(e => e.PhotoBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobPhoto>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobPhoto>()
                .Property(e => e.PhotoURL)
                .IsUnicode(false);

            modelBuilder.Entity<JobTask>()
                .Property(e => e.TaskType)
                .IsUnicode(false);

            modelBuilder.Entity<JobTask>()
                .Property(e => e.TaskName)
                .IsUnicode(false);

            modelBuilder.Entity<JobTask>()
                .Property(e => e.TaskDescription)
                .IsUnicode(false);

            modelBuilder.Entity<JobTask>()
                .Property(e => e.TaskStatus)
                .IsUnicode(false);

            modelBuilder.Entity<JobTask>()
                .Property(e => e.TaskQualifier)
                .IsUnicode(false);

            modelBuilder.Entity<JobTask>()
                .Property(e => e.CompletedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobTask>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobTaskItem>()
                .Property(e => e.TaskItemName)
                .IsUnicode(false);

            modelBuilder.Entity<JobTaskItem>()
                .Property(e => e.Result)
                .IsUnicode(false);

            modelBuilder.Entity<JobTaskItem>()
                .Property(e => e.ValidationResult)
                .IsUnicode(false);

            modelBuilder.Entity<JobTaskItem>()
                .Property(e => e.EnteredBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobTaskItem>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobTaskNote>()
                .Property(e => e.NoteType)
                .IsUnicode(false);

            modelBuilder.Entity<JobTaskNote>()
                .Property(e => e.Note)
                .IsUnicode(false);

            modelBuilder.Entity<JobTaskNote>()
                .Property(e => e.NoteBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobTaskNote>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobTaskPhoto>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<JobTaskPhoto>()
                .Property(e => e.PhotoBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobTaskPhoto>()
                .Property(e => e.ModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobTaskPhoto>()
                .Property(e => e.PhotoURL)
                .IsUnicode(false);


            modelBuilder.Entity<Job>()
                .HasMany(e => e.JobAddresses)
                .WithRequired(e => e.Job)
                .HasForeignKey(e => e.JobID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobSchedule>()
                .HasKey(e => e.JobID);

            modelBuilder.Entity<Job>()
                .HasOptional(s => s.JobSchedule) 
                .WithRequired(ad => ad.Job);

            modelBuilder.Entity<Job>()
               .HasMany(e => e.JobAssets)
               .WithRequired(e => e.Job)
               .HasForeignKey(e => e.JobID)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobAsset>()
               .HasMany(e => e.JobAssetAttributes)
               .WithRequired(e => e.JobAsset)
               .HasForeignKey(e => e.JobAssetID)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
               .HasMany(e => e.JobAttributes)
               .WithRequired(e => e.Job)
               .HasForeignKey(e => e.JobID)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
               .HasMany(e => e.JobContacts)
               .WithRequired(e => e.Job)
               .HasForeignKey(e => e.JobID)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobContact>()
               .HasMany(e => e.JobContactDetails)
               .WithRequired(e => e.JobContact)
               .HasForeignKey(e => e.JobContactID)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
               .HasMany(e => e.JobGPSs)
               .WithRequired(e => e.Job)
               .HasForeignKey(e => e.JobID)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
               .HasMany(e => e.JobNotes)
               .WithRequired(e => e.Job)
               .HasForeignKey(e => e.JobID)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
               .HasMany(e => e.JobPhotos)
               .WithRequired(e => e.Job)
               .HasForeignKey(e => e.JobID)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<Job>()
               .HasMany(e => e.JobTasks)
               .WithRequired(e => e.Job)
               .HasForeignKey(e => e.JobID)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobTask>()
                .HasMany(e => e.SubTasks)
                .WithOptional(e => e.ParentJobTask)
                .HasForeignKey(e => e.ParentJobTaskID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobAsset>()
                .HasMany(e => e.JobTasks)
                .WithOptional(e => e.JobAsset)
                .HasForeignKey(e => e.JobAssetID)
                .WillCascadeOnDelete(false);
            
            modelBuilder.Entity<JobTask>()
                .HasMany(e => e.JobTaskItems)
                .WithRequired(e => e.JobTask)
                .HasForeignKey(e => e.JobTaskID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobTask>()
                .HasMany(e => e.JobTaskNotes)
                .WithOptional(e => e.JobTask)
                .HasForeignKey(e => e.JobTaskID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobTask>()
                .HasMany(e => e.JobTaskPhotos)
                .WithOptional(e => e.JobTask)
                .HasForeignKey(e => e.JobTaskID)
                .WillCascadeOnDelete(false);
        }
    }
}
