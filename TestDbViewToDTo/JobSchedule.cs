namespace ConXApiDtoRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobSchedule")]
    public partial class JobSchedule
    {
        [Key, ForeignKey("Job")]
        public Guid JobID { get; set; }

        public DateTime? PlannedFrom { get; set; }

        public DateTime? PlannedTo { get; set; }

        [StringLength(50)]
        public string TechUserName { get; set; }

        public float? ScheduleType { get; set; }

        public bool? IsAppointment { get; set; }

        public virtual Job Job { get; set; }

    }
}
