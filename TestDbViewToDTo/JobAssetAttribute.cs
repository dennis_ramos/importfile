namespace ConXApiDtoRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobAssetAttributeDTO")]
    public partial class JobAssetAttribute
    {
        [Key]
        [Column(Order = 0)]
        public Guid JobAssetAttribID { get; set; }

        
        [Column(Order = 1)]
        public Guid JobAssetID { get; set; }

        [StringLength(50)]
        public string Attribute { get; set; }

        [StringLength(200)]
        public string AttributeValue { get; set; }

        [StringLength(15)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public virtual JobAsset JobAsset { get; set; }
    }
}
