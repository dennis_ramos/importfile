namespace ConXApiDtoRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobAddressDTO")]
    public partial class JobAddress
    {
        [Key]
        [Column(Order = 0)]
        public Guid JobAddressID { get; set; }

        [Column(Order = 1)]
        public Guid JobID { get; set; }

        [Column(Order = 2)]
        public bool PrimaryAddress { get; set; }

        [StringLength(250)]
        public string AddressDescription { get; set; }

        [StringLength(20)]
        public string StreetNumber { get; set; }

        [StringLength(20)]
        public string FlatAppartment { get; set; }

        [StringLength(50)]
        public string StreetName { get; set; }

        [StringLength(50)]
        public string Suburb { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        public float? Country { get; set; }

        [StringLength(50)]
        public string Region { get; set; }

        [StringLength(50)]
        public string Latitude { get; set; }

        [StringLength(50)]
        public string Longitude { get; set; }

        public virtual Job Job { get; set; }
    }
}
