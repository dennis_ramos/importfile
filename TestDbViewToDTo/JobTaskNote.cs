namespace ConXApiDtoRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobTaskNoteDTO")]
    public partial class JobTaskNote
    {
        [Key]
        public Guid JobNoteID { get; set; }

        public Guid? JobTaskID { get; set; }

        [StringLength(50)]
        public string NoteType { get; set; }

        [StringLength(1000)]
        public string Note { get; set; }

        public DateTime? NoteOn { get; set; }

        [StringLength(50)]
        public string NoteBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        [StringLength(15)]
        public string ModifiedBy { get; set; }

        public JobTask JobTask { get; set; }
    }
}
