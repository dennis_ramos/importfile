namespace ConXApiDtoRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Job")]
    public partial class Job
    {
        public Job()
        {
            
            JobAddresses = new HashSet<JobAddress>();
            JobAssets = new HashSet<JobAsset>();
            JobAttributes = new HashSet<JobAttribute>();
            JobContacts = new HashSet<JobContact>();
            JobGPSs = new HashSet<JobGPS>();
            JobNotes = new HashSet<JobNote>();
            JobPhotos = new HashSet<JobPhoto>();
            JobTasks = new HashSet<JobTask>();

        }

        [Key]
        [Column(Order = 0)]
        public Guid JobID { get; set; }

        [StringLength(50)]
        public string JobNo { get; set; }
        
        [Column(Order = 1)]
        [StringLength(10)]
        public string FSP { get; set; }

        [StringLength(50)]
        public string COReferenceNo { get; set; }

        [StringLength(50)]
        public string WorkStream { get; set; }

        [StringLength(50)]
        public string OrganisationName { get; set; }

        [StringLength(16)]
        public string DeviceName { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        [StringLength(50)]
        public string JobStatus { get; set; }

        [StringLength(50)]
        public string StatusBy { get; set; }

        public DateTime? StatusOn { get; set; }

        [Column(Order = 2)]
        public bool IsCompleted { get; set; }

        [StringLength(50)]
        public string CompletedBy { get; set; }

        public DateTime? CompletedOn { get; set; }

        [StringLength(50)]
        public string WorkFlowName { get; set; }

        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int JobSequenceOrder { get; set; }

        [StringLength(50)]
        public string JobType { get; set; }

        [StringLength(50)]
        public string TechUserName { get; set; }

        [StringLength(50)]
        public string Region { get; set; }

        public DateTime? ModifiedOn { get; set; }

        [StringLength(15)]
        public string ModifiedBy { get; set; }

        [StringLength(50)]
        public string Group1 { get; set; }

        [StringLength(50)]
        public string Group2 { get; set; }

        [StringLength(50)]
        public string Group3 { get; set; }

        [StringLength(50)]
        public string FieldStatus { get; set; }

        [StringLength(50)]
        public string Outcome { get; set; }

        [StringLength(100)]
        public string OutcomeDescription { get; set; }

        public DateTime? FieldOutcomeDate { get; set; }

        [StringLength(50)]
        public string JobQualifier { get; set; }

        public ICollection<JobAddress> JobAddresses { get; set; }
        public virtual JobSchedule JobSchedule { get; set; }
        public ICollection<JobAsset> JobAssets { get; set; }
        public ICollection<JobAttribute> JobAttributes { get; set; }
        public ICollection<JobContact> JobContacts { get; set; }
        public ICollection<JobGPS> JobGPSs { get; set; }
        public ICollection<JobNote> JobNotes { get; set; }
        public ICollection<JobPhoto> JobPhotos { get; set; }
        public ICollection<JobTask> JobTasks { get; set; }

    }
}
