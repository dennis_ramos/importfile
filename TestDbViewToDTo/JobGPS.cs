namespace ConXApiDtoRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobGPSDTO")]
    public partial class JobGPS
    {
        [Key]
        [Column(Order = 0)]
        public Guid GPSID { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid JobID { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string TableName { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(50)]
        public string TableColName { get; set; }

        [Key]
        [Column(Order = 4)]
        public Guid TableRowID { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public double? HDOP { get; set; }

        public short? SatCount { get; set; }

        public short? SatInView { get; set; }

        public double? SeaLevelAltitude { get; set; }

        public DateTime? CapturedDateTime { get; set; }

        public virtual Job Job { get; set; }
    }
}
