namespace ConXApiDtoRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobPhotoDTO")]
    public partial class JobPhoto
    {
        [Key]
        [Column(Order = 0)]
        public Guid JobPhotoID { get; set; }

        
        [Column(Order = 1)]
        public Guid JobID { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        public DateTime? PhotoOn { get; set; }

        [StringLength(50)]
        public string PhotoBy { get; set; }

        [StringLength(15)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public bool? PhotoProcessed { get; set; }

        [StringLength(1189)]
        public string PhotoURL { get; set; }

        public virtual Job Job { get; set; }

    }
}
