namespace ConXApiDtoRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("JobTaskPhotoDTO")]
    public partial class JobTaskPhoto
    {
        [Key]
        public Guid JobPhotoID { get; set; }

        public Guid? JobTaskID { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        public DateTime? PhotoOn { get; set; }

        [StringLength(50)]
        public string PhotoBy { get; set; }

        [StringLength(15)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public bool? PhotoProcessed { get; set; }

        [StringLength(1189)]
        public string PhotoURL { get; set; }

        public JobTask JobTask { get; set; }
    }
}
