﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestConXApiRepository;

namespace TestConXApiRepository
{
    class Program
    {
        static void Main(string[] args)
        {
            var model = new ConXApiRepository.ConXApiModels();
            model.Configuration.LazyLoadingEnabled = true;
            model.Configuration.ProxyCreationEnabled = true;


            //var job = model.jobJobs
            //    //.Include("jobAssets.jobAssetAttribs")
            //    //.Include("jobAssets.AssetTasks")
            //    .FirstOrDefault(a => a.JobNo == "1007473" && a.FSP == "WELLS");

            //Console.WriteLine("Access Job");
            //Console.WriteLine(job.JobNo);

            //Console.WriteLine("Loading Assets");
            //foreach (var asset in job.jobAssets)
            //{
            //    Console.WriteLine("Asset : " + asset.AssetName  + " Asset type : " + asset.enumAssetType1.enumDesc);
            //    foreach (var task in asset.AssetTasks)
            //    {
            //        Console.WriteLine("     Asset Task : " + task.cfgWorkFlowTask.TaskName + " Task ID : " + task.TaskID.ToString());
 
            //    }

            //    foreach (var attrb in asset.jobAssetAttribs)
            //    {
            //        Console.WriteLine("     AttributeName : " + attrb.Attribute + " Value : " + attrb.AttributeValue);
            //    }

            //}

            //foreach (var task in job.jobTasks.Where(a => a.ParentJobTaskID == Guid.Empty).OrderBy(b=> b.TaskOrder).ToList())
            //{
            //    Console.WriteLine("Task Name : " + task.cfgWorkFlowTask.TaskName + " " + task.TaskDescription + " TaskItemCount : " + task.jobTaskItems.Count.ToString());
            //    if (task.SubTasks.Count > 0)
            //    {
            //        foreach (var subTask in task.SubTasks)
            //        {
            //            Console.WriteLine("       Sub Task Name : " + subTask.cfgWorkFlowTask.TaskName + " " + subTask.TaskDescription + " TaskItemCount : " + subTask.jobTaskItems.Count.ToString());
            //        }
            //    }

            //}


            foreach(var stat in model.ApiRequestStatuses.Where(a=> a.ApiRequestStatusId != 0))
            {
                Console.WriteLine(stat.ApiRequestStatusDescription + " " + stat.ApiRequestStatusId.ToString());

            }
            Console.ReadKey();
        }
    }
}
