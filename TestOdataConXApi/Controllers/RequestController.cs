﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ConXApiDtoModels.DataCreationRequest;
using System.Web.Http.Description;
using System.Web;
using ConXApiMapper;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Web.OData;
using ConXApiDtoModels;

namespace ConxODataWebApi.Controllers
{
    [Authorize]
    public class RequestController : ApiController
    {
        //// GET: api/Request
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET: api/Request/5

        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Request
        [ResponseType(typeof(RequestResponse))]
        //public RequestResponse Post([FromBody]Request request)
        //public async Task<IHttpActionResult> Post([FromBody]Request request)
        public RequestResponse Post(HttpRequestMessage request)
        {
            string userFSP = HttpContext.Current.User.Identity.Name.Split(new char[] { '!' })[0];
            string user = HttpContext.Current.User.Identity.Name.Split(new char[] { '!' })[1];

            var jsonString = request.Content.ReadAsStringAsync().Result;

            //JobRequest jobRequest = JsonConvert.DeserializeObject<JobRequest>(jsonString);
            JobRequest[] jobRequests = JsonConvert.DeserializeObject<List<JobRequest>>(jsonString).ToArray();
            Request apiRequest = new Request { RequestType = "Create Job", RequestedBy = user, FSP = userFSP, JobRequests = jobRequests };

            JobCreation jobCreation = new JobCreation();
            RequestResponse reponse = jobCreation.ProcessRequest(apiRequest, userFSP, user);
            return reponse;
        }

        // PUT: api/Request/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Request/5
        public void Delete(int id)
        {
        }

        /*
        public IHttpActionResult Patch([FromBody] UpdateJobStatusRequest request)
        {
            string userFSP = HttpContext.Current.User.Identity.Name.Split(new char[] { '!' })[0];
            string user = HttpContext.Current.User.Identity.Name.Split(new char[] { '!' })[1];

            float status = request.JobStatus;

            return StatusCode(HttpStatusCode.Accepted);
        }*/

        [AcceptVerbs("Patch")]
        public void Patch(string jobNo, Delta<Job> value)
        {
            //RequestResponse reponse = jobCreation.ProcessRequest(apiRequest, userFSP, user);
            //return reponse;
            string userFSP = HttpContext.Current.User.Identity.Name.Split(new char[] { '!' })[0];
            string user = HttpContext.Current.User.Identity.Name.Split(new char[] { '!' })[1];

            /*
            var t = _teams.FirstOrDefault(x => x.Id == id);
            if (t == null) throw new HttpResponseException(HttpStatusCode.NotFound);

            value.Patch(t);
             * */
        }            
    }
}
