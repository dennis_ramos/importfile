﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ConXApiDtoModels;
using System.Web.OData.Routing;
using System.Web.OData;
using System.Web.OData.Query;
using Microsoft.OData.Core;
using ConXApiMapper;
using System.Web;
using ConXApiDtoModels.DataCreationRequest;
using Newtonsoft.Json;

namespace ConxODataWebApi.Controllers
{
    [Authorize]
    public class JobController : ODataController
    {
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        // GET: odata/Job(5)
        public IHttpActionResult GetJob([FromODataUri] System.Guid key, ODataQueryOptions<Job> queryOptions)
        {
            // validate the query.
            try
            {
                queryOptions.Validate(_validationSettings);
            }
            catch (ODataException ex)
            {
                return BadRequest(ex.Message);
            }

            // return Ok<Job>(job);
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        
        [ODataRoute("Jobs/GetJobsByJobNumber(JobNo={jobNo})")]
        [EnableQuery(PageSize = 20, AllowedQueryOptions = AllowedQueryOptions.All)]
        [HttpGet]
        public IHttpActionResult GetJobsByJobNumber([FromODataUri] string jobNo, ODataQueryOptions<Job> queryOptions)
        {
            string userFSP = HttpContext.Current.User.Identity.Name.Split(new char[] { '!' })[0];
            Map mapper = new Map();

            return Ok(mapper.GetResultForJobsByJobNumber(jobNo, userFSP));

            //if (queryOptions.IsNavigationPropertyExpected(x => x.JobAddresses))
            //{
            //    return Ok(mapper.GetDetailedJobsByJobNumber(jobNo, userFSP));
            //}
            //else if (queryOptions.IsNavigationPropertyExpected(x => x.JobTasks))
            //{
            //    return Ok(mapper.GetResultForJobsByJobNumber(jobNo, userFSP));
            //}
            //else
            //{
            //    return Ok(mapper.GetJobsByJobNumber(jobNo, userFSP));
            //}
        }

        [ODataRoute("Jobs/GetJobsByWorkStreamName(WorkStreamName={workStreamName},JobStatus={jobStatus},ModifiedFrom={modifiedFrom},ModifiedTo={modifiedTo})")]
        [EnableQuery(PageSize = 20, AllowedQueryOptions = AllowedQueryOptions.All)]
        [HttpGet]
        public IHttpActionResult GetJobsByWorkStreamName(
            [FromODataUri] string workStreamName, 
            [FromODataUri] string jobStatus,
            [FromODataUri] DateTime modifiedFrom,
            [FromODataUri] DateTime modifiedTo, 
            ODataQueryOptions<Job> queryOptions)
        {
            string userFSP = HttpContext.Current.User.Identity.Name.Split(new char[] { '!' })[0];

            Map mapper = new Map();

            if (queryOptions.IsNavigationPropertyExpected(x => x.JobAddresses))
            {
                return Ok(mapper.GetDetailedJobsByWorkStreamName(workStreamName, jobStatus, modifiedFrom, modifiedTo, userFSP));
            }
            else
            {
                return Ok(mapper.GetJobsByWorkStreamName(workStreamName, jobStatus, modifiedFrom, modifiedTo, userFSP));
            }
        }

        [ODataRoute("Jobs/GetJobsByWorkFlow(WorkFlowName={workFlowName},JobStatus={jobStatus},ModifiedFrom={modifiedFrom},ModifiedTo={modifiedTo})")]
        [EnableQuery(PageSize = 20, AllowedQueryOptions = AllowedQueryOptions.All)]
        [HttpGet]
        public IHttpActionResult GetJobsByWorkFlow(
            [FromODataUri] string workFlowName,
            [FromODataUri] string jobStatus,
            [FromODataUri] DateTime modifiedFrom,
            [FromODataUri] DateTime modifiedTo,
            ODataQueryOptions<Job> queryOptions)
        {
            string userFSP = HttpContext.Current.User.Identity.Name.Split(new char[] { '!' })[0];

            Map mapper = new Map();

            return Ok(mapper.GetJobsByWorkFlow(workFlowName, jobStatus, modifiedFrom, modifiedTo, userFSP));
        }

        [ODataRoute("Jobs/CreateJob()")]
        [HttpPost]
        //public IHttpActionResult CreateJob([FromBody] JobRequest request)
        public IHttpActionResult CreateJob(HttpRequestMessage request)
        {
            try
            {
                //int requestCount = request.JobRequests.Count;

                var jsonString = request.Content.ReadAsStringAsync().Result;

                //JobRequest jobRequest = JsonConvert.DeserializeObject<JobRequest>(jsonString);
                JobRequest[] jobRequests = JsonConvert.DeserializeObject<List<JobRequest>>(jsonString).ToArray();

                JobCreation jc = new JobCreation();

                string FSP = HttpContext.Current.User.Identity.Name.Split(new char[] { '!' })[0];
                string userName = HttpContext.Current.User.Identity.Name.Split(new char[] { '!' })[1];

                Request apiRequest = new Request { RequestType = "Create Job", RequestedBy = userName, FSP = FSP, JobRequests = jobRequests };

                jc.ProcessRequest(apiRequest, FSP, userName); // change this to queue requests if request is more than 1

                //jc.ProcessRequest(request, FSP, userName);

                return StatusCode(HttpStatusCode.Created);
            }
            catch (JsonSerializationException e)
            {
                return StatusCode(HttpStatusCode.BadRequest);
            }
            catch (Exception e)
            {
                return StatusCode(HttpStatusCode.InternalServerError);
            }
        }
    }
}
