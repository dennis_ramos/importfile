﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
//using System.Web.Http.OData;
//using System.Web.Http.OData.Query;
//using System.Web.Http.OData.Routing;
using ConxODataWebApi.Models;
//using Microsoft.Data.OData;
using System.Linq.Expressions;


using Microsoft.OData.Edm;
using System.Web.OData.Builder;
using System.Web.OData.Extensions;
using System.Web.OData;
using System.Web.OData.Query;
using Microsoft.OData.Core;

namespace ConxODataWebApi.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using TestOdataConXApi.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<Product>("Products");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    [Authorize]
    public class ProductsController : ODataController
    {
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        // GET: odata/Products
        [EnableQuery]
        public IHttpActionResult GetProducts(ODataQueryOptions<Product> queryOptions)
        {
            // validate the query.
            try
            {
                queryOptions.Validate(_validationSettings);
            }
            catch (ODataException ex)
            {
                return BadRequest(ex.Message);
            }

            //if (queryOptions.IsNavigationPropertyExpected(x => x.))
            //{ 
            //}
            // return Ok<IEnumerable<Product>>(products);
            //return StatusCode(HttpStatusCode.NotImplemented);
            return Ok(MyProductList());
        }
        //[HttpGet]
        //[Queryable]
        //public IQueryable<Product> Get(ODataQueryOptions<Product> odataQueryOptions)
        //{
        //    //var context = new ProjectionAndExpandExampleContext();

        //    if (odataQueryOptions.IsNavigationPropertyExpected(x => x.Products))
        //    {
        //        var returnValue = context.EfSuppliers
        //            .AsNoTracking()
        //            .Select(x => new Supplier()
        //            {
        //                Id = x.SupplierId,
        //                Name = x.SupplierName,
        //                Products = x.EfProducts.Select(y => new Product()
        //                {
        //                    Id = y.ProductId,
        //                    Name = y.ProductName
        //                })
        //            });

        //        return returnValue;
        //    }
        //    else
        //    {
        //        var returnValue = context.EfSuppliers
        //           .AsNoTracking()
        //           .Select(x => new Supplier()
        //           {
        //               Id = x.SupplierId,
        //               Name = x.SupplierName
        //           });

        //        return returnValue;
        //    }
        //}
        [EnableQuery]
        private List<Product> MyProductList()
        {
            List<Product> list = new List<Product>();
            list.Add(new Product { ID = 1, Name = "Ashley", Price = 100 });
            list.Add(new Product { ID = 2, Name = "Ashley", Price = 100 });
            list.Add(new Product { ID = 3, Name = "Ashley", Price = 100 });
            list.Add(new Product { ID = 4, Name = "Ashley", Price = 100 });
            list.Add(new Product { ID = 5, Name = "Ashley", Price = 100 });
            return list;

        }

        // GET: odata/Products(5)
        public IHttpActionResult GetProduct([FromODataUri] int key, ODataQueryOptions<Product> queryOptions)
        {
            // validate the query.
            try
            {
                queryOptions.Validate(_validationSettings);
            }
            catch (ODataException ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok<Product>(MyProductList().FirstOrDefault(a=> a.ID == key));
            //return StatusCode(HttpStatusCode.NotImplemented);
        }

        // PUT: odata/Products(5)
        public IHttpActionResult Put([FromODataUri] int key, Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (key != product.ID)
            {
                return BadRequest();
            }

            // TODO: Add replace logic here.

            // return Updated(product);
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // POST: odata/Products
        public IHttpActionResult Post(Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // TODO: Add create logic here.

            // return Created(product);
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // PATCH: odata/Products(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Product> delta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // TODO: Get the entity here.

            // delta.Patch(product);

            // TODO: Save the patched entity.

            // return Updated(product);
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // DELETE: odata/Products(5)
        public IHttpActionResult Delete([FromODataUri] int key)
        {
            // TODO: Add delete logic here.

            // return StatusCode(HttpStatusCode.NoContent);
            return StatusCode(HttpStatusCode.NotImplemented);
        }
    }

    public static class ODataQueryOptionsExtensions
    {
        public static bool IsNavigationPropertyExpected<TSource, TKey>(this ODataQueryOptions<TSource> source, Expression<Func<TSource, TKey>> keySelector)
        {
            if (source == null) { throw new ArgumentNullException("source"); }
            if (keySelector == null) { throw new ArgumentNullException("keySelector"); }

            var returnValue = false;
            var propertyName = (keySelector.Body as MemberExpression ?? ((UnaryExpression)keySelector.Body).Operand as MemberExpression).Member.Name;
            var expandProperties = source.SelectExpand == null || string.IsNullOrWhiteSpace(source.SelectExpand.RawExpand) ? new List<string>().ToArray() : source.SelectExpand.RawExpand.Split(',');
            var selectProperties = source.SelectExpand == null || string.IsNullOrWhiteSpace(source.SelectExpand.RawSelect) ? new List<string>().ToArray() : source.SelectExpand.RawSelect.Split(',');

            returnValue = returnValue ^ expandProperties.Contains<string>(propertyName);
            returnValue = returnValue ^ selectProperties.Contains<string>(propertyName);

            return returnValue;
        }
    }
}

