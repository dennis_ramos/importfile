﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using System.Web.Http.OData.Query;

using ConXApiDtoRepository;
using Microsoft.OData.Core;
using System.Web.Http.OData.Query.Validators;
using Microsoft.Data.OData.Query.SemanticAst;
using System.Web;

namespace ConxODataWebApi.Controllers
{

    public class JobFilterQueryValidator : FilterQueryValidator
    {
        static readonly string[] allowedProperties = { "JobNo", "COReferenceNo", "WorkStream", "DeviceName", "JobStatus", "WorkFlowName","TechUserName","Group1","Group2", "Group3", "Outcome", "StatusBy", "StatusOn", "CompletedOn","ModifiedOn","ModifiedBy" };

        public override void ValidateSingleValuePropertyAccessNode(
            SingleValuePropertyAccessNode propertyAccessNode,
            ODataValidationSettings settings)
        {
            string propertyName = null;
            if (propertyAccessNode != null)
            {
                propertyName = propertyAccessNode.Property.Name;
            }

            if (propertyName != null && !allowedProperties.Contains(propertyName))
            {
                throw new ODataException(
                    String.Format("Filter on {0} not allowed", propertyName));
            }
            base.ValidateSingleValuePropertyAccessNode(propertyAccessNode, settings);
        }
    }

    [Authorize]
    public class JobsController : ODataController
    {
        

        private static ODataValidationSettings _validationSettings = new ODataValidationSettings() 
        {AllowedQueryOptions = 
            AllowedQueryOptions.Expand | 
            AllowedQueryOptions.Filter | 
            AllowedQueryOptions.Select | 
            AllowedQueryOptions.Skip |
            AllowedQueryOptions.Top ,
        AllowedLogicalOperators = 
            AllowedLogicalOperators.Equal | 
            AllowedLogicalOperators.And |
            AllowedLogicalOperators.GreaterThan |
            AllowedLogicalOperators.GreaterThanOrEqual |
            AllowedLogicalOperators.LessThan |
            AllowedLogicalOperators.LessThanOrEqual,
        AllowedArithmeticOperators = 
            AllowedArithmeticOperators.None,
        AllowedFunctions = 
            AllowedFunctions.AllDateTimeFunctions 
            };

        private JobDtoModel db = new JobDtoModel();

        // GET: odata/Jobs
        //public IQueryable<Job> GetJobs(ODataQueryOptions<Job> queryOptions)
        [EnableQuery(PageSize = 50)]
        public IHttpActionResult GetJobs(ODataQueryOptions<Job> queryOptions)
        {
            string userFSP = HttpContext.Current.User.Identity.Name.Split(new char[] { '!' })[0];
            
            try
            {
                queryOptions.Filter.Validator = new JobFilterQueryValidator();
                queryOptions.Validate(_validationSettings);
            }
            catch (ODataException ex)
            {
                return BadRequest(ex.Message);
            }

            var jobs = db.Jobs.Where(a => a.FSP == userFSP);
            return Ok(jobs);
        }
        
        // GET: odata/Jobs(5)
        public SingleResult<Job> GetJob([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.Jobs.Where(job => job.JobID == key));
        }

        // PUT: odata/Jobs(5)
        public async Task<IHttpActionResult> Put([FromODataUri] Guid key, Job job)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (key != job.JobID)
            {
                return BadRequest();
            }

            db.Entry(job).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JobExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(job);
        }

        // POST: odata/Jobs
        public async Task<IHttpActionResult> Post(Job job)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Jobs.Add(job);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (JobExists(job.JobID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Created(job);
        }

        // PATCH: odata/Jobs(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] Guid key, Delta<Job> patch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Job job = await db.Jobs.FindAsync(key);
            if (job == null)
            {
                return NotFound();
            }

            patch.Patch(job);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JobExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(job);
        }

        // DELETE: odata/Jobs(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] Guid key)
        {
            Job job = await db.Jobs.FindAsync(key);
            if (job == null)
            {
                return NotFound();
            }

            db.Jobs.Remove(job);
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/Jobs(5)/JobAddresses
        [Queryable]
        public IQueryable<JobAddress> GetJobAddresses([FromODataUri] Guid key)
        {
            return db.Jobs.Where(m => m.JobID == key).SelectMany(m => m.JobAddresses);
        }

        // GET: odata/Jobs(5)/JobAssets
        [Queryable]
        public IQueryable<JobAsset> GetJobAssets([FromODataUri] Guid key)
        {
            return db.Jobs.Where(m => m.JobID == key).SelectMany(m => m.JobAssets);
        }

        // GET: odata/Jobs(5)/JobAttributes
        [Queryable]
        public IQueryable<JobAttribute> GetJobAttributes([FromODataUri] Guid key)
        {
            return db.Jobs.Where(m => m.JobID == key).SelectMany(m => m.JobAttributes);
        }

        // GET: odata/Jobs(5)/JobContacts
        [Queryable]
        public IQueryable<JobContact> GetJobContacts([FromODataUri] Guid key)
        {
            return db.Jobs.Where(m => m.JobID == key).SelectMany(m => m.JobContacts);
        }

        // GET: odata/Jobs(5)/JobGPSs
        [Queryable]
        public IQueryable<JobGPS> GetJobGPSs([FromODataUri] Guid key)
        {
            return db.Jobs.Where(m => m.JobID == key).SelectMany(m => m.JobGPSs);
        }

        // GET: odata/Jobs(5)/JobNotes
        [Queryable]
        public IQueryable<JobNote> GetJobNotes([FromODataUri] Guid key)
        {
            return db.Jobs.Where(m => m.JobID == key).SelectMany(m => m.JobNotes);
        }

        // GET: odata/Jobs(5)/JobPhotos
        [Queryable]
        public IQueryable<JobPhoto> GetJobPhotos([FromODataUri] Guid key)
        {
            return db.Jobs.Where(m => m.JobID == key).SelectMany(m => m.JobPhotos);
        }

        // GET: odata/Jobs(5)/JobSchedule
        [Queryable]
        public SingleResult<JobSchedule> GetJobSchedule([FromODataUri] Guid key)
        {
            return SingleResult.Create(db.Jobs.Where(m => m.JobID == key).Select(m => m.JobSchedule));
        }

        // GET: odata/Jobs(5)/JobTasks
        [Queryable]
        public IQueryable<JobTask> GetJobTasks([FromODataUri] Guid key)
        {
            return db.Jobs.Where(m => m.JobID == key).SelectMany(m => m.JobTasks);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool JobExists(Guid key)
        {
            return db.Jobs.Count(e => e.JobID == key) > 0;
        }
    }
}
