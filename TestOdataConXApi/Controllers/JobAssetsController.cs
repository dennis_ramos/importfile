﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
//using System.Web.Http.OData;
//using System.Web.Http.OData.Query;
//using System.Web.Http.OData.Routing;
using System.Web.OData.Routing;

using ConxODataWebApi.Models;
//using Microsoft.Data.OData;
using System.Web.OData;
using System.Web.OData.Query;
using Microsoft.OData.Core;
using System.Threading.Tasks;

namespace ConxODataWebApi.Controllers
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using TestOdataConXApi.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<JobAsset>("JobAssets");
    config.Routes.MapODataRoute("odata", "odata", builder.GetEdmModel());
    */
    //[Authorize]
    
    public class JobAssetsController : ODataController
    {
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        // GET: odata/JobAssets
        //[EnableQuery]
        
        //public IHttpActionResult GetJobAssets(ODataQueryOptions<JobAsset> queryOptions)
        //{
        //    // validate the query.
        //    try
        //    {
        //        queryOptions.Validate(_validationSettings);
        //    }
        //    catch (ODataException ex)
        //    {
        //        return BadRequest(ex.Message);
        //    }

        //    var ddd = queryOptions.Filter;



        //    return Ok(SeedJobAssets());
        //    //return StatusCode(HttpStatusCode.NotImplemented);
        //}

        //[ODataRoute("JobAssets({id})/d.GetAssetbyAssetNameSerialNumber(AssetType={assetType},AssetNumber={assetNumber})")]

        
        //[ODataRoute("d.GetAssetbyAssetNameSerialNumber(AssetType={assetType},AssetNumber={assetNumber})")]
        [ODataRoute("JobAssets({id})/d.GetAssetbyAssetNameSerialNumber(AssetType={assetType},AssetNumber={assetNumber})")]
        [EnableQuery(PageSize = 20, AllowedQueryOptions = AllowedQueryOptions.All)]
        [HttpGet]
        public IHttpActionResult GetAssetbyAssetNameSerialNumber([FromODataUri] string assetType, [FromODataUri] string assetNumber, ODataQueryOptions<JobAsset> queryOptions)
        {

            return Ok(SeedJobAssets().Where(a=> a.AssetType == assetType && a.AssetValueOrSerialNo == assetNumber));
        }
        //[EnableQuery]
        //[ODataRoute("(AssetType={assetType}, AssetValueOrSerialNo={assetValueOrSerialNo})")]
        //public IHttpActionResult GetJobAssets([FromODataUri] string assetType, [FromODataUri] string assetValueOrSerialNo, ODataQueryOptions<JobAsset> queryOptions)
        //{
            
        //    // validate the query.
        //    try
        //    {
        //        queryOptions.Validate(_validationSettings);
        //    }
        //    catch (ODataException ex)
        //    {
        //        return BadRequest(ex.Message);
        //    }

        //    var ddd = queryOptions.Filter;



        //    return Ok(SeedJobAssets());
        //    //return StatusCode(HttpStatusCode.NotImplemented);
        //}

        
        public List<JobAsset> SeedJobAssets()
        {
            List<JobAsset> primaryAssetList = new List<JobAsset>();
            JobAsset aa =  new JobAsset { AssetID = "a", AssetType = "Old Meter", AssetValueOrSerialNo = "Serial A", AppendAssetValueToTaskName = true, ParentAssetID = null, SubAssets = new List<JobAsset>() };
            aa.SubAssets.Add(new JobAsset { AssetID = "a1", AssetType = "Register", AssetValueOrSerialNo = "Serial A-Reg 1", AppendAssetValueToTaskName = true, ParentAssetID = "a", SubAssets = new List<JobAsset>() });
            aa.SubAssets.Add(new JobAsset { AssetID = "a2", AssetType = "Register", AssetValueOrSerialNo = "Serial A-Reg 2", AppendAssetValueToTaskName = true, ParentAssetID = "a", SubAssets = new List<JobAsset>() });
            primaryAssetList.Add(aa);

            JobAsset ab = new JobAsset { AssetID = "b", AssetType = "Old Meter", AssetValueOrSerialNo = "Serial B", AppendAssetValueToTaskName = true, ParentAssetID = null, SubAssets = new List<JobAsset>() };
            ab.SubAssets.Add(new JobAsset { AssetID = "b1", AssetType = "Register", AssetValueOrSerialNo = "Serial B-Reg 1", AppendAssetValueToTaskName = true, ParentAssetID = "b", SubAssets = new List<JobAsset>() });
            ab.SubAssets.Add(new JobAsset { AssetID = "b2", AssetType = "Register", AssetValueOrSerialNo = "Serial B-Reg 2", AppendAssetValueToTaskName = true, ParentAssetID = "b", SubAssets = new List<JobAsset>() });
            primaryAssetList.Add(ab);

            return primaryAssetList;

        }

        // GET: odata/JobAssets(5)
         [EnableQuery]
        public IHttpActionResult GetJobAsset([FromODataUri] string key, ODataQueryOptions<JobAsset> queryOptions)
        {
            // validate the query.
            try
            {
                queryOptions.Validate(_validationSettings);
            }
            catch (ODataException ex)
            {
                return BadRequest(ex.Message);
            }
            var ddd = queryOptions.Filter;
             JobAsset j = SeedJobAssets().FirstOrDefault(a=> a.AssetID == key);
             
            return Ok<JobAsset>(j);
            //return StatusCode(HttpStatusCode.NotImplemented);
        }

        // PUT: odata/JobAssets(5)
        public IHttpActionResult Put([FromODataUri] string key, JobAsset jobAsset)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (key != jobAsset.AssetID)
            {
                return BadRequest();
            }

            // TODO: Add replace logic here.

            // return Updated(jobAsset);
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // POST: odata/JobAssets
        public IHttpActionResult Post(JobAsset jobAsset)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // TODO: Add create logic here.

            // return Created(jobAsset);
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // PATCH: odata/JobAssets(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] string key, Delta<JobAsset> delta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // TODO: Get the entity here.

            // delta.Patch(jobAsset);

            // TODO: Save the patched entity.

            // return Updated(jobAsset);
            return StatusCode(HttpStatusCode.NotImplemented);
        }

        // DELETE: odata/JobAssets(5)
        public IHttpActionResult Delete([FromODataUri] string key)
        {
            // TODO: Add delete logic here.

            // return StatusCode(HttpStatusCode.NoContent);
            return StatusCode(HttpStatusCode.NotImplemented);
        }
    }
}
