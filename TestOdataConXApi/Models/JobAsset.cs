﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ConxODataWebApi.Models
{
    public class JobAssetXXX
    {
        [Key]
        public string AssetID { get; set; }

        public string AssetValueOrSerialNo { get; set; }

        [Required]
        public string AssetType { get; set; }
        public bool AppendAssetValueToTaskName { get; set; }
        public string ParentAssetID { get; set; }

        //public DateTime ModifiedOn { get; set; }
        //public string ModifiedBy { get; set; }
        public IList<JobAssetXXX> SubAssets { get; set; }
        
    }
}