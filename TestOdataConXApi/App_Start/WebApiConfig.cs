﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;

using ConxODataWebApi.Models;
using ConXApiDtoModels;
//using ConXApiDtoRepository;

using Microsoft.OData.Edm;
using System.Web.OData.Builder;
using System.Web.OData.Extensions;
using ConXApiDtoModels.DataCreationRequest;





namespace ConxODataWebApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.

            //builder.EntityType<JobAsset>().Collection
            //    .Function("GetAssetbyAssetNameSerialNumber")
            //     .Parameter<string>("assetName")

            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling =
                Newtonsoft.Json.ReferenceLoopHandling.Serialize; 

            config.Formatters.JsonFormatter.SerializerSettings.PreserveReferencesHandling =
                Newtonsoft.Json.PreserveReferencesHandling.Objects; 

            config.SuppressDefaultHostAuthentication();
            config.EnableCaseInsensitive(caseInsensitive: true);
            config.EnableUnqualifiedNameCall(unqualifiedNameCall: true);


            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            builder.EntitySet<Product>("Products");
            
            //builder.EntitySet<ConxODataWebApi.Models.JobAsset>("JobAssets");
            builder.Namespace = "d";
            
            //var assets = builder.EntityType<ConxODataWebApi.Models.JobAsset>();
            //var function = assets.Function("GetAssetbyAssetNameSerialNumber");
            //function.Parameter<string>("AssetType");
            //function.Parameter<string>("AssetNumber");
            //function.ReturnsCollectionFromEntitySet<ConxODataWebApi.Models.JobAsset>("JobAssets");
            
            
            builder.EntitySet<Job>("Jobs");

            //
            //builder.EntitySet<JobAddress>("JobAddresses"); 
            //builder.EntitySet<JobAsset>("JobAssets");
            //builder.EntitySet<JobAttribute>("JobAttributes");
            //builder.EntitySet<JobContact>("JobContacts");
            //builder.EntitySet<JobGPS>("JobGPSs");
            //builder.EntitySet<JobNote>("JobNotes");
            //builder.EntitySet<JobPhoto>("JobPhotos");
            //builder.EntitySet<JobSchedule>("JobSchedules");
            //builder.EntitySet<JobTask>("JobTasks"); 
              
            //


            builder.Namespace = "d";

            
            EntitySetConfiguration<Job> jobs = builder.EntitySet<Job>("Jobs");
            FunctionConfiguration getJobsByJobNo = jobs.EntityType.Collection.Function("GetJobsByJobNumber");
            getJobsByJobNo.Parameter<string>("JobNo");
            getJobsByJobNo.ReturnsCollectionFromEntitySet<Job>("Job");


            //EntitySetConfiguration<Job> jobs = builder.EntitySet<Job>("Jobs");
            FunctionConfiguration getJobsByWorkStreamName = jobs.EntityType.Collection.Function("GetJobsByWorkStreamName");
            getJobsByWorkStreamName.Parameter<string>("WorkStreamName");
            getJobsByWorkStreamName.Parameter<string>("JobStatus");
            getJobsByWorkStreamName.Parameter<DateTime>("ModifiedFrom");
            getJobsByWorkStreamName.Parameter<DateTime>("ModifiedTo");
            getJobsByWorkStreamName.ReturnsCollectionFromEntitySet<Job>("Job");


            FunctionConfiguration getJobsByWorkFlowName = jobs.EntityType.Collection.Function("GetJobsByWorkFlow");
            getJobsByWorkFlowName.Parameter<string>("WorkFlowName");
            getJobsByWorkFlowName.Parameter<string>("JobStatus");
            getJobsByWorkFlowName.Parameter<DateTime>("ModifiedFrom");
            getJobsByWorkFlowName.Parameter<DateTime>("ModifiedTo");
            getJobsByWorkFlowName.ReturnsCollectionFromEntitySet<Job>("Job");

            FunctionConfiguration createJob = jobs.EntityType.Collection.Function("CreateJob");
            
            createJob.Returns<IHttpActionResult>();


            config.MapODataServiceRoute(
                routeName: "odata",
                routePrefix: "odata",
                model: builder.GetEdmModel()
                );
            

        }

    }
}
