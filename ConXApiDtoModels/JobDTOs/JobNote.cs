namespace ConXApiDtoModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class JobNote
    {
        [Key]
        [Column(Order = 0)]
        public Guid JobNoteID { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid JobID { get; set; }

        [StringLength(50)]
        public string NoteType { get; set; }

        [StringLength(1000)]
        public string Note { get; set; }

        public DateTime? NoteOn { get; set; }

        [StringLength(50)]
        public string NoteBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        [StringLength(15)]
        public string ModifiedBy { get; set; }

        
    }
}
