namespace ConXApiDtoModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    
    public partial class JobTaskItem
    {
        [Key]
        [Column(Order = 0)]
        public Guid JobTaskItemID { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid JobTaskID { get; set; }

        [Key]
        [Column(Order = 2)]
        public Guid TaskItemID { get; set; }

        [StringLength(50)]
        public string TaskItemName { get; set; }

        [StringLength(50)]
        public string Result { get; set; }

        [StringLength(50)]
        public string ValidationResult { get; set; }

        [StringLength(50)]
        public string EnteredBy { get; set; }

        public DateTime? EnteredOn { get; set; }

        [StringLength(15)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public short? TaskItemOrder { get; set; }

        
    }
}
