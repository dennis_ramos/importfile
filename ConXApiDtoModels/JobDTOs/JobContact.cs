namespace ConXApiDtoModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class JobContact
    {
        public JobContact()
        {
            JobContactDetails = new List<JobContactDetail>();
        }

        [Key]
        [Column(Order = 0)]
        public Guid JobContactID { get; set; }

        
        [Column(Order = 1)]
        public Guid JobID { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string MiddleName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(15)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        [StringLength(50)]
        public string ContactSource { get; set; }

        public int RowId { get; set; }
        public bool HasError { get; set; }
        public string ErrorDescription { get; set; }
        public string JobRef { get; set; }

        public IList<JobContactDetail> JobContactDetails { get; set; }


    }
}
