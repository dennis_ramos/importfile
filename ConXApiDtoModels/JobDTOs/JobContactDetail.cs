namespace ConXApiDtoModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class JobContactDetail
    {
        [Key]
        [Column(Order = 0)]
        public Guid JobContactPhoneID { get; set; }

        [Column(Order = 1)]
        public Guid JobContactID { get; set; }

        [StringLength(50)]
        public string ContactType { get; set; }

        [StringLength(200)]
        public string ContactValue { get; set; }

        [StringLength(15)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public bool? PreferredContact { get; set; }

        
        
    }
}
