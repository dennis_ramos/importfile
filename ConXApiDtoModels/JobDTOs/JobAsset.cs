namespace ConXApiDtoModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    
    public partial class JobAsset
    {
        public JobAsset()
        {
            JobAssetAttributes = new List<JobAssetAttribute>();
            JobTasks = new List<JobTask>();
        }

        [Key]
        [Column(Order = 0)]
        public Guid JobAssetID { get; set; }

        [Column(Order = 1)]
        public Guid JobID { get; set; }

        [StringLength(50)]
        public string AssetType { get; set; }

        [StringLength(50)]
        public string AssetValueOrSerialNo { get; set; }

        public DateTime? ModifiedOn { get; set; }

        [StringLength(15)]
        public string ModifiedBy { get; set; }

        public IList<JobAssetAttribute> JobAssetAttributes { get; set; }
        public IList<JobTask> JobTasks { get; set; }
    }
}
