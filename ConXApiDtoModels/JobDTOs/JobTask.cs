namespace ConXApiDtoModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class JobTask
    {
        public JobTask()
        {
            SubTasks = new List<JobTask>();
            JobTaskItems = new List<JobTaskItem>();
            JobTaskNotes = new List<JobTaskNote>();
            JobTaskPhotos = new List<JobTaskPhoto>();
        }

        [Key]
        [Column(Order = 0)]
        public Guid JobTaskID { get; set; }
        
        [Column(Order = 1)]
        public Guid JobID { get; set; }

        public Guid? TaskID { get; set; }

        [StringLength(50)]
        public string TaskType { get; set; }

        [StringLength(50)]
        public string TaskName { get; set; }

        public short? TaskOrder { get; set; }

        [StringLength(50)]
        public string TaskDescription { get; set; }

        [StringLength(50)]
        public string TaskStatus { get; set; }

        [StringLength(50)]
        public string TaskQualifier { get; set; }

        public bool? IsTaskQualifierForced { get; set; }

        [StringLength(50)]
        public string CompletedBy { get; set; }

        public DateTime? CompletedOn { get; set; }

        public Guid? JobAssetID { get; set; }

        [StringLength(15)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedOn { get; set; }

        public Guid? ParentJobTaskID { get; set; }

        public virtual Job Job { get; set; } 

        public IList<JobTask> SubTasks { get; set; }
        public IList<JobTaskItem> JobTaskItems { get; set; }
        public IList<JobTaskNote> JobTaskNotes { get; set; }
        public IList<JobTaskPhoto> JobTaskPhotos { get; set; }

    }
}
