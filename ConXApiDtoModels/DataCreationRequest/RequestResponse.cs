﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConXApiDtoModels.DataCreationRequest
{
    public class RequestResponse: BaseRequest
    {
        public int RequestID { get; set; }
        public DateTime? ProcessedOn { get; set; }
        public DateTime? CompletedOn { get; set; }
        public string Notes { get; set; }
        public string RequestStatus { get; set; }
        public IList<JobRequestResponse> JobRequestResponses { get; set; }
    }
}
