﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConXApiDtoModels.DataCreationRequest
{
    public class JobTaskItem
    {
        public int TaskItemOrder { get; set; }
        public string TaskItemName { get; set; }
        public string TaskItemValue { get; set; }
    }
}
