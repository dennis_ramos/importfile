﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConXApiDtoModels.DataCreationRequest
{
    public class JobAttribute: Attribute
    {
        public string TaskNameToPrePopulate { get; set; }
        public string TaskItemNameToPrePopulate { get; set; }
        public int? TaskItemOrderToPrePopulate { get; set; }
    }
}
