﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConXApiDtoModels.DataCreationRequest
{
    
    public class Note
    {
        public string NoteType { get; set; }
        public string NoteMessage { get; set; }
    }

    public class TaskNote : Note
    {
        public string TaskName { get; set; }
        public string ParentTaskName { get; set; }
    }
}
