﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConXApiDtoModels.DataCreationRequest
{
    public class JobAssetAttribute : Attribute
    {
        public int? TaskItemOrderToPrePopulate { get; set; }
        public string TaskItemNameToPrePopulate { get; set; }
    }
}
