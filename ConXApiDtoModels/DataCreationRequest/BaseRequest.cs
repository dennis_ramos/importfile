﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConXApiDtoModels.DataCreationRequest
{
    public abstract class BaseRequest
    {
        public string FSP { get; set; }
        public string RequestType { get; set; }
        public string RequestedBy { get; set; }
        public DateTime RequestedOn { get; set; }
        // Filename of job file to process
       // public string FileToProcess { get; set; }

    }
}
