﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConXApiDtoModels.DataCreationRequest
{
    public class JobAsset
    {
        public string AssetType { get; set; }
        public string AssetValueOrSerialNo { get; set; }
        public bool PopulateTaskDescriptionWithAssetValue { get; set; }
        public IList<Note> Notes { get; set; }
        public IList<JobAssetAttribute> AssetAttributes { get; set; }
        public IList<JobAsset> SubAssets { get; set; }
        public IList<JobTaskItem> JobTaskItemPrePopulations { get; set; }

        public JobAsset()
        {
            Notes = new List<Note>();
            AssetAttributes = new List<JobAssetAttribute>();
            SubAssets = new List<JobAsset>();
            JobTaskItemPrePopulations = new List<JobTaskItem>();
        }
    }
}
