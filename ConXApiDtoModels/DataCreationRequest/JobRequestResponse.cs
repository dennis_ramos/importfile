﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConXApiDtoModels.DataCreationRequest
{
    public class JobRequestResponse
    {
        public string JobReference { get; set; }
        public bool Processed { get; set; }
        public DateTime? ProcessedOn { get; set; }
        public bool? HasError { get; set; }
        public string ErrorDescription { get; set; }
        public IList<JobRequestResponseDetail> JobRequestResponseDetail { get; set; }
    }

}
