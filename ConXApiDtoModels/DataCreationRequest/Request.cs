﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConXApiDtoModels.DataCreationRequest
{
    public class Request: BaseRequest
    {
        public IList<JobRequest> JobRequests { get; set; }

        public Request()
        {
            RequestedOn = DateTime.Now;
        }

    }

}
