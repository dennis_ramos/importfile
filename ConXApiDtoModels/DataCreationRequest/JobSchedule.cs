﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConXApiDtoModels.DataCreationRequest
{
    public class JobSchedule
    {
        public DateTime? PlannedFrom { get; set; }
        public DateTime? PlannedTo { get; set; }
        public string TechUserName { get; set; }
        public bool? RequiresAppointment { get; set; }
        public string ScheduleType { get; set; }
        public bool CreateMultipleInstances { get; set; }
        public int? NumberOfOccurrences { get; set; }
        public int? Frequency { get; set; }
        public bool? AssignToDevice { get; set; }

    }
}
