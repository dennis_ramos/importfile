﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConXApiDtoModels.DataCreationRequest
{
    public class JobRequestResponseDetail
    {
        public Guid JobId { get; set; }
        public string JobNo { get; set; }
        public string Uri { get; set; }
    }
}
