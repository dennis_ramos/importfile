﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConXApiDtoModels.DataCreationRequest
{
    public class UpdateJobStatusRequest : BaseRequest
    {
        public string JobNo { get; set; }
        public string FSP { get; set; }
        public float JobStatus { get; set; }
    }
}
