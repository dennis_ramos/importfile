﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConXApiDtoModels.DataCreationRequest
{
    public class JobRequest
    {

        public string JobReference { get; set; }
        public string WorkStream { get; set; }
        public string Description { get; set; }
        public string Group1 { get; set; }
        public string Group2 { get; set; }
        public string Group3 { get; set; }
        public int? JobSequence { get; set; }
        public string JobType { get; set; }
        public string Region { get; set; }
        public string ImportFileJobRef { get; set; }

        public IList<JobAddress> JobAddresses { get; set; }
        public IList<JobContact> JobContacts { get; set; }
        public IList<JobAsset> JobAssets { get; set; }
        public IList<JobAttribute> JobAttributes { get; set; }
        public IList<NonAssetBasedTaskPrePopulation> NonAssetBasedTaskPrePopulations { get; set; }
        public IList<Note> JobNotes { get; set; }
        public JobSchedule JobSchedule { get; set; }
        public IList<TaskNote> JobTaskNotes { get; set; }

    }
}
