﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConXApiDtoModels.DataCreationRequest
{
    public class JobContact
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string ContactDescription { get; set; }
        public IList<JobContactDetail> JobContactDetails { get; set; }
        public JobContact()
        {
            FirstName = "";
            MiddleName = "";
            LastName = "";
            ContactDescription = "";
        }
    }
}
