﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConXApiDtoModels.DataCreationRequest
{
    public class NonAssetBasedTaskPrePopulation
    {
        public string TaskName { get; set; }
        public List<JobTaskItem> JobTaskItems { get; set; }
    }
}
