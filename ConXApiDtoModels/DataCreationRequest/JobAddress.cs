﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConXApiDtoModels.DataCreationRequest
{
    public class JobAddress
    {
        public bool PrimaryAddress { get; set; }
        public string AddressDescription { get; set; }
        public string StreetNumber { get; set; }
        public string FlatAppartment { get; set; }
        public string StreetName { get; set; }
        public string Suburb { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string Country { get; set; }
        public string Region { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        //public string Lat { get; set; }
        //public string Lon { get; set; }
        
    }
}
