﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConXApiDtoModels.DataCreationRequest
{
    public class JobContactDetail
    {
        public string ContactType { get; set; }
        public string ContactValue { get; set; }
        public bool? PreferredContact { get; set; }
    }
}
