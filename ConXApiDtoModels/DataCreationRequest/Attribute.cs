﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConXApiDtoModels.DataCreationRequest
{
    public abstract class Attribute
    {
        public string AttributeName { get; set; }
        public string AttributeValue { get; set; }
    }
}
