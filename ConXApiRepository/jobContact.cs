namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("jobContacts")]
    public partial class JobContact
    {
        public JobContact()
        {
            JobContactPhoneDetails = new HashSet<jobContactPhoneDetail>();
        }

        public Guid JobContactID { get; set; }

        public Guid JobID { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string MidName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(50)]
        public string Phone { get; set; }

        [StringLength(50)]
        public string Mobile { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        [StringLength(50)]
        public string Fax { get; set; }

        public float? enumContactSource { get; set; }

        public bool wadtDeleted { get; set; }

        [StringLength(15)]
        public string wadtModifiedBy { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        [StringLength(6)]
        public string wadtModifiedOnDTOffset { get; set; }

        public Guid? wadtRowID { get; set; }

        public Guid rowguid { get; set; }

        public virtual ICollection<jobContactPhoneDetail> JobContactPhoneDetails { get; set; }

        public virtual JobJob jobJob { get; set; }
    }
}
