namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cfgUsersInGroups")]
    public partial class CfgUsersInGroup
    {
        public Guid UserId { get; set; }

        public Guid UserGroupId { get; set; }

        [Key]
        public Guid rowguid { get; set; }

        public virtual CfgUserGroup cfgUserGroup { get; set; }

        public virtual CfgUser cfgUser { get; set; }
    }
}
