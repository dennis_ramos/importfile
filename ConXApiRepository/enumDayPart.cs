namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("enumDayPart")]
    public partial class EnumDayPart
    {
        public EnumDayPart()
        {
            jobJobs = new HashSet<JobJob>();
        }

        [Key]
        [Column("enumDayPart")]
        public float enumDayPart { get; set; }

        [StringLength(50)]
        public string enumDesc { get; set; }

        [StringLength(10)]
        public string enumSDesc { get; set; }

        public virtual ICollection<JobJob> jobJobs { get; set; }
    }
}
