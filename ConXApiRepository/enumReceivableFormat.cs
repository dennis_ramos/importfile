﻿namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("enumReceivableFormat")]
    public partial class EnumReceivableFormat
    {
        public EnumReceivableFormat()
        {
            cfgContractReceivables = new HashSet<CfgContractReceivable>();
        }

        [Key]
        [Column("enumReceivableFormat")] 
        public float enumReceivableFormat { get; set; }

        [StringLength(50)]
        public string enumDesc { get; set; }

        [StringLength(10)]
        public string enumSDesc { get; set; }

        public virtual ICollection<CfgContractReceivable> cfgContractReceivables { get; set; }
    }
}
