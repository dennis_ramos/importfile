namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cfgValidValueListItems")]
    public partial class CfgValidValueListItem
    {
        [Key]
        public Guid ValidValueListItemID { get; set; }

        public Guid ValidValueListID { get; set; }

        [Required]
        [StringLength(50)]
        public string Category { get; set; }

        [Required]
        [StringLength(50)]
        public string ItemText { get; set; }

        public bool wadtDeleted { get; set; }

        [StringLength(15)]
        public string wadtModifiedBy { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        [StringLength(6)]
        public string wadtModifiedOnDTOffset { get; set; }

        public Guid? wadtRowID { get; set; }

        [StringLength(200)]
        public string Description { get; set; }

        [StringLength(200)]
        public string ItemDescription { get; set; }

        public Guid rowguid { get; set; }

        public Guid? ParentValidValueListItemID { get; set; }

        public virtual CfgValidValueList cfgValidValueList { get; set; }
    }
}
