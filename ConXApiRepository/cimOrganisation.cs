namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cimOrganisations")]
    public partial class CimOrganisation
    {
        public CimOrganisation()
        {
            cimContacts = new HashSet<CimContact>();
            jobJobs = new HashSet<JobJob>();
        }

        [Key]
        public Guid OrgID { get; set; }

        public Guid? RootOrgID { get; set; }

        public float enumOrgType { get; set; }

        [StringLength(50)]
        public string OrganisationName { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        public float enumActive { get; set; }

        public bool wadtDeleted { get; set; }

        [StringLength(15)]
        public string wadtModifiedBy { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        [StringLength(6)]
        public string wadtModifiedOnDTOffset { get; set; }

        public Guid? wadtRowID { get; set; }

        [StringLength(10)]
        public string FSP { get; set; }

        public Guid rowguid { get; set; }

        public virtual CfgFSP cfgFSP { get; set; }

        public virtual ICollection<CimContact> cimContacts { get; set; }

        public virtual ICollection<JobJob> jobJobs { get; set; }
    }
}
