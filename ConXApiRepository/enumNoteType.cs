namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("enumNoteType")]
    public partial class EnumNoteType
    {
        public EnumNoteType()
        {
            jobNotes = new HashSet<JobNote>();
        }

        [Key]
        [Column("enumNoteType")]
        public float enumNoteType { get; set; }

        [StringLength(50)]
        public string enumDesc { get; set; }

        [StringLength(10)]
        public string enumSDesc { get; set; }

        public Guid rowguid { get; set; }

        public virtual ICollection<JobNote> jobNotes { get; set; }
    }
}
