namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cfgWorkFlow")]
    public partial class WorkFlow
    {
        public WorkFlow()
        {
            cfgContracts = new HashSet<CfgContract>();
            WorkFlowTasks = new HashSet<WorkFlowTask>();
            JobJobs = new HashSet<JobJob>();
        }

        [Key]
        public Guid WorkFlowID { get; set; }

        [StringLength(10)]
        public string FSP { get; set; }

        [StringLength(50)]
        public string WorkFlowName { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        public float? enumActive { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(6)]
        public string CreatedOnOffset { get; set; }

        public bool wadtDeleted { get; set; }

        [StringLength(15)]
        public string wadtModifiedBy { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        [StringLength(6)]
        public string wadtModifiedOnDTOffset { get; set; }

        public Guid? wadtRowID { get; set; }

        public Guid? vvlidFieldOutcome { get; set; }

        public Guid? vvlidFieldOutcomeReason { get; set; }

        public Guid rowguid { get; set; }

        public int? wfDuration { get; set; }

        public virtual ICollection<CfgContract> cfgContracts { get; set; }

        public virtual CfgFSP cfgFSP { get; set; }

        public virtual ICollection<WorkFlowTask> WorkFlowTasks { get; set; }

        public virtual ICollection<JobJob> JobJobs { get; set; }
    }
}
