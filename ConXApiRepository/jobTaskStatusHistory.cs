namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("jobTaskStatusHistory")]
    public partial class JobTaskStatusHistory
    {
        public Guid id { get; set; }

        public Guid JobTaskID { get; set; }

        public DateTime? StatusOn { get; set; }

        [StringLength(50)]
        public string StatusBy { get; set; }

        public float? enumTaskStatus { get; set; }

        public virtual JobTask jobTask { get; set; }
    }
}
