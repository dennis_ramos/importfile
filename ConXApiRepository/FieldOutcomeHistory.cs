namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FieldOutcomeHistory")]
    public partial class FieldOutcomeHistory
    {
        public Guid id { get; set; }

        public Guid JobID { get; set; }

        [StringLength(50)]
        public string Outcome { get; set; }

        [StringLength(100)]
        public string Reason { get; set; }

        public DateTime? OutcomeOn { get; set; }

        [StringLength(50)]
        public string OutcomeBy { get; set; }

        public virtual JobJob jobJob { get; set; }
    }
}
