namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("jobStatusHistory")]
    public partial class JobStatusHistory
    {
        public Guid id { get; set; }

        public Guid JobID { get; set; }

        public DateTime? StatusOn { get; set; }

        [StringLength(50)]
        public string StatusBy { get; set; }

        public float? enumJobStatus { get; set; }

        public virtual JobJob JobJob { get; set; }
    }
}
