namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("enumPhoneType")]
    public partial class EnumPhoneType
    {
        public EnumPhoneType()
        {
            jobContactPhoneDetailes = new HashSet<jobContactPhoneDetail>();
        }

        [Key]
        [Column("enumPhoneType")]
        public float enumPhoneType { get; set; }

        [StringLength(50)]
        public string enumDesc { get; set; }

        [StringLength(10)]
        public string enumSDesc { get; set; }

        public Guid rowguid { get; set; }

        public virtual ICollection<jobContactPhoneDetail> jobContactPhoneDetailes { get; set; }
    }
}
