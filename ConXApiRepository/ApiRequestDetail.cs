namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ApiRequestDetail")]
    public partial class ApiRequestDetail
    {
        public ApiRequestDetail()
        {
            ApiRequestDetailResult = new HashSet<ApiRequestDetailResult>();
        }

        public int ApiRequestDetailID { get; set; }

        public int RequestID { get; set; }

        [Required]
        public string RequestData { get; set; }

        public bool Processed { get; set; }

        public DateTime? ProcessedOn { get; set; }

        public bool? HasError { get; set; }

        [StringLength(2000)]
        public string ErrorDescription { get; set; }

        public virtual ApiRequest ApiRequest { get; set; }

        public virtual ICollection<ApiRequestDetailResult> ApiRequestDetailResult { get; set; }
    }
}
