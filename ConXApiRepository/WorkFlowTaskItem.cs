namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cfgWorkFlowTaskItem")]
    public partial class WorkFlowTaskItem
    {
        public WorkFlowTaskItem()
        {
            jobTaskItems = new HashSet<JobTaskItem>();
        }

        [Key]
        public Guid TaskItemID { get; set; }

        public Guid? TaskID { get; set; }

        public short? TaskItemOrder { get; set; }

        [StringLength(50)]
        public string TaskItemName { get; set; }

        public float? enumTaskItemType { get; set; }

        [StringLength(50)]
        public string Label { get; set; }

        public float? enumControlType { get; set; }

        [StringLength(50)]
        public string PostLabel { get; set; }

        public Guid? ValidValueListID { get; set; }

        [StringLength(50)]
        public string VVLCatFilterTaskItemName { get; set; }

        [StringLength(500)]
        public string ValidationExpression { get; set; }

        public float? enumCriteria { get; set; }

        [StringLength(50)]
        public string CriteriaLow { get; set; }

        [StringLength(50)]
        public string CriteriaHigh { get; set; }

        public bool? isRequired { get; set; }

        public bool wadtDeleted { get; set; }

        [StringLength(15)]
        public string wadtModifiedBy { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        [StringLength(6)]
        public string wadtModifiedOnDTOffset { get; set; }

        public Guid? wadtRowID { get; set; }

        public float? Vpass_enumActionType { get; set; }

        [StringLength(500)]
        public string Vpass_ActionParamateres { get; set; }

        public float? Vfail_enumActionType { get; set; }

        [StringLength(500)]
        public string Vfail_ActionParamateres { get; set; }

        public bool? ShowComboItemDescription { get; set; }

        public bool? UsePreviousValue { get; set; }

        public Guid rowguid { get; set; }

        public virtual ICollection<JobTaskItem> jobTaskItems { get; set; }

        public virtual WorkFlowTask WorkflowTask { get; set; }
    }
}
