namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("jobAssetAttrib")]
    public partial class JobAssetAttribute
    {
        [Key]
        public Guid JobAssetAttribID { get; set; }

        public Guid JobAssetID { get; set; }

        [StringLength(50)]
        public string Attribute { get; set; }

        [StringLength(200)]
        public string AttributeValue { get; set; }

        public bool wadtDeleted { get; set; }

        [StringLength(15)]
        public string wadtModifiedBy { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        [StringLength(6)]
        public string wadtModifiedOnDTOffset { get; set; }

        public Guid? wadtRowID { get; set; }

        public int? aOrder { get; set; }

        public Guid rowguid { get; set; }

        public virtual JobAsset JobAsset { get; set; }
    }
}
