namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cfgUsers")]
    public partial class CfgUser
    {
        public CfgUser()
        {
            cfgUsersInGroups = new HashSet<CfgUsersInGroup>();
        }

        [Key]
        public Guid UserId { get; set; }

        [Required]
        [StringLength(10)]
        public string FSP { get; set; }

        public Guid UserGroupId { get; set; }

        public Guid? ContactID { get; set; }

        [StringLength(50)]
        public string UserName { get; set; }

        [StringLength(50)]
        public string UserPassword { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string MidName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(15)]
        public string CommonName { get; set; }

        public float? enumUserType { get; set; }

        public float? enumActive { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public bool wadtDeleted { get; set; }

        [StringLength(15)]
        public string wadtModifiedBy { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        [StringLength(6)]
        public string wadtModifiedOnDTOffset { get; set; }

        public Guid? wadtRowID { get; set; }

        public DateTime? CreatedOn { get; set; }

        [StringLength(6)]
        public string CreatedOnDTOffset { get; set; }

        [StringLength(200)]
        public string Email { get; set; }

        public bool isDeviceUser { get; set; }

        public Guid rowguid { get; set; }

        [MaxLength(128)]
        public byte[] PasswordHash { get; set; }

        public DateTime? PasswordModifiedOn { get; set; }

        [StringLength(50)]
        public string PhoneNumber { get; set; }

        [StringLength(50)]
        public string MobilPhoneNumber { get; set; }

        public virtual ICollection<CfgUsersInGroup> cfgUsersInGroups { get; set; }
    }
}
