﻿namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("impImports")]
    public partial class impImport
    {
        [Key]
        public Guid ImportID { get; set; }

        [StringLength(10)]
        public string FSP { get; set; }

        public Guid? ContractID { get; set; }

        public Guid? ContrRecvID { get; set; }

        [StringLength(250)]
        public string FileName { get; set; }

        [StringLength(250)]
        public string FileFullPath { get; set; }

        [StringLength(50)]
        public string Reference { get; set; }

        public float? enumImpStatus { get; set; }

        public DateTime? StatusOn { get; set; }

        [StringLength(15)]
        public string StatusBy { get; set; }

        [StringLength(250)]
        public string ImportMSG { get; set; }

        [StringLength(6)]
        public string StatusOnOffset { get; set; }

        public DateTime? ProcessedOn { get; set; }

        [StringLength(6)]
        public string ProcessedOnOffset { get; set; }

        public virtual CfgContract cfgContract { get; set; }

        public virtual EnumImpStatus EnumImpStatus { get; set; }

        public virtual CfgContractReceivable cfgContractReceivable { get; set; }
    }
}
