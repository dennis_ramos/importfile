namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("jobPhotos")]
    public partial class JobPhoto
    {
        public Guid JobPhotoID { get; set; }

        public Guid JobID { get; set; }

        public Guid? ObjID { get; set; }

        public float? enumObjType { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        public DateTime? PhotoOn { get; set; }

        [StringLength(6)]
        public string PhotoOnOffset { get; set; }

        [StringLength(50)]
        public string PhotoBy { get; set; }

        public bool wadtDeleted { get; set; }

        [StringLength(15)]
        public string wadtModifiedBy { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        [StringLength(6)]
        public string wadtModifiedOnDTOffset { get; set; }

        public Guid? wadtRowID { get; set; }

        public Guid rowguid { get; set; }

        public bool? hasPhoto { get; set; }

        public bool? hasPhotoThumbnail { get; set; }

        public int? enumMediaType { get; set; }

        public int? enumMediaLocation { get; set; }

        //public byte[] photoThumbnail { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public byte[] photo { get; set; }

        public virtual JobJob JobJob { get; set; }
    }
}
