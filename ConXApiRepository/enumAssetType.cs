namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("enumAssetType")]
    public partial class EnumAssetType
    {
        public EnumAssetType()
        {
            cfgWorkFlowTasks = new HashSet<WorkFlowTask>();
            jobAssets = new HashSet<JobAsset>();
        }

        [Key]
        [Column("enumAssetType")]
        public float enumAssetType { get; set; }

        [StringLength(50)]
        public string enumDesc { get; set; }

        [StringLength(10)]
        public string enumSDesc { get; set; }

        public Guid rowguid { get; set; }

        [StringLength(10)]
        public string fsp { get; set; }

        public bool wadtDeleted { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        [StringLength(50)]
        public string wadtModifiedBy { get; set; }

        public virtual ICollection<WorkFlowTask> cfgWorkFlowTasks { get; set; }

        public virtual ICollection<JobAsset> jobAssets { get; set; }
    }
}
