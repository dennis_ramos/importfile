namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cfgValidValueList")]
    public partial class CfgValidValueList
    {
        public CfgValidValueList()
        {
            cfgValidValueListItems = new HashSet<CfgValidValueListItem>();
        }

        [Key]
        public Guid ValidValueListID { get; set; }

        [Required]
        [StringLength(10)]
        public string FSP { get; set; }

        [Required]
        [StringLength(50)]
        public string ValidValueList { get; set; }

        public bool wadtDeleted { get; set; }

        [StringLength(15)]
        public string wadtModifiedBy { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        [StringLength(6)]
        public string wadtModifiedOnDTOffset { get; set; }

        public Guid? wadtRowID { get; set; }

        public Guid rowguid { get; set; }

        public Guid? ParentValidValueListID { get; set; }

        public virtual CfgFSP cfgFSP { get; set; }

        public virtual ICollection<CfgValidValueListItem> cfgValidValueListItems { get; set; }
    }
}
