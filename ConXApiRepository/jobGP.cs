namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("jobGPS")]
    public partial class jobGps
    {
        [Key]
        public Guid GPSID { get; set; }

        [Required]
        [StringLength(50)]
        public string TableName { get; set; }

        [Required]
        [StringLength(50)]
        public string TableColName { get; set; }

        public Guid TableRowID { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public double? HDOP { get; set; }

        public short? SatCount { get; set; }

        public short? SatInView { get; set; }

        public double? SeaLevelAltitude { get; set; }

        public DateTime? GPSDateTime { get; set; }

        public DateTime? DeviceDateTime { get; set; }

        public Guid JobID { get; set; }

        public Guid rowguid { get; set; }

        public virtual JobJob jobJob { get; set; }
    }
}
