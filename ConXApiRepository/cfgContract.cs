namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cfgContracts")]
    public partial class CfgContract
    {
        public CfgContract()
        {
            cfgContractAttributes = new HashSet<CfgContractAttribute>();
            cfgContractDeliverables = new HashSet<CfgContractDeliverable>();
            cfgXreferences = new HashSet<ContractXReference>();
            cfgContractReceivables = new HashSet<CfgContractReceivable>();
            JobJobs = new HashSet<JobJob>();
        }

        [Key]
        public Guid ContractID { get; set; }

        [StringLength(10)]
        public string FSP { get; set; }

        [StringLength(50)]
        public string ContractName { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        public Guid? PrimaryContactID { get; set; }

        public DateTime? AffectiveDate { get; set; }

        public Guid? WorkFlowID { get; set; }

        public bool wadtDeleted { get; set; }

        [StringLength(15)]
        public string wadtModifiedBy { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        [StringLength(6)]
        public string wadtModifiedOnDTOffset { get; set; }

        public Guid? wadtRowID { get; set; }

        public Guid rowguid { get; set; }

        [StringLength(10)]
        public string ClientFSP { get; set; }

        public virtual ICollection<CfgContractAttribute> cfgContractAttributes { get; set; }

        public virtual ICollection<CfgContractDeliverable> cfgContractDeliverables { get; set; }

        public virtual WorkFlow WorkFlow { get; set; }

        public virtual CimContact cimContact { get; set; }

        public virtual ICollection<ContractXReference> cfgXreferences { get; set; }

        public virtual ICollection<JobJob> JobJobs { get; set; }

        public virtual ICollection<CfgContractReceivable> cfgContractReceivables { get; set; }
    }
}
