namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("jobTasks")]
    public partial class JobTask
    {
        public JobTask()
        {
            JobTaskItems = new HashSet<JobTaskItem>();
            JobTaskStatusHistories = new HashSet<JobTaskStatusHistory>();
            SubTasks = new HashSet<JobTask>();

        }

        public Guid JobTaskID { get; set; }

        public Guid JobID { get; set; }

        public float? enumTaskType { get; set; }

        public Guid? TaskID { get; set; }

        public short? TaskOrder { get; set; }

        [StringLength(50)]
        public string TaskDescription { get; set; }

        public float? enumTaskStatus { get; set; }

        public float? enumTaskQualifier { get; set; }

        public bool? isTaskQualifierForced { get; set; }

        [StringLength(50)]
        public string CompletedBy { get; set; }

        public DateTime? CompletedOn { get; set; }

        [StringLength(6)]
        public string CompletedOnOffset { get; set; }

        public Guid? JobAssetID { get; set; }

        public bool wadtDeleted { get; set; }

        [StringLength(15)]
        public string wadtModifiedBy { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        [StringLength(6)]
        public string wadtModifiedOnDTOffset { get; set; }

        public Guid? wadtRowID { get; set; }

        public Guid? ParentJobTaskID { get; set; }

        public Guid rowguid { get; set; }

        public virtual WorkFlowTask cfgWorkFlowTask { get; set; }

        public virtual EnumTaskStatus EnumTaskStatus { get; set; }

        public virtual EnumTaskType EnumTaskType { get; set; }

        public virtual JobJob JobJob { get; set; }

        public virtual ICollection<JobTaskItem> JobTaskItems { get; set; }

        public virtual ICollection<JobTaskStatusHistory> JobTaskStatusHistories { get; set; }

        public virtual ICollection<JobTask> SubTasks { get; set; }

        public virtual JobTask ParentJobTask { get; set; }
        public virtual JobAsset JobTaskAsset { get; set; }
    }
}
