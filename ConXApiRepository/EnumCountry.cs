﻿

namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;


    [Table("enumCountry")]
    public partial class EnumCountry
    {

        [Key]
        [Column("enumCountry")]
        public float enumCountry { get; set; }

        [StringLength(50)]
        public string enumDesc { get; set; }

        [StringLength(10)]
        public string enumSDesc { get; set; }

        public Guid rowguid { get; set; }

    
    }

    
}
