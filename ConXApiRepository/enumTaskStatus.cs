namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("enumTaskStatus")]
    public partial class EnumTaskStatus
    {
        public EnumTaskStatus()
        {
            jobTasks = new HashSet<JobTask>();
        }

        [Key]
        public float enumTaskStatus { get; set; }

        [StringLength(50)]
        public string EnumDesc { get; set; }

        [StringLength(10)]
        public string EnumSDesc { get; set; }

        public Guid rowguid { get; set; }

        public virtual ICollection<JobTask> jobTasks { get; set; }
    }
}
