namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cfgUserGroups")]
    public partial class CfgUserGroup
    {
        public CfgUserGroup()
        {
            cfgUserGroupQueries = new HashSet<CfgUserGroupQuery>();
            cfgUsersInGroups = new HashSet<CfgUsersInGroup>();
        }

        [Key]
        public Guid UserGroupId { get; set; }

        [Required]
        [StringLength(10)]
        public string FSP { get; set; }

        [StringLength(50)]
        public string UserGroupName { get; set; }

        public bool wadtDeleted { get; set; }

        [StringLength(15)]
        public string wadtModifiedBy { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        [StringLength(6)]
        public string wadtModifiedOnDTOffset { get; set; }

        public Guid? wadtRowID { get; set; }

        public virtual ICollection<CfgUserGroupQuery> cfgUserGroupQueries { get; set; }

        public virtual ICollection<CfgUsersInGroup> cfgUsersInGroups { get; set; }
    }
}
