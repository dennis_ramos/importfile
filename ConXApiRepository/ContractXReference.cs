namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cfgXreference")]
    public partial class ContractXReference
    {
        [Key]
        public Guid XrefID { get; set; }

        public Guid ContractID { get; set; }

        [Required]
        [StringLength(10)]
        public string FSP { get; set; }

        [StringLength(50)]
        public string ConXFiled { get; set; }

        [StringLength(50)]
        public string ConXValue { get; set; }

        [StringLength(55)]
        public string OrgValue { get; set; }

        public virtual CfgContract cfgContract { get; set; }
    }
}
