namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("enumRegions")]
    public partial class EnumRegion
    {
        public EnumRegion()
        {
            jobJobs = new HashSet<JobJob>();
        }

        [Key]
        public float enumRegions { get; set; }

        [StringLength(50)]
        public string enumDesc { get; set; }

        [StringLength(10)]
        public string enumSDesc { get; set; }

        [StringLength(10)]
        public string FSP { get; set; }

        public bool wadtDeleted { get; set; }

        [StringLength(15)]
        public string wadtModifiedBy { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        [StringLength(6)]
        public string wadtModifiedOnDTOffset { get; set; }

        public Guid? wadtRowID { get; set; }

        public Guid rowguid { get; set; }

        public virtual CfgFSP cfgFSP { get; set; }

        public virtual ICollection<JobJob> jobJobs { get; set; }
    }
}
