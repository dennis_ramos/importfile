﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("enumImpStatus")]
    public partial class EnumImpStatus
    {
        public EnumImpStatus()
        {
            impImports = new HashSet<impImport>();
        }

        [Key]
        public float enumImpStatus { get; set; }

        [StringLength(50)]
        public string enumDesc { get; set; }

        [StringLength(10)]
        public string enumSDesc { get; set; }

        public virtual ICollection<impImport> impImports { get; set; }
    }
}
