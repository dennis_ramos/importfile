namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cfgFSP")]
    public partial class CfgFSP
    {
        public CfgFSP()
        {
            cfgValidValueLists = new HashSet<CfgValidValueList>();
            cfgWorkFlows = new HashSet<WorkFlow>();
            cimOrganisations = new HashSet<CimOrganisation>();
            enumJobTypes = new HashSet<EnumJobType>();
            enumRegions = new HashSet<EnumRegion>();
            jobJobs = new HashSet<JobJob>();
        }

        [Key]
        [StringLength(10)]
        public string FSP { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        public float? enumActive { get; set; }

        public bool wadtDeleted { get; set; }

        [StringLength(15)]
        public string wadtModifiedBy { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        [StringLength(6)]
        public string wadtModifiedOnDTOffset { get; set; }

        public Guid? wadtRowID { get; set; }

        public bool? LimitPhotoSize { get; set; }

        public Guid? ResellerUserID { get; set; }

        public float enumMapProvider { get; set; }

        [StringLength(10)]
        public string deviceName { get; set; }

        public int? deviceNumber { get; set; }

        public Guid rowguid { get; set; }

        public virtual ICollection<CfgValidValueList> cfgValidValueLists { get; set; }

        public virtual ICollection<WorkFlow> cfgWorkFlows { get; set; }

        public virtual ICollection<CimOrganisation> cimOrganisations { get; set; }

        public virtual ICollection<EnumJobType> enumJobTypes { get; set; }

        public virtual ICollection<EnumRegion> enumRegions { get; set; }

        public virtual ICollection<JobJob> jobJobs { get; set; }
    }
}
