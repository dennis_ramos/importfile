namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cfgContractAttributes")]
    public partial class CfgContractAttribute
    {
        public int Id { get; set; }

        public Guid ContractID { get; set; }

        [Required]
        [StringLength(50)]
        public string Attribute { get; set; }

        [StringLength(500)]
        public string AttributeValue { get; set; }

        public bool? wadtDeleted { get; set; }

        [StringLength(50)]
        public string wadtModifiedBy { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        public virtual CfgContract cfgContract { get; set; }
    }
}
