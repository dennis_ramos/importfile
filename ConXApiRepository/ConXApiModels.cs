namespace ConXApiRepository
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ConXApiModels : DbContext
    {
        public ConXApiModels()
            : base("name=ConXApiModels")
        {
        }

        public virtual DbSet<CfgContractAttribute> cfgContractAttributes { get; set; }
        public virtual DbSet<CfgContractDeliverable> cfgContractDeliverables { get; set; }
        public virtual DbSet<CfgContract> cfgContracts { get; set; }
        public virtual DbSet<CfgFSP> cfgFSPs { get; set; }
        public virtual DbSet<CfgUserGroupQuery> cfgUserGroupQueries { get; set; }
        public virtual DbSet<CfgUserGroup> cfgUserGroups { get; set; }
        public virtual DbSet<CfgUser> cfgUsers { get; set; }
        public virtual DbSet<CfgUsersInGroup> cfgUsersInGroups { get; set; }
        public virtual DbSet<CfgValidValueList> cfgValidValueLists { get; set; }
        public virtual DbSet<CfgValidValueListItem> cfgValidValueListItems { get; set; }
        public virtual DbSet<WorkFlow> cfgWorkFlows { get; set; }
        public virtual DbSet<WorkFlowTaskItem> cfgWorkFlowTaskItems { get; set; }
        public virtual DbSet<WorkFlowTask> cfgWorkFlowTasks { get; set; }
        public virtual DbSet<ContractXReference> cfgXreferences { get; set; }
        public virtual DbSet<CimContact> cimContacts { get; set; }
        public virtual DbSet<CimOrganisation> cimOrganisations { get; set; }
        public virtual DbSet<EnumAssetType> enumAssetTypes { get; set; }
        public virtual DbSet<EnumCountry> enumCountries { get; set; }
        public virtual DbSet<EnumDayPart> enumDayParts { get; set; }
        public virtual DbSet<EnumJobSource> enumJobSources { get; set; }
        public virtual DbSet<EnumJobStatus> enumJobStatus { get; set; }
        public virtual DbSet<EnumJobType> enumJobTypes { get; set; }
        public virtual DbSet<EnumNoteType> enumNoteTypes { get; set; }
        public virtual DbSet<EnumPhoneType> enumPhoneTypes { get; set; }
        public virtual DbSet<EnumRegion> enumRegions { get; set; }
        public virtual DbSet<EnumTaskStatus> enumTaskStatus { get; set; }
        public virtual DbSet<EnumTaskType> enumTaskTypes { get; set; }
        public virtual DbSet<EnumWFTaskType> enumWFTaskTypes { get; set; }
        public virtual DbSet<FieldOutcomeHistory> FieldOutcomeHistories { get; set; }
        public virtual DbSet<JobAddress> jobAddresses { get; set; }
        public virtual DbSet<JobAssetAttribute> jobAssetAttribs { get; set; }
        public virtual DbSet<JobAsset> jobAssets { get; set; }
        public virtual DbSet<jobContactPhoneDetail> jobContactPhoneDetailes { get; set; }
        public virtual DbSet<JobContact> jobContacts { get; set; }
        public virtual DbSet<JobDeliverable> jobDeliverables { get; set; }
        public virtual DbSet<jobGps> jobGPS { get; set; }
        public virtual DbSet<JobJob> jobJobs { get; set; }
        public virtual DbSet<JobJobAttribute> jobJobAttributes { get; set; }
        public virtual DbSet<JobNote> jobNotes { get; set; }
        public virtual DbSet<JobPhoto> jobPhotos { get; set; }
        public virtual DbSet<JobStatusHistory> jobStatusHistories { get; set; }
        public virtual DbSet<JobTaskItem> jobTaskItems { get; set; }
        public virtual DbSet<JobTask> jobTasks { get; set; }
        public virtual DbSet<JobTaskStatusHistory> jobTaskStatusHistories { get; set; }
        public virtual DbSet<ApiRequest> ApiRequests { get; set; }
        public virtual DbSet<ApiRequestDetail> ApiRequestDetails { get; set; }
        public virtual DbSet<ApiRequestDetailResult> ApiRequestDetailResults { get; set; }
        public virtual DbSet<ApiRequestStatus> ApiRequestStatuses { get; set; }
        public virtual DbSet<ApiRequestType> ApiRequestTypes { get; set; }

        public virtual DbSet<CfgContractReceivable> CfgContractReceivables { get; set; }
        public virtual DbSet<EnumReceivableFormat> enumReceivableFormats { get; set; }
        public virtual DbSet<EnumImpStatus> enumImpStatus { get; set; }
        public virtual DbSet<EnumReceivableType> enumReceivableTypes { get; set; }
        public virtual DbSet<impImport> impImports { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<CfgContractAttribute>()
                .Property(e => e.Attribute)
                .IsUnicode(false);

            modelBuilder.Entity<CfgContractAttribute>()
                .Property(e => e.AttributeValue)
                .IsUnicode(false);

            modelBuilder.Entity<CfgContractAttribute>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<CfgContractDeliverable>()
                .Property(e => e.DeliverableName)
                .IsUnicode(false);

            modelBuilder.Entity<CfgContractDeliverable>()
                .Property(e => e.Destination)
                .IsUnicode(false);

            modelBuilder.Entity<CfgContractDeliverable>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<CfgContractDeliverable>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<CfgContract>()
                .Property(e => e.FSP)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<CfgContract>()
                .Property(e => e.ContractName)
                .IsUnicode(false);

            modelBuilder.Entity<CfgContract>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<CfgContract>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<CfgContract>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<CfgContract>()
                .Property(e => e.ClientFSP)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<CfgContract>()
                .HasMany(e => e.cfgContractAttributes)
                .WithRequired(e => e.cfgContract)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CfgContract>()
                .HasMany(e => e.cfgContractDeliverables)
                .WithRequired(e => e.cfgContract)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CfgContract>()
                .HasMany(e => e.cfgXreferences)
                .WithRequired(e => e.cfgContract)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CfgContract>()
                .HasMany(e => e.cfgContractReceivables)
                .WithRequired(e => e.cfgContract)
                .WillCascadeOnDelete(false);




            modelBuilder.Entity<CfgFSP>()
                .Property(e => e.FSP)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<CfgFSP>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<CfgFSP>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<CfgFSP>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<CfgFSP>()
                .HasMany(e => e.cfgValidValueLists)
                .WithRequired(e => e.cfgFSP)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CfgFSP>()
                .HasMany(e => e.jobJobs)
                .WithRequired(e => e.CfgFSP)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CfgUserGroup>()
                .Property(e => e.FSP)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<CfgUserGroup>()
                .Property(e => e.UserGroupName)
                .IsUnicode(false);

            modelBuilder.Entity<CfgUserGroup>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<CfgUserGroup>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<CfgUserGroup>()
                .HasMany(e => e.cfgUserGroupQueries)
                .WithRequired(e => e.cfgUserGroup)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CfgUserGroup>()
                .HasMany(e => e.cfgUsersInGroups)
                .WithRequired(e => e.cfgUserGroup)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CfgUser>()
                .Property(e => e.FSP)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<CfgUser>()
                .Property(e => e.UserName)
                .IsUnicode(false);

            modelBuilder.Entity<CfgUser>()
                .Property(e => e.UserPassword)
                .IsUnicode(false);

            modelBuilder.Entity<CfgUser>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<CfgUser>()
                .Property(e => e.MidName)
                .IsUnicode(false);

            modelBuilder.Entity<CfgUser>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<CfgUser>()
                .Property(e => e.CommonName)
                .IsUnicode(false);

            modelBuilder.Entity<CfgUser>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<CfgUser>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<CfgUser>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<CfgUser>()
                .Property(e => e.CreatedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<CfgUser>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<CfgUser>()
                .Property(e => e.PhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<CfgUser>()
                .Property(e => e.MobilPhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<CfgUser>()
                .HasMany(e => e.cfgUsersInGroups)
                .WithRequired(e => e.cfgUser)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CfgValidValueList>()
                .Property(e => e.FSP)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<CfgValidValueList>()
                .Property(e => e.ValidValueList)
                .IsUnicode(false);

            modelBuilder.Entity<CfgValidValueList>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<CfgValidValueList>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<CfgValidValueList>()
                .HasMany(e => e.cfgValidValueListItems)
                .WithRequired(e => e.cfgValidValueList)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CfgValidValueListItem>()
                .Property(e => e.Category)
                .IsUnicode(false);

            modelBuilder.Entity<CfgValidValueListItem>()
                .Property(e => e.ItemText)
                .IsUnicode(false);

            modelBuilder.Entity<CfgValidValueListItem>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<CfgValidValueListItem>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<CfgValidValueListItem>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<CfgValidValueListItem>()
                .Property(e => e.ItemDescription)
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlow>()
                .Property(e => e.FSP)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlow>()
                .Property(e => e.WorkFlowName)
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlow>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlow>()
                .Property(e => e.CreatedBy)
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlow>()
                .Property(e => e.CreatedOnOffset)
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlow>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlow>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlow>()
                .HasMany(e => e.JobJobs)
                .WithRequired(e => e.WorkFlow)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<WorkFlowTaskItem>()
                .Property(e => e.TaskItemName)
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlowTaskItem>()
                .Property(e => e.Label)
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlowTaskItem>()
                .Property(e => e.PostLabel)
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlowTaskItem>()
                .Property(e => e.VVLCatFilterTaskItemName)
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlowTaskItem>()
                .Property(e => e.ValidationExpression)
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlowTaskItem>()
                .Property(e => e.CriteriaLow)
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlowTaskItem>()
                .Property(e => e.CriteriaHigh)
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlowTaskItem>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlowTaskItem>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlowTaskItem>()
                .Property(e => e.Vpass_ActionParamateres)
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlowTaskItem>()
                .Property(e => e.Vfail_ActionParamateres)
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlowTaskItem>()
                .HasMany(e => e.jobTaskItems)
                .WithRequired(e => e.WorkFlowTaskItem)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<WorkFlowTask>()
                .Property(e => e.TaskName)
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlowTask>()
                .Property(e => e.CustomFormToRun)
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlowTask>()
                .Property(e => e.ValidationExpression)
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlowTask>()
                .Property(e => e.CriteriaLow)
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlowTask>()
                .Property(e => e.CriteriaHigh)
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlowTask>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlowTask>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<WorkFlowTask>()
                .Property(e => e.NotifyEmail)
                .IsUnicode(false);

            modelBuilder.Entity<ContractXReference>()
                .Property(e => e.FSP)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ContractXReference>()
                .Property(e => e.ConXFiled)
                .IsUnicode(false);

            modelBuilder.Entity<ContractXReference>()
                .Property(e => e.ConXValue)
                .IsUnicode(false);

            modelBuilder.Entity<ContractXReference>()
                .Property(e => e.OrgValue)
                .IsUnicode(false);

            modelBuilder.Entity<CimContact>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<CimContact>()
                .Property(e => e.FullName)
                .IsUnicode(false);

            modelBuilder.Entity<CimContact>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<CimContact>()
                .Property(e => e.MidName)
                .IsUnicode(false);

            modelBuilder.Entity<CimContact>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<CimContact>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<CimContact>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<CimContact>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<CimContact>()
                .HasMany(e => e.cfgContracts)
                .WithOptional(e => e.cimContact)
                .HasForeignKey(e => e.PrimaryContactID);

            modelBuilder.Entity<CimOrganisation>()
                .Property(e => e.OrganisationName)
                .IsUnicode(false);

            modelBuilder.Entity<CimOrganisation>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<CimOrganisation>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<CimOrganisation>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<CimOrganisation>()
                .Property(e => e.FSP)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<EnumAssetType>()
                .Property(e => e.enumDesc)
                .IsUnicode(false);

            modelBuilder.Entity<EnumAssetType>()
                .Property(e => e.enumSDesc)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<EnumAssetType>()
                .Property(e => e.fsp)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<EnumAssetType>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<EnumAssetType>()
                .HasMany(e => e.cfgWorkFlowTasks)
                .WithOptional(e => e.enumAssetType1)
                .HasForeignKey(e => e.enumAssetType);

            modelBuilder.Entity<EnumAssetType>()
                .HasMany(e => e.jobAssets)
                .WithRequired(e => e.enumAssetType1)
                .HasForeignKey(e => e.enumAssetType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EnumDayPart>()
                .Property(e => e.enumDesc)
                .IsUnicode(false);

            modelBuilder.Entity<EnumDayPart>()
                .Property(e => e.enumSDesc)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<EnumDayPart>()
                .HasMany(e => e.jobJobs)
                .WithOptional(e => e.EnumDayPart)
                .HasForeignKey(e => e.enumDayPart);

            modelBuilder.Entity<EnumJobSource>()
                .Property(e => e.enumDesc)
                .IsUnicode(false);

            modelBuilder.Entity<EnumJobSource>()
                .Property(e => e.enumSDesc)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<EnumJobSource>()
                .HasMany(e => e.jobJobs)
                .WithOptional(e => e.EnumJobSource)
                .HasForeignKey(e => e.enumJobSource);

            modelBuilder.Entity<EnumJobStatus>()
                .Property(e => e.EnumDesc)
                .IsUnicode(false);

            modelBuilder.Entity<EnumJobStatus>()
                .Property(e => e.EnumSDesc)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<EnumJobStatus>()
                .HasMany(e => e.jobJobs)
                .WithRequired(e => e.EnumJobStatus)
                .HasForeignKey(e => e.enumJobStatus)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EnumJobStatus>()
                .HasMany(e => e.jobJobs1)
                .WithOptional(e => e.EnumFieldJobStatus)
                .HasForeignKey(e => e.enumFieldJobStatus);

            modelBuilder.Entity<EnumJobType>()
                .Property(e => e.enumDesc)
                .IsUnicode(false);

            modelBuilder.Entity<EnumJobType>()
                .Property(e => e.enumSDesc)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<EnumJobType>()
                .Property(e => e.FSP)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<EnumJobType>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<EnumJobType>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<EnumJobType>()
                .HasMany(e => e.jobJobs)
                .WithRequired(e => e.EnumJobType)
                .HasForeignKey(e => e.enumJobType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EnumNoteType>()
                .Property(e => e.enumDesc)
                .IsUnicode(false);

            modelBuilder.Entity<EnumNoteType>()
                .Property(e => e.enumSDesc)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<EnumNoteType>()
                .HasMany(e => e.jobNotes)
                .WithOptional(e => e.EnumNoteType)
                .HasForeignKey(e => e.enumNoteType);

            modelBuilder.Entity<EnumCountry>()
                .Property(e => e.enumDesc)
                .IsUnicode(false);

            modelBuilder.Entity<EnumPhoneType>()
                .Property(e => e.enumDesc)
                .IsUnicode(false);

            modelBuilder.Entity<EnumCountry>()
                .Property(e => e.enumSDesc)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<EnumPhoneType>()
                .Property(e => e.enumSDesc)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<EnumPhoneType>()
                .HasMany(e => e.jobContactPhoneDetailes)
                .WithRequired(e => e.enumPhoneType1)
                .HasForeignKey(e => e.enumPhoneType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EnumRegion>()
                .Property(e => e.enumDesc)
                .IsUnicode(false);

            modelBuilder.Entity<EnumRegion>()
                .Property(e => e.enumSDesc)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<EnumRegion>()
                .Property(e => e.FSP)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<EnumRegion>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<EnumRegion>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<EnumRegion>()
                .HasMany(e => e.jobJobs)
                .WithRequired(e => e.EnumRegion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EnumTaskStatus>()
                .Property(e => e.EnumDesc)
                .IsUnicode(false);

            modelBuilder.Entity<EnumTaskStatus>()
                .Property(e => e.EnumSDesc)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<EnumTaskType>()
                .Property(e => e.enumDesc)
                .IsUnicode(false);

            modelBuilder.Entity<EnumTaskType>()
                .Property(e => e.enumSDesc)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<EnumTaskType>()
                .HasMany(e => e.jobTasks)
                .WithOptional(e => e.EnumTaskType)
                .HasForeignKey(e => e.enumTaskType);

            modelBuilder.Entity<EnumWFTaskType>()
                .Property(e => e.enumDesc)
                .IsUnicode(false);

            modelBuilder.Entity<EnumWFTaskType>()
                .Property(e => e.enumSDesc)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<EnumWFTaskType>()
                .HasMany(e => e.cfgWorkFlowTasks)
                .WithOptional(e => e.enumWFTaskType1)
                .HasForeignKey(e => e.enumWFTaskType);

            modelBuilder.Entity<FieldOutcomeHistory>()
                .Property(e => e.Outcome)
                .IsUnicode(false);

            modelBuilder.Entity<FieldOutcomeHistory>()
                .Property(e => e.Reason)
                .IsUnicode(false);

            modelBuilder.Entity<FieldOutcomeHistory>()
                .Property(e => e.OutcomeBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobAddress>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<JobAddress>()
                .Property(e => e.StreetNo)
                .IsUnicode(false);

            modelBuilder.Entity<JobAddress>()
                .Property(e => e.StreetName)
                .IsUnicode(false);

            modelBuilder.Entity<JobAddress>()
                .Property(e => e.Apartment)
                .IsUnicode(false);

            modelBuilder.Entity<JobAddress>()
                .Property(e => e.Suburb)
                .IsUnicode(false);

            modelBuilder.Entity<JobAddress>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<JobAddress>()
                .Property(e => e.PostCode)
                .IsUnicode(false);

            modelBuilder.Entity<JobAddress>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<JobAddress>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobAddress>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<JobAddress>()
                .Property(e => e.Longitude)
                .IsUnicode(false);

            modelBuilder.Entity<JobAddress>()
                .Property(e => e.Latitude)
                .IsUnicode(false);

            modelBuilder.Entity<JobAddress>()
                .Property(e => e.Elevation)
                .IsUnicode(false);

            modelBuilder.Entity<JobAssetAttribute>()
                .Property(e => e.Attribute)
                .IsUnicode(false);

            modelBuilder.Entity<JobAssetAttribute>()
                .Property(e => e.AttributeValue)
                .IsUnicode(false);

            modelBuilder.Entity<JobAssetAttribute>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobAssetAttribute>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<JobAsset>()
                .Property(e => e.AssetName)
                .IsUnicode(false);

            modelBuilder.Entity<JobAsset>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobAsset>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<jobContactPhoneDetail>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<jobContactPhoneDetail>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<jobContactPhoneDetail>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<JobContact>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<JobContact>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<JobContact>()
                .Property(e => e.MidName)
                .IsUnicode(false);

            modelBuilder.Entity<JobContact>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<JobContact>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<JobContact>()
                .Property(e => e.Mobile)
                .IsUnicode(false);

            modelBuilder.Entity<JobContact>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<JobContact>()
                .Property(e => e.Fax)
                .IsUnicode(false);

            modelBuilder.Entity<JobContact>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobContact>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<JobContact>()
                .HasMany(e => e.JobContactPhoneDetails)
                .WithRequired(e => e.JobContact)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobDeliverable>()
                .Property(e => e.StatusBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobDeliverable>()
                .Property(e => e.StatusOnOffset)
                .IsUnicode(false);

            modelBuilder.Entity<JobDeliverable>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobDeliverable>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<jobGps>()
                .Property(e => e.TableName)
                .IsUnicode(false);

            modelBuilder.Entity<jobGps>()
                .Property(e => e.TableColName)
                .IsUnicode(false);

            modelBuilder.Entity<JobJob>()
                .Property(e => e.FSP)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<JobJob>()
                .Property(e => e.WorkDoneByFSP)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<JobJob>()
                .Property(e => e.JobNo)
                .IsUnicode(false);

            modelBuilder.Entity<JobJob>()
                .Property(e => e.COReferenceNo)
                .IsUnicode(false);

            modelBuilder.Entity<JobJob>()
                .Property(e => e.DeviceName)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<JobJob>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<JobJob>()
                .Property(e => e.StatusBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobJob>()
                .Property(e => e.StatusOnOffset)
                .IsUnicode(false);

            modelBuilder.Entity<JobJob>()
                .Property(e => e.CompletedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobJob>()
                .Property(e => e.CompletedOnOffset)
                .IsUnicode(false);

            modelBuilder.Entity<JobJob>()
                .Property(e => e.ScheduledForDateOffset)
                .IsUnicode(false);

            modelBuilder.Entity<JobJob>()
                .Property(e => e.ScheduledForTech)
                .IsUnicode(false);

            modelBuilder.Entity<JobJob>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobJob>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<JobJob>()
                .Property(e => e.Group1)
                .IsUnicode(false);

            modelBuilder.Entity<JobJob>()
                .Property(e => e.Group2)
                .IsUnicode(false);

            modelBuilder.Entity<JobJob>()
                .Property(e => e.Group3)
                .IsUnicode(false);

            modelBuilder.Entity<JobJob>()
                .Property(e => e.FieldOutcome)
                .IsUnicode(false);

            modelBuilder.Entity<JobJob>()
                .Property(e => e.FieldOutcomeReason)
                .IsUnicode(false);

            modelBuilder.Entity<JobJob>()
                .HasMany(e => e.FieldOutcomeHistories)
                .WithRequired(e => e.jobJob)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobJob>()
                .HasMany(e => e.JobAddresses)
                .WithRequired(e => e.jobJob)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobJob>()
                .HasMany(e => e.JobAssets)
                .WithRequired(e => e.jobJob)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobJob>()
                .HasMany(e => e.JobContacts)
                .WithRequired(e => e.jobJob)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobJob>()
                .HasMany(e => e.JobDeliverables)
                .WithRequired(e => e.jobJob)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobJob>()
                .HasMany(e => e.JobGPSs)
                .WithRequired(e => e.jobJob)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobJob>()
                .HasMany(e => e.JobJobAttributes)
                .WithRequired(e => e.jobJob)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobJob>()
                .HasMany(e => e.JobNotes)
                .WithRequired(e => e.JobJob)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobJob>()
                .HasMany(e => e.JobPhotos)
                .WithRequired(e => e.JobJob)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobJob>()
                .HasMany(e => e.JobTasks)
                .WithRequired(e => e.JobJob)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobJobAttribute>()
                .Property(e => e.Attribute)
                .IsUnicode(false);

            modelBuilder.Entity<JobJobAttribute>()
                .Property(e => e.AttributeValue)
                .IsUnicode(false);

            modelBuilder.Entity<JobJobAttribute>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobJobAttribute>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<JobNote>()
                .Property(e => e.Note)
                .IsUnicode(false);

            modelBuilder.Entity<JobNote>()
                .Property(e => e.NoteOnOffset)
                .IsUnicode(false);

            modelBuilder.Entity<JobNote>()
                .Property(e => e.NoteBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobNote>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobNote>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<JobPhoto>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<JobPhoto>()
                .Property(e => e.PhotoOnOffset)
                .IsUnicode(false);

            modelBuilder.Entity<JobPhoto>()
                .Property(e => e.PhotoBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobPhoto>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobPhoto>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<JobStatusHistory>()
                .Property(e => e.StatusBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobTaskItem>()
                .Property(e => e.TaskItemName)
                .IsUnicode(false);

            modelBuilder.Entity<JobTaskItem>()
                .Property(e => e.Result)
                .IsUnicode(false);

            modelBuilder.Entity<JobTaskItem>()
                .Property(e => e.ValidationResult)
                .IsUnicode(false);

            modelBuilder.Entity<JobTaskItem>()
                .Property(e => e.EnteredBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobTaskItem>()
                .Property(e => e.EnteredOnOffset)
                .IsUnicode(false);

            modelBuilder.Entity<JobTaskItem>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobTaskItem>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<JobTask>()
                .Property(e => e.TaskDescription)
                .IsUnicode(false);

            modelBuilder.Entity<JobTask>()
                .Property(e => e.CompletedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobTask>()
                .Property(e => e.CompletedOnOffset)
                .IsUnicode(false);

            modelBuilder.Entity<JobTask>()
                .Property(e => e.wadtModifiedBy)
                .IsUnicode(false);

            modelBuilder.Entity<JobTask>()
                .Property(e => e.wadtModifiedOnDTOffset)
                .IsUnicode(false);

            modelBuilder.Entity<JobTask>()
                .HasMany(e => e.JobTaskItems)
                .WithRequired(e => e.JobTask)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobTask>()
                .HasMany(e => e.JobTaskStatusHistories)
                .WithRequired(e => e.jobTask)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobTask>()
                .HasMany(e => e.SubTasks)
                .WithOptional(e => e.ParentJobTask)
                .HasForeignKey(e => e.ParentJobTaskID);


            modelBuilder.Entity<CfgContractReceivable>()
                .Property(e => e.ReceivableName)
                .IsUnicode(false);

            modelBuilder.Entity<CfgContractReceivable>()
                .HasMany(e => e.ImpImports)
                .WithOptional(e => e.cfgContractReceivable)
                .WillCascadeOnDelete(false);


            //modelBuilder.Entity<Employee>()
            //   .HasMany(e => e.Employee1)
            //   .WithOptional(e => e.Employee2)
            //   .HasForeignKey(e => e.ManagerID);

            modelBuilder.Entity<WorkFlowTask>()
                .HasMany(e => e.SubWorkFlowTasks)
                .WithOptional(e => e.ParentWorkFlowTask)
                .HasForeignKey(e => e.ParentTaskID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<WorkFlowTask>()
                .HasMany(e => e.WorkflowTaskItems)
                .WithRequired(e => e.WorkflowTask)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<JobTaskStatusHistory>()
                .Property(e => e.StatusBy)
                .IsUnicode(false);

            //modelBuilder.Entity<jobAsset>()
            //   .HasMany(e => e.JobAssetAttributes)
            //   .WithRequired(e => e.JobAsset)
            //   .HasForeignKey(e => e.JobAssetID)
            //   .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobAsset>()
            .HasMany(e => e.jobAssetAttribs)
            .WithRequired(e => e.JobAsset)
            .WillCascadeOnDelete(false);

            modelBuilder.Entity<JobAsset>()
                .HasMany(e => e.AssetTasks)
                .WithRequired(e => e.JobTaskAsset)
                //.HasForeignKey(e => new {e.JobAssetID, e.JobID} )
                .HasForeignKey(e => e.JobAssetID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApiRequest>()
                .Property(e => e.FSP)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ApiRequest>()
                .Property(e => e.RequestedBy)
                .IsUnicode(false);

            modelBuilder.Entity<ApiRequest>()
                .Property(e => e.Notes)
                .IsUnicode(false);

            modelBuilder.Entity<ApiRequest>()
                .HasMany(e => e.ApiRequestDetail)
                .WithRequired(e => e.ApiRequest)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApiRequestDetail>()
                .Property(e => e.RequestData)
                .IsUnicode(false);

            modelBuilder.Entity<ApiRequestDetail>()
                .Property(e => e.ErrorDescription)
                .IsUnicode(false);

            modelBuilder.Entity<ApiRequestDetail>()
                .HasMany(e => e.ApiRequestDetailResult)
                .WithRequired(e => e.ApiRequestDetail)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApiRequestDetailResult>()
                .Property(e => e.ResultDescription)
                .IsUnicode(false);

            modelBuilder.Entity<ApiRequestStatus>()
                .Property(e => e.ApiRequestStatusDescription)
                .IsUnicode(false);

            modelBuilder.Entity<ApiRequestStatus>()
                .HasMany(e => e.ApiRequest)
                .WithRequired(e => e.ApiRequestStatus)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApiRequestType>()
                .Property(e => e.ApiRequestTypeDescription)
                .IsUnicode(false);

            modelBuilder.Entity<ApiRequestType>()
                .HasMany(e => e.ApiRequest)
                .WithRequired(e => e.ApiRequestType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EnumReceivableFormat>()
                .Property(e => e.enumDesc)
                .IsUnicode(false);

            modelBuilder.Entity<EnumReceivableFormat>()
                .Property(e => e.enumSDesc)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<EnumReceivableFormat>()
                .HasMany(e => e.cfgContractReceivables)
                .WithOptional(e => e.EnumReceivableFormat)
                .HasForeignKey(e => e.enumReceivableFormat);

            modelBuilder.Entity<EnumImpStatus>()
                .Property(e => e.enumDesc)
                .IsUnicode(false);

            modelBuilder.Entity<EnumImpStatus>()
                .Property(e => e.enumSDesc)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<EnumReceivableType>()
                .Property(e => e.enumDesc)
                .IsUnicode(false);

            modelBuilder.Entity<EnumReceivableType>()
                .Property(e => e.enumSdesc)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<impImport>()
                .Property(e => e.FSP)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<impImport>()
                .Property(e => e.FileName)
                .IsUnicode(false);

            modelBuilder.Entity<impImport>()
                .Property(e => e.FileFullPath)
                .IsUnicode(false);

            modelBuilder.Entity<impImport>()
                .Property(e => e.Reference)
                .IsUnicode(false);

            modelBuilder.Entity<impImport>()
                .Property(e => e.StatusBy)
                .IsUnicode(false);

            modelBuilder.Entity<impImport>()
                .Property(e => e.ImportMSG)
                .IsUnicode(false);

            modelBuilder.Entity<impImport>()
                .Property(e => e.StatusOnOffset)
                .IsUnicode(false);

            modelBuilder.Entity<impImport>()
                .Property(e => e.ProcessedOnOffset)
                .IsUnicode(false);
            
                
        }
    }
}
