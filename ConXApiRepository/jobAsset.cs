namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("jobAssets")]
    public partial class JobAsset
    {
        public JobAsset()
        {
            jobAssetAttribs = new HashSet<JobAssetAttribute>();
            AssetTasks = new HashSet<JobTask>();
        }

        public Guid JobAssetID { get; set; }

        public Guid JobID { get; set; }

        [StringLength(50)]
        public string AssetName { get; set; }

        public float enumAssetType { get; set; }

        public int AssetOrder { get; set; }

        public bool wadtDeleted { get; set; }

        [StringLength(15)]
        public string wadtModifiedBy { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        [StringLength(6)]
        public string wadtModifiedOnDTOffset { get; set; }

        public Guid? wadtRowID { get; set; }

        public Guid rowguid { get; set; }

        public virtual EnumAssetType enumAssetType1 { get; set; }

        public virtual JobJob jobJob { get; set; }

        public ICollection<JobAssetAttribute> jobAssetAttribs { get; set; }

        public ICollection<JobTask> AssetTasks { get; set; }
    }
}
