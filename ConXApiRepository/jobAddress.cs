namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("jobAddress")]
    public partial class JobAddress
    {
        public Guid JobAddressID { get; set; }

        public Guid JobID { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        [StringLength(20)]
        public string StreetNo { get; set; }

        [StringLength(50)]
        public string StreetName { get; set; }

        [StringLength(20)]
        public string Apartment { get; set; }

        [StringLength(50)]
        public string Suburb { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(50)]
        public string PostCode { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        public float enumAddressType { get; set; }

        public float enumAddressSource { get; set; }

        public float enumCountry { get; set; }

        public bool PrimaryAddress { get; set; }

        public bool wadtDeleted { get; set; }

        [StringLength(15)]
        public string wadtModifiedBy { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        [StringLength(6)]
        public string wadtModifiedOnDTOffset { get; set; }

        public Guid? wadtRowID { get; set; }

        [StringLength(50)]
        public string Longitude { get; set; }

        [StringLength(50)]
        public string Latitude { get; set; }

        [StringLength(50)]
        public string Elevation { get; set; }

        public Guid rowguid { get; set; }

        public virtual JobJob jobJob { get; set; }
    }
}
