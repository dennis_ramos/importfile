namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cfgContractDeliverables")]
    public partial class CfgContractDeliverable
    {
        [Key]
        public Guid ContrDelivID { get; set; }

        public Guid ContractID { get; set; }

        [StringLength(50)]
        public string DeliverableName { get; set; }

        public float enumDeliverableType { get; set; }

        public float enumDeliverableFormat { get; set; }

        [StringLength(250)]
        public string Destination { get; set; }

        public bool AddByDefaultToJob { get; set; }

        public bool? wadtDeleted { get; set; }

        [StringLength(15)]
        public string wadtModifiedBy { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        [StringLength(6)]
        public string wadtModifiedOnDTOffset { get; set; }

        public Guid? wadtRowID { get; set; }

        public virtual CfgContract cfgContract { get; set; }
    }
}
