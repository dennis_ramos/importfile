namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("jobNotes")]
    public partial class JobNote
    {
        public Guid JobNoteID { get; set; }

        public Guid JobID { get; set; }

        public Guid? ObjID { get; set; }

        public float? enumObjType { get; set; }

        public float? enumNoteType { get; set; }

        [StringLength(1000)]
        public string Note { get; set; }

        public DateTime? NoteOn { get; set; }

        [StringLength(6)]
        public string NoteOnOffset { get; set; }

        [StringLength(50)]
        public string NoteBy { get; set; }

        public bool wadtDeleted { get; set; }

        [StringLength(15)]
        public string wadtModifiedBy { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        [StringLength(6)]
        public string wadtModifiedOnDTOffset { get; set; }

        public Guid? wadtRowID { get; set; }

        public Guid rowguid { get; set; }

        public virtual EnumNoteType EnumNoteType { get; set; }

        public virtual JobJob JobJob { get; set; }
    }
}
