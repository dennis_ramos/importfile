namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("jobDeliverables")]
    public partial class JobDeliverable
    {
        [Key]
        public Guid JobDelivID { get; set; }

        public Guid JobID { get; set; }

        public float enumDeliverableType { get; set; }

        public float enumDeliverableStatus { get; set; }

        [StringLength(50)]
        public string StatusBy { get; set; }

        public DateTime? StatusOn { get; set; }

        [StringLength(6)]
        public string StatusOnOffset { get; set; }

        public bool wadtDeleted { get; set; }

        [StringLength(15)]
        public string wadtModifiedBy { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        [StringLength(6)]
        public string wadtModifiedOnDTOffset { get; set; }

        public Guid? wadtRowID { get; set; }

        public Guid? ContrDelivID { get; set; }

        public Guid? DelivTaskID { get; set; }

        public virtual JobJob jobJob { get; set; }
    }
}
