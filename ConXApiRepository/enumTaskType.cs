namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("enumTaskType")]
    public partial class EnumTaskType
    {
        public EnumTaskType()
        {
            jobTasks = new HashSet<JobTask>();
        }

        [Key]
        [Column("enumTaskType")]
        public float enumTaskType { get; set; }

        [StringLength(50)]
        public string enumDesc { get; set; }

        [StringLength(10)]
        public string enumSDesc { get; set; }

        public Guid rowguid { get; set; }

        public virtual ICollection<JobTask> jobTasks { get; set; }
    }
}
