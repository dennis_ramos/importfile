namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ApiRequest")]
    public partial class ApiRequest
    {
        public ApiRequest()
        {
            ApiRequestDetail = new HashSet<ApiRequestDetail>();
        }

        [Key]
        public int RequestID { get; set; }

        [Required]
        [StringLength(10)]
        public string FSP { get; set; }

        public int ApiRequestTypeID { get; set; }

        [Required]
        [StringLength(50)]
        public string RequestedBy { get; set; }

        public DateTime RequestedOn { get; set; }

        public DateTime? ProcessedOn { get; set; }

        public DateTime? CompletedOn { get; set; }

        [StringLength(2000)]
        public string Notes { get; set; }

        public int ApiRequestStatusId { get; set; }

        public virtual ApiRequestStatus ApiRequestStatus { get; set; }

        public virtual ApiRequestType ApiRequestType { get; set; }

        public virtual ICollection<ApiRequestDetail> ApiRequestDetail { get; set; }
    }
}
