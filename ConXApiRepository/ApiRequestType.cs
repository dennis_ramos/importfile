namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ApiRequestType")]
    public partial class ApiRequestType
    {
        public ApiRequestType()
        {
            ApiRequest = new HashSet<ApiRequest>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ApiRequestTypeID { get; set; }

        [Required]
        [StringLength(50)]
        public string ApiRequestTypeDescription { get; set; }

        public virtual ICollection<ApiRequest> ApiRequest { get; set; }
    }
}
