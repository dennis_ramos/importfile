namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cfgWorkFlowTasks")]
    public partial class WorkFlowTask
    {
        public WorkFlowTask()
        {
            jobTasks = new HashSet<JobTask>();
            WorkflowTaskItems = new HashSet<WorkFlowTaskItem>();
            SubWorkFlowTasks = new HashSet<WorkFlowTask>();
        }

        [Key]
        public Guid TaskID { get; set; }

        public Guid? WorkFlowID { get; set; }

        public short? TaskOrder { get; set; }

        [StringLength(50)]
        public string TaskName { get; set; }

        public float? Default_enumTaskQualifier { get; set; }

        public bool? Default_isTaskQualifierForced { get; set; }

        public float? enumWFTaskType { get; set; }

        [StringLength(50)]
        public string CustomFormToRun { get; set; }

        [StringLength(1000)]
        public string ValidationExpression { get; set; }

        public float? enumCriteria { get; set; }

        [StringLength(50)]
        public string CriteriaLow { get; set; }

        [StringLength(50)]
        public string CriteriaHigh { get; set; }

        public bool? SynchronizeWithDB { get; set; }

        public float? enumAssetType { get; set; }

        public bool wadtDeleted { get; set; }

        [StringLength(15)]
        public string wadtModifiedBy { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        [StringLength(6)]
        public string wadtModifiedOnDTOffset { get; set; }

        public Guid? wadtRowID { get; set; }

        public bool? techCanAddItOnFiled { get; set; }

        public Guid? ParentTaskID { get; set; }

        public bool? HasChildren { get; set; }

        public bool? AddByDefaultToJob { get; set; }

        public bool? isExistingAsset { get; set; }

        public bool? RepeateTaskForEachAsset { get; set; }

        public bool? RepeateItemsForEachAsset { get; set; }

        public bool? NotifyOnCompletion { get; set; }

        [StringLength(200)]
        public string NotifyEmail { get; set; }

        public Guid? NotifyReportID { get; set; }

        public Guid rowguid { get; set; }

        public virtual WorkFlow WorkFlow { get; set; }

        public virtual EnumAssetType enumAssetType1 { get; set; }

        public virtual EnumWFTaskType enumWFTaskType1 { get; set; }

        public virtual ICollection<JobTask> jobTasks { get; set; }

        public virtual WorkFlowTask ParentWorkFlowTask { get; set; }

        public ICollection<WorkFlowTask> SubWorkFlowTasks { get; set; }

        public ICollection<WorkFlowTaskItem> WorkflowTaskItems { get; set; }
    }
}
