namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("jobJob")]
    public partial class JobJob
    {
        public JobJob()
        {
            FieldOutcomeHistories = new HashSet<FieldOutcomeHistory>();
            JobAddresses = new HashSet<JobAddress>();
            JobAssets = new HashSet<JobAsset>();
            JobContacts = new HashSet<JobContact>();
            JobDeliverables = new HashSet<JobDeliverable>();
            JobGPSs = new HashSet<jobGps>();
            JobJobAttributes = new HashSet<JobJobAttribute>();
            JobNotes = new HashSet<JobNote>();
            JobPhotos = new HashSet<JobPhoto>();
            JobTasks = new HashSet<JobTask>();
        }

        [Key]
        public Guid JobID { get; set; }

        [Required]
        [StringLength(10)]
        public string FSP { get; set; }

        [StringLength(10)]
        public string WorkDoneByFSP { get; set; }

        [StringLength(50)]
        public string JobNo { get; set; }

        [StringLength(50)]
        public string COReferenceNo { get; set; }

        public Guid? ContractID { get; set; }

        public Guid? OrgID { get; set; }

        public Guid? DeviceID { get; set; }

        [StringLength(16)]
        public string DeviceName { get; set; }

        [StringLength(250)]
        public string Description { get; set; }

        public float enumJobStatus { get; set; }

        [StringLength(50)]
        public string StatusBy { get; set; }

        public DateTime? StatusOn { get; set; }

        [StringLength(6)]
        public string StatusOnOffset { get; set; }

        public bool Completed { get; set; }

        [StringLength(50)]
        public string CompletedBy { get; set; }

        public DateTime? CompletedOn { get; set; }

        [StringLength(5)]
        public string CompletedOnOffset { get; set; }

        public Guid WorkFlowID { get; set; }

        public int jOrder { get; set; }

        public float enumJobType { get; set; }

        public DateTime? ScheduledForDate { get; set; }

        [StringLength(6)]
        public string ScheduledForDateOffset { get; set; }

        [StringLength(50)]
        public string ScheduledForTech { get; set; }

        public float enumRegions { get; set; }

        public bool wadtDeleted { get; set; }

        [StringLength(15)]
        public string wadtModifiedBy { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        [StringLength(6)]
        public string wadtModifiedOnDTOffset { get; set; }

        public Guid? wadtRowID { get; set; }

        [StringLength(50)]
        public string Group1 { get; set; }

        [StringLength(50)]
        public string Group2 { get; set; }

        [StringLength(50)]
        public string Group3 { get; set; }

        public float? enumFieldJobStatus { get; set; }

        [StringLength(50)]
        public string FieldOutcome { get; set; }

        [StringLength(100)]
        public string FieldOutcomeReason { get; set; }

        public float? enumJobQualifier { get; set; }

        public bool? isMoved { get; set; }

        public Guid rowguid { get; set; }

        public int? wfDuration { get; set; }

        public DateTime? ScheduledForDate2 { get; set; }

        public float? enumDayPart { get; set; }

        public bool? isAppointment { get; set; }

        public DateTime? JobSyncDateTime { get; set; }

        public float? enumJobSyncStatus { get; set; }

        public float? enumJobSource { get; set; }

        public DateTime? FieldOutcomeDate { get; set; }

        public virtual CfgContract CfgContract { get; set; }

        public virtual CfgFSP CfgFSP { get; set; }

        public virtual WorkFlow WorkFlow { get; set; }

        public virtual CimOrganisation CimOrganisation { get; set; }

        public virtual EnumDayPart EnumDayPart { get; set; }

        public virtual EnumJobSource EnumJobSource { get; set; }

        public virtual EnumJobStatus EnumJobStatus { get; set; }

        public virtual EnumJobStatus EnumFieldJobStatus { get; set; }

        public virtual EnumJobType EnumJobType { get; set; }

        public virtual EnumRegion EnumRegion { get; set; }

        public virtual ICollection<FieldOutcomeHistory> FieldOutcomeHistories { get; set; }

        public virtual ICollection<JobAddress> JobAddresses { get; set; }

        public virtual ICollection<JobAsset> JobAssets { get; set; }

        public virtual ICollection<JobContact> JobContacts { get; set; }

        public virtual ICollection<JobDeliverable> JobDeliverables { get; set; }

        public virtual ICollection<jobGps> JobGPSs { get; set; }

        public virtual ICollection<JobJobAttribute> JobJobAttributes { get; set; }

        public virtual ICollection<JobNote> JobNotes { get; set; }

        public virtual ICollection<JobPhoto> JobPhotos { get; set; }

        public virtual ICollection<JobTask> JobTasks { get; set; }
    }
}
