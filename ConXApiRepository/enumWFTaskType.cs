namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("enumWFTaskType")]
    public partial class EnumWFTaskType
    {
        public EnumWFTaskType()
        {
            cfgWorkFlowTasks = new HashSet<WorkFlowTask>();
        }

        [Key]
        [Column("enumWFTaskType")]
        public float enumWFTaskType { get; set; }

        [StringLength(50)]
        public string enumDesc { get; set; }

        [StringLength(10)]
        public string enumSDesc { get; set; }

        public Guid rowguid { get; set; }

        public virtual ICollection<WorkFlowTask> cfgWorkFlowTasks { get; set; }
    }
}
