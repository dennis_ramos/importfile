namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ApiRequestDetailResult")]
    public partial class ApiRequestDetailResult
    {
        public int ID { get; set; }

        public int ApiRequestDetailID { get; set; }

        public Guid ResultID { get; set; }

        [Required]
        [StringLength(200)]
        public string ResultDescription { get; set; }

        public virtual ApiRequestDetail ApiRequestDetail { get; set; }
    }
}
