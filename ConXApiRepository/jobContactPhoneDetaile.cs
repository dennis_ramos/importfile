namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("jobContactPhoneDetaile")]
    public partial class jobContactPhoneDetail
    {
        [Key]
        public Guid JobContactPhoneID { get; set; }

        public Guid JobContactID { get; set; }

        public float enumPhoneType { get; set; }

        [StringLength(200)]
        public string Phone { get; set; }

        public bool wadtDeleted { get; set; }

        [StringLength(15)]
        public string wadtModifiedBy { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        [StringLength(6)]
        public string wadtModifiedOnDTOffset { get; set; }

        public Guid? wadtRowID { get; set; }

        public bool? PreferedContact { get; set; }

        public float? enumCountry { get; set; }

        public virtual EnumPhoneType enumPhoneType1 { get; set; }

        public virtual JobContact JobContact { get; set; }
    }
}
