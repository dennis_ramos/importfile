namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    [Table("ApiRequestStatus")]
    public partial class ApiRequestStatus
    {
        public ApiRequestStatus()
        {
            ApiRequest = new HashSet<ApiRequest>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ApiRequestStatusId { get; set; }

        [StringLength(50)]
        public string ApiRequestStatusDescription { get; set; }

        public virtual ICollection<ApiRequest> ApiRequest { get; set; }
    }
}
