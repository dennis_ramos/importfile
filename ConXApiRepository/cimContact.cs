namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    [Table("cimContacts")]
    public partial class CimContact
    {
        public CimContact()
        {
            cfgContracts = new HashSet<CfgContract>();
        }

        [Key]
        public Guid ContactID { get; set; }

        public Guid? OrgID { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        [StringLength(100)]
        public string FullName { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string MidName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(200)]
        public string Email { get; set; }

        public float enumActive { get; set; }

        public bool wadtDeleted { get; set; }

        [StringLength(15)]
        public string wadtModifiedBy { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        [StringLength(6)]
        public string wadtModifiedOnDTOffset { get; set; }

        public Guid? wadtRowID { get; set; }

        public Guid rowguid { get; set; }

        public bool? AllowAccessToPortal { get; set; }

        public virtual ICollection<CfgContract> cfgContracts { get; set; }

        public virtual CimOrganisation cimOrganisation { get; set; }
    }
}
