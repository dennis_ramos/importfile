﻿
namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cfgContractReceivables")]
    public partial class CfgContractReceivable
    {
        public CfgContractReceivable()
        {
            ImpImports = new HashSet<impImport>();
        }

        [Key]
        public Guid ContrRecvID { get; set; }

        public Guid? ContractID { get; set; }

        [StringLength(50)]
        public string ReceivableName { get; set; }

        public float? enumReceivableFormat { get; set; }

        public float? enumReceivableType { get; set; }

        public virtual CfgContract cfgContract { get; set; }

        public virtual EnumReceivableFormat EnumReceivableFormat { get; set; }

        public virtual ICollection<impImport> ImpImports { get; set; }
    }
}
