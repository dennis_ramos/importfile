﻿namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("enumReceivableType")]
    public partial class EnumReceivableType
    {
        [Key]
        [Column("enumReceivableType")]
        public float enumReceivableType { get; set; }

        [StringLength(50)]
        public string enumDesc { get; set; }

        [StringLength(10)]
        public string enumSdesc { get; set; }
    }
}