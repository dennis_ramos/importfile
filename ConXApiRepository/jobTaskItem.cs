namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("jobTaskItem")]
    public partial class JobTaskItem
    {
        public Guid JobTaskItemID { get; set; }

        public Guid JobTaskID { get; set; }

        public Guid TaskItemID { get; set; }

        [StringLength(50)]
        public string TaskItemName { get; set; }

        [StringLength(50)]
        public string Result { get; set; }

        [StringLength(50)]
        public string ValidationResult { get; set; }

        [StringLength(50)]
        public string EnteredBy { get; set; }

        public DateTime? EnteredOn { get; set; }

        [StringLength(6)]
        public string EnteredOnOffset { get; set; }

        public bool wadtDeleted { get; set; }

        [StringLength(15)]
        public string wadtModifiedBy { get; set; }

        public DateTime? wadtModifiedOn { get; set; }

        [StringLength(6)]
        public string wadtModifiedOnDTOffset { get; set; }

        public Guid? wadtRowID { get; set; }

        public int? ItemOrder { get; set; }

        public Guid rowguid { get; set; }

        public virtual WorkFlowTaskItem WorkFlowTaskItem { get; set; }

        public virtual JobTask JobTask { get; set; }
    }
}
