namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("enumJobStatus")]
    public partial class EnumJobStatus
    {
        public EnumJobStatus()
        {
            jobJobs = new HashSet<JobJob>();
            jobJobs1 = new HashSet<JobJob>();
        }

        [Key]
        public float enumJobStatus { get; set; }

        [StringLength(50)]
        public string EnumDesc { get; set; }

        [StringLength(10)]
        public string EnumSDesc { get; set; }

        public Guid rowguid { get; set; }

        public virtual ICollection<JobJob> jobJobs { get; set; }

        public virtual ICollection<JobJob> jobJobs1 { get; set; }
    }
}
