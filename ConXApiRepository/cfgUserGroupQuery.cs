namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("cfgUserGroupQuery")]
    public partial class CfgUserGroupQuery
    {
        [Key]
        public Guid UGQID { get; set; }

        public Guid UserGroupId { get; set; }

        public Guid QueryID { get; set; }

        public virtual CfgUserGroup cfgUserGroup { get; set; }
    }
}
