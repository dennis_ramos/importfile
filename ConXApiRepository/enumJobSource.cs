namespace ConXApiRepository
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("enumJobSource")]
    public partial class EnumJobSource
    {
        public EnumJobSource()
        {
            jobJobs = new HashSet<JobJob>();
        }

        [Key]
        [Column("enumJobSource")]
        public float enumJobSource { get; set; }

        [StringLength(50)]
        public string enumDesc { get; set; }

        [StringLength(50)]
        public string enumSDesc { get; set; }

        public Guid rowguid { get; set; }

        public virtual ICollection<JobJob> jobJobs { get; set; }
    }
}
