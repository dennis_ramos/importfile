﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExcelReader.ImportV2;
using r = ConXApiDtoModels.DataCreationRequest;
using ImportV2.ApiMapper;
using ConXApiRepository;
using log4net;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace ExcelReader
{
    class Program
    {
        private static log4net.ILog _log;

        static void Main(string[] args)
        {
            log4net.GlobalContext.Properties["LogName"] = string.Format("{0}_{1}.log", "Excel Import", DateTime.Now.ToString("yyyyMMdd"));
            _log = log4net.LogManager.GetLogger("Excel Import");
            _log.Info("Program Starts");
            ProcessPendingApiRequest();
            ProcessImportV2File().Wait();
            //ProcessOneFile().Wait();
            Console.WriteLine("Process Completed");
            _log.Info("Programs Closes");

        }

        static void ProcessPendingApiRequest()
        {
            _log.Info("Check for Web API Job Creation Requests");
            try
            {
                using (ConXApiRepository.ConXApiModels db = new ConXApiModels())
                {
                    db.Database.CommandTimeout = 300;
                    //var pendingRequestList = db.ApiRequests.Where(a => a.ApiRequestStatusId == 1).OrderBy(b => b.RequestedOn).ToList();
                    var pendingRequestList = db.ApiRequests.Where(a => a.RequestID == 895).OrderBy(b => b.RequestedOn).ToList();


                    if (pendingRequestList.Count == 0) return;

                    ConXApiMapper.JobCreation jobCreation = new ConXApiMapper.JobCreation(db);

                    foreach (var pendingRequest in pendingRequestList)
                    {
                        _log.Info("Creating jobs for RequestID " + pendingRequest.RequestID.ToString());
                        jobCreation.ProcessRequestID(pendingRequest.RequestID);
                        _log.Info("Completed Creating Jobs for RequestID" + pendingRequest.RequestID.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
            }
        }

        private static void LogException(Exception ex)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Exception Caught:");
            sb.Append("Exception = " + ex.Message);
            if (ex.InnerException != null)
                sb.AppendLine("Inner Exception = " + ex.InnerException.Message);
            sb.AppendLine("Stack Trace: " + ex.StackTrace);
            _log.Error(sb.ToString());
        }

        static async Task ProcessOneFile()
        {
            Mapper mapper = new Mapper(@"C:\Conx Utilities\ConXMiscFileRepository\ConX Import File - AMS Meter Survey 1.xls", "WELLS", "dramos", "dennis.ramos@e-mergedata.com");
            bool success = await mapper.ProcessFile();
        }

        static async Task ProcessImportV2File()
        {
            _log.Info("Reading impImport Table");
            try
            {
                using (ConXApiRepository.ConXApiModels db = new ConXApiModels())
                {

                    foreach (var import in db.impImports.Where(a => a.enumImpStatus == 1f && a.cfgContractReceivable.enumReceivableFormat == 12f).OrderBy(b => b.StatusOn).ToList())
                    {
                        _log.Info("Start Processing ImportID '" + import.ImportID.ToString() + "' FileName='" + import.FileFullPath + "'. Set status to In Progress");
                        import.enumImpStatus = 2f;
                        import.ProcessedOn = DateTime.Now;
                        import.ProcessedOnOffset = DateTime.Now.ToString("zzzz");

                        await db.SaveChangesAsync();

                        string sourceFileName = Properties.Settings.Default.FileStoratePath + import.FileFullPath;
                        string destinationFileName = Properties.Settings.Default.ProcessedFileStoragePath + import.FSP.Trim() + @"\" + import.FileFullPath;
                        string destinationFolder = Properties.Settings.Default.ProcessedFileStoragePath + import.FSP.Trim() + @"\";

                        if (System.IO.File.Exists(sourceFileName) == true)
                        {

                            Mapper mapper = new Mapper(sourceFileName, import.FSP.Trim(), import.StatusBy.Trim(), import.Reference);
                            bool success = await mapper.ProcessFile();

                            if (success)
                            {
                                _log.Info("Import Successful");
                                import.enumImpStatus = 3f;
                            }
                            else
                            {
                                _log.Error("Import Failed");
                                import.enumImpStatus = 9f;
                                import.ImportMSG = "Error: import failed";
                            }

                            //move the file
                            _log.Info("Moving File to '" + destinationFolder + "'");
                            MoveFile(import.FSP.Trim(), sourceFileName, destinationFileName, destinationFolder);


                        }
                        else
                        {
                            _log.Error("File: " + sourceFileName + " Could not be found");
                            import.enumImpStatus = 9f;
                            import.ImportMSG = "Error: File not found";
                        }


                        await db.SaveChangesAsync();

                    }
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
            }

        }

        static private void MoveFile(string fsp, string sourceFile, string destinationFile, string destinationFolder)
        {
            try
            {
                if (System.IO.Directory.Exists(destinationFolder) == false)
                {
                    System.IO.Directory.CreateDirectory(destinationFolder);
                }

                System.IO.File.Copy(sourceFile, destinationFile);
                System.IO.File.Delete(sourceFile);
            }
            catch (Exception ex)
            {
                LogException(ex);

            }
        }

    }
}
