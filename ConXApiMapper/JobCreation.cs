﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConXApiRepository;
using ConXApiDtoModels.DataCreationRequest;
using Newtonsoft.Json;
using System.Data.SqlClient;
using System.Data.Entity;
using System.ComponentModel;
using System.Data.Entity.Validation;
using log4net;
using System.Reflection;
using System.Threading;

namespace ConXApiMapper
{


    public class JobCreation
    {

        private ConXApiModels _db;

        private List<ApiRequestStatus> _apirRequestStatuses;
        private List<ApiRequestType> _apiRequestTypes;
        private List<ApiRequest> _apiRequests;

        private List<EnumAssetType> _enumAssetTypes;
        private List<EnumPhoneType> _enumPhoneTypes;
        private List<EnumNoteType> _enumNoteTypes;
        private List<EnumDayPart> _enumDayParts;
        private List<EnumJobType> _enumJobTypes;
        private List<EnumRegion> _enumRegions;

        private List<CfgContract> _cfgContracts = new List<CfgContract>();
        private List<EnumCountry> _enumCountries;

        private StringBuilder _processLog = new StringBuilder();

        private int _jobRequestCount = 0;
        private StringBuilder sb = new StringBuilder();
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public JobCreation()
        {
            _db = new ConXApiModels();
            _db.Database.CommandTimeout = 300;

            if (Properties.Settings.Default.CommandTimeOut != null)
            {
                _db.Database.CommandTimeout = Properties.Settings.Default.CommandTimeOut;
            }



            Initialize();
        }

        public JobCreation(ConXApiRepository.ConXApiModels db)
        {
            _db = db;
            Initialize();
        }

        public void Initialize()
        {
            _apirRequestStatuses = new List<ApiRequestStatus>();
            _apirRequestStatuses = _db.ApiRequestStatuses.ToList();

            _apiRequestTypes = new List<ApiRequestType>();
            _apiRequestTypes = _db.ApiRequestTypes.ToList();

            _apiRequests = new List<ApiRequest>();
            _apiRequests = _db.ApiRequests.Where(a => a.ApiRequestStatusId == 1 && a.ApiRequestDetail.Count() > 1).ToList();

            _enumAssetTypes = new List<EnumAssetType>();
            _enumAssetTypes = _db.enumAssetTypes.ToList();

            _enumPhoneTypes = new List<EnumPhoneType>();
            _enumPhoneTypes = _db.enumPhoneTypes.ToList();

            _enumNoteTypes = new List<EnumNoteType>();
            _enumNoteTypes = _db.enumNoteTypes.ToList();

            _enumDayParts = new List<EnumDayPart>();
            _enumDayParts = _db.enumDayParts.ToList();

            _enumJobTypes = new List<EnumJobType>();
            _enumJobTypes = _db.enumJobTypes.ToList();

            _enumRegions = new List<EnumRegion>();
            _enumRegions = _db.enumRegions.ToList();

            _enumCountries = _db.enumCountries.ToList();
        }

        private void LogException(Exception ex)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Error Caught: exception = " + ex.Message);
            if (ex.InnerException != null)
            {
                sb.AppendLine("Inner Exception = " + ex.InnerException.Message);
                if (ex.InnerException.InnerException != null)
                    sb.AppendLine("Inner Inner Exception = " + ex.InnerException.InnerException.Message);
            }



            sb.AppendLine("Stack Trace: ");
            sb.AppendLine(ex.StackTrace);
            _log.Error(sb.ToString());
        }

        private void ValidateRequestHeaders(Request request, string FSP, string userName, StringBuilder sb)
        {
            List<string> RequestTypes = new List<string> { "Create Job", "Copy Job", "Create VVL", "Update VVL" };

            if (string.IsNullOrWhiteSpace(request.FSP))
                sb.AppendLine("FSP cannot be empty");

            if (!string.IsNullOrWhiteSpace(request.FSP) && request.FSP.ToUpper() != FSP.ToUpper())
                sb.AppendLine("FSP: '" + request.FSP + "' is not valid");


            if (string.IsNullOrWhiteSpace(request.RequestType))
                sb.AppendLine("Request Type cannot be empty");

            if (!string.IsNullOrWhiteSpace(request.RequestType) && !RequestTypes.Contains(request.RequestType))
                sb.AppendLine("Request Type: '" + request.RequestType + "' is not valid");
        }

        private void ValidateRequestBody(JobRequest requestBody, StringBuilder sb)
        {
            //ValidateJobSchedulingData(requestBody.JobSchedule, sb);
            //ValidateJobAddressesData(requestBody.JobAddresses, sb);
            //ValidateJobContactsData(requestBody.JobContacts, sb);
            //ValidateJobAssetsData(requestBody.JobAssets, sb);
            //ValidateJobAttributeData(requestBody.JobAttributes, sb);
            //ValidateNonAssetBasedTaskPrePopulationData(requestBody.NonAssetBasedTaskPrePopulations, sb);
            //ValidateJNotesData(requestBody.JobNotes, sb);


            //validate job properties (lenth of the string, isRequired or Not)
            //validate job address 
            //validate scheduling properties
            //valiate job attributes
            //validate job notes
            //validate job assets
            //Asset Type Check against the enumAssetType
            //validate job contacts
            //validate NonAssetBasedTaskPrePopulations

        }

        public RequestResponse ProcessRequest(Request request, string FSP, string userName, bool proccessAsync = true)
        {
            _log.Info("ProcessRequest Method Start");

            StringBuilder sb = new StringBuilder();
            sb.Append("");

            ValidateRequestHeaders(request, FSP, userName, sb);

            if (sb.ToString().Length > 0)
            {
                return new RequestResponse { FSP = request.FSP, RequestedOn = DateTime.Now, RequestedBy = request.RequestedBy, RequestStatus = "Error", Notes = sb.ToString() };
            }

            this._jobRequestCount = request.JobRequests.Count;

            ApiRequest apiRequest = new ApiRequest() { RequestedOn = DateTime.Now, FSP = request.FSP, RequestedBy = request.RequestedBy };

            apiRequest.ApiRequestStatusId = GetApiRequestStatus("In Queue").ApiRequestStatusId;
            apiRequest.ApiRequestTypeID = GetApiRequestTypes(request.RequestType).ApiRequestTypeID;

            _log.Info("start Serialize object ");
            foreach (var jRequest in request.JobRequests.ToList())
            {
                apiRequest.ApiRequestDetail.Add(new ApiRequestDetail { RequestData = JsonConvert.SerializeObject(jRequest) });
            }

            _log.Info("End serialize object");

            RequestResponse response = new RequestResponse();
            try
            {
                _log.Info("Start Save Api Request");

                _db.ApiRequests.Add(apiRequest);
                _db.SaveChanges();

                _log.Info("End Save Api Request");


                if (request.JobRequests.Count == 1)
                {
                    response = CreateJobFromApiRequest(apiRequest);
                }
                else
                {
                    response = new RequestResponse { RequestID = apiRequest.RequestID, FSP = request.FSP, RequestedOn = DateTime.Now, RequestedBy = request.RequestedBy, RequestStatus = apiRequest.ApiRequestStatus.ApiRequestStatusDescription, Notes = "Request/GetJobsByRequestID(RequestID='" + apiRequest.RequestID.ToString() + "')", RequestType = request.RequestType };

                    //if (proccessAsync)
                    //{
                    //    Task<RequestResponse> ProcessRequestWithMultiple = new Task<RequestResponse>(param =>
                    //    {
                    //        return CreateJobFromApiRequest((ApiRequest)param);
                    //    }, apiRequest);

                    //    ProcessRequestWithMultiple.Start();
                    //}
                }
            }
            catch (Exception e)
            {
                LogException(e);
                return null;
            }

            return response;
        }
        
        public void ProcessRequestID(int requestId)
        {
            ApiRequest request = _db.ApiRequests.FirstOrDefault(a => a.RequestID == requestId);
            CreateJobFromApiRequest(request);
        }

        public async Task<bool> ProcessRequestIdAsyc(int requestId, bool isNewProcess)
        {
            ApiRequest request = await _db.ApiRequests.FirstOrDefaultAsync(a => a.RequestID == requestId).ConfigureAwait(false);
            RequestResponse response = await CreateJobFromApiRequestAsync(request, isNewProcess);
            if (response.Notes.Contains("Continue"))
                return true;
            else
                return false;
        }

        public StringBuilder GetProcessLog(int requestId, out bool hasCreationErrors)
        {
            hasCreationErrors = false;

            StringBuilder sb = new StringBuilder();
            ApiRequest request = _db.ApiRequests.FirstOrDefault(a => a.RequestID == requestId);

            foreach (var apiRequestDetail in request.ApiRequestDetail.Where(a => a.ProcessedOn != null).OrderBy(b => b.ProcessedOn).ToList())
            {
                var jobRequest = JsonConvert.DeserializeObject<JobRequest>(apiRequestDetail.RequestData);
                if (apiRequestDetail.HasError.HasValue)
                    if (apiRequestDetail.HasError.Value)
                    {
                        sb.AppendLine(apiRequestDetail.ProcessedOn.Value.ToString("yyyy-MM-dd HH:mm:ss") + "Error creating job(s) for Job Ref= " + jobRequest.ImportFileJobRef + " Description = " + jobRequest.Description +
                            "Error : " + apiRequestDetail.ErrorDescription);
                        hasCreationErrors = true;
                    }
                    else
                    {
                        int occurence = 1;
                        foreach (var jobResult in apiRequestDetail.ApiRequestDetailResult.OrderBy(a => a.ID).ToList())
                        {
                            sb.AppendLine(apiRequestDetail.ProcessedOn.Value.ToString("yyyy-MM-dd HH:mm:ss") + " Successfully created job for Job Ref= " + jobRequest.ImportFileJobRef + "  Description = " + jobRequest.Description + " Occurrence(" + occurence.ToString() + ") : Job No: " + jobResult.ResultDescription);
                            occurence += 1;
                        }
                    }
            }

            return sb;

            //sb.AppendLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + logEvent);
        }

        public void ProcessRequestWithMultiple()
        {
            var apiRequestList = _apiRequests;
            foreach (var apiRequest in apiRequestList)
            {
                CreateJobFromApiRequest(apiRequest);
            }
        }


        public async Task<RequestResponse> CreateJobFromApiRequestAsync(ApiRequest request, bool isNewProcess)
        {
            //_log.Info("Start Creating Job");


            //int createdJobs = 0;
            if (isNewProcess)
            {
                request.ProcessedOn = DateTime.Now;
                request.ApiRequestStatusId = GetApiRequestStatus("In Progress").ApiRequestStatusId;

                await _db.SaveChangesAsync().ConfigureAwait(false);
            }

            RequestResponse response = new
            RequestResponse
            {
                FSP = request.FSP,
                JobRequestResponses = new List<JobRequestResponse>(),
                ProcessedOn = request.ProcessedOn,
                Notes = "",
                RequestedBy = request.RequestedBy,
                RequestedOn = request.RequestedOn,
                RequestID = request.RequestID,
                RequestType = request.ApiRequestType.ApiRequestTypeDescription
            };

            StringBuilder sb = new StringBuilder();
            int counter = 0;
            int batchCounter = 0;
            int jobCounter = 0;
            int lastSaveCounter = 0;

            foreach (var apiRequestDetail in request.ApiRequestDetail.Where(a => a.Processed == false).ToList())
            {

                var jobRequest = JsonConvert.DeserializeObject<JobRequest>(apiRequestDetail.RequestData);

                counter++;
                batchCounter++;
                sb.Append("");
                ValidateRequestBody(jobRequest, sb);

                if (sb.ToString().Length == 0)
                {
                    try
                    {

                        jobCounter += await ProcessJobCreationAsync(jobRequest, apiRequestDetail, request);

                        _log.Info("Start Saving Job");
                        if (jobCounter - lastSaveCounter >= 10)
                        {
                            await _db.SaveChangesAsync().ConfigureAwait(false); ;
                            lastSaveCounter = jobCounter;
                        }
                        _log.Info("End Saving Job");

                        var jobRequestResponse = new JobRequestResponse { JobReference = jobRequest.JobReference, ErrorDescription = "", HasError = false, Processed = true, ProcessedOn = apiRequestDetail.ProcessedOn, JobRequestResponseDetail = new List<JobRequestResponseDetail>() };
                        foreach (var details in apiRequestDetail.ApiRequestDetailResult)
                        {
                            jobRequestResponse.JobRequestResponseDetail.Add(new JobRequestResponseDetail { JobId = details.ResultID, JobNo = details.ResultDescription, Uri = "/GetJobsByJobNumber(JobNo='" + details.ResultDescription + "')" });
                            //createdJobs += 1;
                        }

                        response.JobRequestResponses.Add(jobRequestResponse);

                        if (jobCounter >= 1000)
                        {
                            await _db.SaveChangesAsync().ConfigureAwait(false);
                            response.Notes = "Continue";
                            return response;
                        }
                    }
                    catch (Exception ex)
                    {
                        apiRequestDetail.HasError = true;

                        apiRequestDetail.ErrorDescription = "Unhandled Exception: ";
                        if (ex.ToString().Length > 1968)
                        {
                            apiRequestDetail.ErrorDescription += ex.ToString().Substring(0, 1960);
                        }
                        else
                        {
                            apiRequestDetail.ErrorDescription += ex.ToString();
                        }

                        apiRequestDetail.Processed = true;
                        apiRequestDetail.ProcessedOn = DateTime.Now;
                        response.JobRequestResponses.Add(new JobRequestResponse { JobReference = jobRequest.JobReference, ErrorDescription = apiRequestDetail.ErrorDescription, HasError = true, Processed = true, ProcessedOn = apiRequestDetail.ProcessedOn });
                        request.Notes += string.IsNullOrWhiteSpace(request.Notes) ? "" : request.Notes + " \r\n" + "Error creating job for Job Reference " + jobRequest.JobReference + " please see request response for details";
                        LogException(ex);
                    }
                }
                else
                {


                    apiRequestDetail.HasError = true;
                    apiRequestDetail.ErrorDescription = sb.ToString();
                    apiRequestDetail.Processed = true;
                    apiRequestDetail.ProcessedOn = DateTime.Now;
                    response.JobRequestResponses.Add(new JobRequestResponse { JobReference = jobRequest.JobReference, ErrorDescription = apiRequestDetail.ErrorDescription, HasError = true, Processed = true, ProcessedOn = apiRequestDetail.ProcessedOn });
                    response.Notes += "\r\n" + "Data Validation error: " + apiRequestDetail.ErrorDescription;
                    response.CompletedOn = DateTime.Now;
                    request.Notes = string.IsNullOrWhiteSpace(request.Notes) ? "" : request.Notes + " \r\n" + "Data Validation error: " + apiRequestDetail.ErrorDescription;

                }

                sb = new StringBuilder();
            }


            request.CompletedOn = DateTime.Now;
            request.ApiRequestStatusId = GetApiRequestStatus("Completed").ApiRequestStatusId;
            await _db.SaveChangesAsync().ConfigureAwait(false);

            response.CompletedOn = request.CompletedOn;
            response.Notes = "Completed";
            response.RequestStatus = "Completed";


            return response;
        }

        public RequestResponse CreateJobFromApiRequest(ApiRequest request)
        {
            //_log.Info("Start Creating Job");

            int createdJobs = 0;
            request.ProcessedOn = DateTime.Now;
            request.ApiRequestStatusId = GetApiRequestStatus("In Progress").ApiRequestStatusId;

            _db.SaveChanges();

            RequestResponse response = new
            RequestResponse
            {
                FSP = request.FSP,
                JobRequestResponses = new List<JobRequestResponse>(),
                ProcessedOn = request.ProcessedOn,
                Notes = "",
                RequestedBy = request.RequestedBy,
                RequestedOn = request.RequestedOn,
                RequestID = request.RequestID,
                RequestType = request.ApiRequestType.ApiRequestTypeDescription
            };

            StringBuilder sb = new StringBuilder();
            int counter = 0;
            //foreach (var apiRequestDetail in request.ApiRequestDetail.Where(a=> a.Processed == false).ToList())
            foreach (var apiRequestDetail in request.ApiRequestDetail)
            {
                var jobRequest = JsonConvert.DeserializeObject<JobRequest>(apiRequestDetail.RequestData);

                counter++;

                sb.Append("");
                ValidateRequestBody(jobRequest, sb);

                if (sb.ToString().Length == 0)
                {
                    try
                    {

                        ProcessJobCreation(jobRequest, apiRequestDetail, request);

                        _log.Info("Start Saving Job");
                        _db.SaveChanges();
                        _log.Info("End Saving Job");
                        if (counter >= 800)
                        {
                            Thread.Sleep(10000);
                            counter = 0;
                        }


                        var jobRequestResponse = new JobRequestResponse { JobReference = jobRequest.JobReference, ErrorDescription = "", HasError = false, Processed = true, ProcessedOn = apiRequestDetail.ProcessedOn, JobRequestResponseDetail = new List<JobRequestResponseDetail>() };
                        foreach (var details in apiRequestDetail.ApiRequestDetailResult)
                        {
                            jobRequestResponse.JobRequestResponseDetail.Add(new JobRequestResponseDetail { JobId = details.ResultID, JobNo = details.ResultDescription, Uri = "/GetJobsByJobNumber(JobNo='" + details.ResultDescription + "')" });
                            createdJobs += 1;
                        }

                        response.JobRequestResponses.Add(jobRequestResponse);
                    }
                    catch (Exception ex)
                    {
                        apiRequestDetail.HasError = true;

                        apiRequestDetail.ErrorDescription = "Unhandled Exception: ";
                        if (ex.ToString().Length > 1968)
                        {
                            apiRequestDetail.ErrorDescription += ex.ToString().Substring(0, 1960);
                        }
                        else
                        {
                            apiRequestDetail.ErrorDescription += ex.ToString();
                        }

                        apiRequestDetail.Processed = true;
                        apiRequestDetail.ProcessedOn = DateTime.Now;
                        response.JobRequestResponses.Add(new JobRequestResponse { JobReference = jobRequest.JobReference, ErrorDescription = apiRequestDetail.ErrorDescription, HasError = true, Processed = true, ProcessedOn = apiRequestDetail.ProcessedOn });
                        request.Notes += string.IsNullOrWhiteSpace(request.Notes) ? "" : request.Notes + " \r\n" + "Error creating job for Job Reference " + jobRequest.JobReference + " please see request response for details";
                        LogException(ex);
                    }
                }
                else
                {


                    apiRequestDetail.HasError = true;
                    apiRequestDetail.ErrorDescription = sb.ToString();
                    apiRequestDetail.Processed = true;
                    apiRequestDetail.ProcessedOn = DateTime.Now;
                    response.JobRequestResponses.Add(new JobRequestResponse { JobReference = jobRequest.JobReference, ErrorDescription = apiRequestDetail.ErrorDescription, HasError = true, Processed = true, ProcessedOn = apiRequestDetail.ProcessedOn });
                    response.Notes += "\r\n" + "Data Validation error: " + apiRequestDetail.ErrorDescription;
                    response.CompletedOn = DateTime.Now;
                    request.Notes = string.IsNullOrWhiteSpace(request.Notes) ? "" : request.Notes + " \r\n" + "Data Validation error: " + apiRequestDetail.ErrorDescription;

                }

                sb = new StringBuilder();
            }


            request.CompletedOn = DateTime.Now;
            request.ApiRequestStatusId = GetApiRequestStatus("Completed").ApiRequestStatusId;
            _db.SaveChanges();

            response.CompletedOn = request.CompletedOn;
            response.Notes = "Completed";
            response.RequestStatus = "Completed";


            return response;
        }

        public void ProcessJobCreation(JobRequest jobRequest, ApiRequestDetail apiRequestDetail, ApiRequest request)
        {
            int numberOfJobsToCreate = 1;

            if (jobRequest.JobSchedule != null && jobRequest.JobSchedule.NumberOfOccurrences.HasValue)
            {
                numberOfJobsToCreate = jobRequest.JobSchedule.NumberOfOccurrences.Value;

            }

            string JobSource = "";
            string SeriesSource = "";
            DateTime? PlanFrom = null;
            DateTime? PlanTo = null;

            apiRequestDetail.Processed = true;
            apiRequestDetail.ProcessedOn = DateTime.Now;
            int counter = 0;
            try
            {

                for (int seriesCounter = 1; seriesCounter <= numberOfJobsToCreate; seriesCounter++)
                {
                    JobJob job = new JobJob();

                    _log.Info("Start Get Contract");
                    CfgContract contract = GetContract(jobRequest.WorkStream, request.FSP);

                    _log.Info("Start PopulateJobLevelProperties");
                    PopulateJobLevelProperties(jobRequest, request, job, contract);

                    JobSource = job.JobNo;

                    _log.Info("Start PopulateJobAttributes");
                    PopulateJobAttributes(jobRequest, request, job);

                    _log.Info("Start PopulateJobContacts");
                    PopulateJobContacts(jobRequest, request, job);

                    _log.Info("Start PopulateJobNotes");
                    PopulateJobNotes(jobRequest, request, job);

                    _log.Info("Start PopulateJobAddress");
                    PopulateJobAddress(jobRequest, request, job);

                    if (seriesCounter == 1)
                        SeriesSource = job.JobNo;


                    _log.Info("Start PopulateJobScheduleAndRecurringAttributes");
                    PopulateJobScheduleAndRecurringAttributes(jobRequest, request, numberOfJobsToCreate, JobSource, SeriesSource, ref PlanFrom, ref PlanTo, seriesCounter, job);

                    _log.Info("Start PopulateJobAssets");
                    PopulateJobAssets(jobRequest, request, job);

                    _log.Info("Start PopulateJobTasks");
                    PopulateJobTasks(jobRequest, request, job, contract);

                    _log.Info("Start PrePopulateBasedOnJobAttributes");
                    PrePopulateBasedOnJobAttributes(jobRequest, job);

                    _log.Info("Start PrePopulateBasedOnAssetAttributes");
                    PrePopulateBasedOnAssetAttributes(jobRequest, job);

                    _log.Info("Start Pre-Populate Non Asset Based Task Pre-Population");
                    PrePopulateNonAssetBased(jobRequest, job);

                    _log.Info("Start PrePopulateBasedOnXReference");
                    PrePopulateBasedOnXReference(job, contract);

                    CreateAssetLevelNotes(jobRequest, job);

                    CreateNonAssetBasedTaskNotes(jobRequest, job);


                    _db.jobJobs.Add(job);

                    apiRequestDetail.ApiRequestDetailResult.Add(
                        new ApiRequestDetailResult
                        {
                            ResultID = job.JobID,
                            ResultDescription = job.JobNo
                        });

                    if (numberOfJobsToCreate > 1)
                    {
                        counter++;
                        if (counter == 10)
                        {
                            _db.SaveChanges();
                            counter = 0;
                        }
                    }
                }

                apiRequestDetail.HasError = false;
                apiRequestDetail.ErrorDescription = "";
            }
            catch (Exception e)
            {
                apiRequestDetail.HasError = true;
                apiRequestDetail.ErrorDescription = e.ToString();
                LogException(e);
            }
        }



        public async Task<int> ProcessJobCreationAsync(JobRequest jobRequest, ApiRequestDetail apiRequestDetail, ApiRequest request)
        {
            int jobCounter = 0;
            int numberOfJobsToCreate = 1;

            if (jobRequest.JobSchedule != null && jobRequest.JobSchedule.NumberOfOccurrences.HasValue)
            {
                numberOfJobsToCreate = jobRequest.JobSchedule.NumberOfOccurrences.Value;

            }

            string JobSource = "";
            string SeriesSource = "";
            DateTime? PlanFrom = null;
            DateTime? PlanTo = null;

            apiRequestDetail.Processed = true;
            apiRequestDetail.ProcessedOn = DateTime.Now;
            int counter = 0;
            try
            {

                for (int seriesCounter = 1; seriesCounter <= numberOfJobsToCreate; seriesCounter++)
                {
                    JobJob job = new JobJob();
                    jobCounter++;

                    _log.Info("Start Get Contract");
                    CfgContract contract = GetContract(jobRequest.WorkStream, request.FSP);

                    _log.Info("Start PopulateJobLevelProperties");
                    PopulateJobLevelProperties(jobRequest, request, job, contract);

                    JobSource = job.JobNo;

                    _log.Info("Start PopulateJobAttributes");
                    PopulateJobAttributes(jobRequest, request, job);

                    _log.Info("Start PopulateJobContacts");
                    PopulateJobContacts(jobRequest, request, job);

                    _log.Info("Start PopulateJobNotes");
                    PopulateJobNotes(jobRequest, request, job);

                    _log.Info("Start PopulateJobAddress");
                    PopulateJobAddress(jobRequest, request, job);

                    if (seriesCounter == 1)
                        SeriesSource = job.JobNo;


                    _log.Info("Start PopulateJobScheduleAndRecurringAttributes");
                    PopulateJobScheduleAndRecurringAttributes(jobRequest, request, numberOfJobsToCreate, JobSource, SeriesSource, ref PlanFrom, ref PlanTo, seriesCounter, job);

                    _log.Info("Start PopulateJobAssets");
                    PopulateJobAssets(jobRequest, request, job);

                    _log.Info("Start PopulateJobTasks");
                    PopulateJobTasks(jobRequest, request, job, contract);

                    _log.Info("Start PrePopulateBasedOnJobAttributes");
                    PrePopulateBasedOnJobAttributes(jobRequest, job);

                    _log.Info("Start PrePopulateBasedOnAssetAttributes");
                    PrePopulateBasedOnAssetAttributes(jobRequest, job);

                    _log.Info("Start Pre-Populate Non Asset Based Task Pre-Population");
                    PrePopulateNonAssetBased(jobRequest, job);

                    _log.Info("Start PrePopulateBasedOnXReference");
                    PrePopulateBasedOnXReference(job, contract);

                    CreateAssetLevelNotes(jobRequest, job);

                    CreateNonAssetBasedTaskNotes(jobRequest, job);


                    _db.jobJobs.Add(job);

                    apiRequestDetail.ApiRequestDetailResult.Add(
                        new ApiRequestDetailResult
                        {
                            ResultID = job.JobID,
                            ResultDescription = job.JobNo
                        });

                    if (numberOfJobsToCreate > 1)
                    {
                        counter++;
                        if (counter == 10)
                        {
                            await _db.SaveChangesAsync().ConfigureAwait(false);
                            counter = 0;
                        }
                    }
                }

                apiRequestDetail.HasError = false;
                apiRequestDetail.ErrorDescription = "";
            }
            catch (Exception e)
            {
                apiRequestDetail.HasError = true;
                apiRequestDetail.ErrorDescription = e.ToString();
                LogException(e);
            }

            return jobCounter;
        }


        private void PrePopulateBasedOnXReference(JobJob job, CfgContract contract)
        {
            if (contract.cfgContractAttributes.FirstOrDefault(a => a.Attribute == "Pre-Populate From Assets" && a.AttributeValue == "Yes" && a.wadtDeleted == false) != null)
            {
                var xRefList = contract.cfgXreferences.Where(a => a.ConXFiled == "Populate ITEM From Asset Attrib" || a.ConXFiled == "Populate ITEM From Asset Name").ToList();

                foreach (JobTask jt in job.JobTasks.Where(a => a.JobAssetID.HasValue && a.wadtDeleted == false && a.ParentJobTaskID == Guid.Empty).ToList())
                {
                    PrePopulateTaskFromAsset(jt, jt.JobAssetID.Value, xRefList, job);

                    foreach (var childJT in jt.SubTasks.Where(x => x.wadtDeleted == false).ToList())
                    {
                        if (childJT.JobAssetID == null)
                            PrePopulateTaskFromAsset(childJT, jt.JobAssetID.Value, xRefList, job);
                        else
                            PrePopulateTaskFromAsset(childJT, childJT.JobAssetID.Value, xRefList, job);
                    }
                }
            }
        }

        private void PrePopulateTaskFromAsset(JobTask jt, Guid jobAssetID, List<ContractXReference> xRefList, JobJob newJob)
        {
            foreach (var item in jt.JobTaskItems.ToList())
            {
                string TaskNameTaskItemName = jt.cfgWorkFlowTask.TaskName.Trim() + "." + item.TaskItemName.Trim();

                ContractXReference refData = xRefList.FirstOrDefault(x => x.OrgValue == TaskNameTaskItemName);

                if (refData != null)
                {
                    var ja = newJob.JobAssets.FirstOrDefault(asset => asset.JobAssetID == jobAssetID);
                    if (ja != null)
                    {
                        if (refData.ConXFiled == "Populate ITEM From Asset Name")
                        {
                            item.Result = ja.AssetName;
                        }

                        if (refData.ConXFiled == "Populate ITEM From Asset Attrib")
                        {
                            var jAttrib = ja.jobAssetAttribs.FirstOrDefault(jatrib => jatrib.Attribute == refData.ConXValue);
                            if (jAttrib != null)
                            {
                                item.Result = jAttrib.AttributeValue;
                            }
                        }
                    }
                }
            }
        }

        private static void PrePopulateNonAssetBased(JobRequest jobRequest, JobJob job)
        {
            if (jobRequest.NonAssetBasedTaskPrePopulations != null)
            {
                foreach (var nonAssetBasedTaskPrePopulation in jobRequest.NonAssetBasedTaskPrePopulations.ToList())
                {

                    var task = job.JobTasks.FirstOrDefault(a => a.cfgWorkFlowTask.TaskName.Trim().ToUpper() == nonAssetBasedTaskPrePopulation.TaskName.Trim().ToUpper());
                    if (task != null)
                    {
                        foreach (var taskItem in nonAssetBasedTaskPrePopulation.JobTaskItems.ToList())
                        {
                            if (!string.IsNullOrWhiteSpace(taskItem.TaskItemName))
                            {
                                var jTaskItem = task.JobTaskItems.FirstOrDefault(a => a.TaskItemName.Trim().ToUpper() == taskItem.TaskItemName.Trim().ToUpper());
                                if (jTaskItem != null)
                                    jTaskItem.Result = taskItem.TaskItemValue;
                            }

                            if (taskItem.TaskItemOrder > 0)
                            {
                                var jTaskItemByOrder = task.JobTaskItems.FirstOrDefault(a => a.ItemOrder.Value == short.Parse(taskItem.TaskItemOrder.ToString()));
                                if (jTaskItemByOrder != null)
                                    jTaskItemByOrder.Result = taskItem.TaskItemValue;
                            }
                        }
                    }
                }
            }
        }


        private void CreateNonAssetBasedTaskNotes(JobRequest jobRequest, JobJob job)
        {
            if (jobRequest.JobTaskNotes == null) return;

            try
            {
                foreach (var taskNotes in jobRequest.JobTaskNotes.Where(a => string.IsNullOrWhiteSpace(a.ParentTaskName)).ToList())
                {

                    var task = job.JobTasks.FirstOrDefault(a => a.cfgWorkFlowTask.TaskName.Trim().ToUpper() == taskNotes.TaskName.Trim().ToUpper());
                    var n = new List<Note>();
                    n.Add(new Note { NoteMessage = taskNotes.NoteMessage, NoteType = taskNotes.NoteType });
                    CreateTaskNotes(job, task.JobTaskID, n);
                }


                foreach (var taskNotes in jobRequest.JobTaskNotes.Where(a => !string.IsNullOrWhiteSpace(a.ParentTaskName)).ToList())
                {
                    var pTask = job.JobTasks.FirstOrDefault(a => a.cfgWorkFlowTask.TaskName.Trim().ToUpper() == taskNotes.ParentTaskName.Trim().ToUpper());
                    var task = pTask.SubTasks.FirstOrDefault(a => a.cfgWorkFlowTask.TaskName.Trim().ToUpper() == taskNotes.TaskName.Trim().ToUpper());
                    var n = new List<Note>();
                    n.Add(new Note { NoteMessage = taskNotes.NoteMessage, NoteType = taskNotes.NoteType });
                    CreateTaskNotes(job, task.JobTaskID, n);
                }
            }
            catch (Exception ex)
            {
                _log.Info("Problem Creating Non Asset Based Task Notes");
                LogException(ex);
            }
        }

        private static void PrePopulateBasedOnAssetAttributes(JobRequest jobRequest, JobJob job)
        {
            foreach (var parentAsset in jobRequest.JobAssets.ToList())
            {
                var jobAsset = job.JobAssets.FirstOrDefault(a => a.enumAssetType1.enumDesc == parentAsset.AssetType && a.AssetName == parentAsset.AssetValueOrSerialNo);

                //pre populate based on TaskItemName
                foreach (var parentAssetAttribs in parentAsset.AssetAttributes.Where(a => a.TaskItemNameToPrePopulate != null).ToList())
                {
                    foreach (var task in job.JobTasks.Where(a => a.JobAssetID != null && a.JobAssetID.Value == jobAsset.JobAssetID).ToList())
                    {
                        var jobtaskItem = task.JobTaskItems.FirstOrDefault(a => a.TaskItemName == parentAssetAttribs.TaskItemNameToPrePopulate);
                        if (jobtaskItem != null)
                            jobtaskItem.Result = parentAssetAttribs.AttributeValue;
                    }
                }

                //pre populate based on TaskItemOrder (for compatibility with Excel Imports)
                foreach (var parentAssetAttribs in parentAsset.AssetAttributes.Where(a => a.TaskItemOrderToPrePopulate != null).ToList())
                {
                    foreach (var task in job.JobTasks.Where(a => a.JobAssetID != null && a.JobAssetID.Value == jobAsset.JobAssetID).ToList())
                    {
                        var jobtaskItem = task.JobTaskItems.FirstOrDefault(a => a.ItemOrder == parentAssetAttribs.TaskItemOrderToPrePopulate.Value);
                        if (jobtaskItem != null)
                            jobtaskItem.Result = parentAssetAttribs.AttributeValue;
                    }
                }

                //pre populate from JobTaskItemPrePopulations based on task Item Name
                if (parentAsset.JobTaskItemPrePopulations != null)
                {
                    foreach (var assetJobTaskItem in parentAsset.JobTaskItemPrePopulations.Where(a => a.TaskItemName != null).ToList())
                    {
                        foreach (var task in job.JobTasks.Where(a => a.JobAssetID != null && a.JobAssetID.Value == jobAsset.JobAssetID).ToList())
                        {
                            var jobtaskItem = task.JobTaskItems.FirstOrDefault(a => a.TaskItemName.Trim().ToUpper() == assetJobTaskItem.TaskItemName.Trim().ToUpper());
                            if (jobtaskItem != null)
                                jobtaskItem.Result = assetJobTaskItem.TaskItemValue;
                        }
                    }

                    //pre populate from JobTaskItemPrePopulations based on task Item Order
                    foreach (var assetJobTaskItem in parentAsset.JobTaskItemPrePopulations.Where(a => a.TaskItemOrder > 0).ToList())
                    {
                        foreach (var task in job.JobTasks.Where(a => a.JobAssetID != null && a.JobAssetID.Value == jobAsset.JobAssetID).ToList())
                        {
                            var jobtaskItem = task.JobTaskItems.FirstOrDefault(a => a.ItemOrder == assetJobTaskItem.TaskItemOrder);
                            if (jobtaskItem != null)
                                jobtaskItem.Result = assetJobTaskItem.TaskItemValue;
                        }
                    }
                }

                if (parentAsset.SubAssets != null && parentAsset.SubAssets.Count > 0)
                {
                    foreach (var subAsset in parentAsset.SubAssets.Where(a => a.AssetAttributes.Count(b => b.TaskItemNameToPrePopulate != null) > 0).ToList())
                    {
                        var jobSubAsset = job.JobAssets.FirstOrDefault(
                                a => a.enumAssetType1.enumDesc == subAsset.AssetType
                                && a.AssetName == subAsset.AssetValueOrSerialNo
                                && a.jobAssetAttribs.Count(b => b.Attribute == "Parent Asset" && b.AttributeValue == parentAsset.AssetValueOrSerialNo) > 0
                                );

                        if (jobSubAsset != null)
                        {
                            foreach (var childAssetAttribs in subAsset.AssetAttributes.Where(a => a.TaskItemNameToPrePopulate != null).ToList())
                            {
                                foreach (var task in job.JobTasks.Where(a => a.JobAssetID != null && a.JobAssetID.Value == jobAsset.JobAssetID).ToList())
                                {
                                    foreach (var subTask in task.SubTasks.Where(a => a.JobAssetID == jobSubAsset.JobAssetID))
                                    {
                                        var jobtaskItem = subTask.JobTaskItems.FirstOrDefault(a => a.TaskItemName == childAssetAttribs.TaskItemNameToPrePopulate);
                                        if (jobtaskItem != null)
                                            jobtaskItem.Result = childAssetAttribs.AttributeValue;
                                    }
                                }
                            }
                        }
                    }

                    foreach (var subAsset in parentAsset.SubAssets.Where(a => a.AssetAttributes.Count(b => b.TaskItemOrderToPrePopulate != null) > 0).ToList())
                    {
                        var jobSubAsset = job.JobAssets.FirstOrDefault(
                                a => a.enumAssetType1.enumDesc == subAsset.AssetType
                                && a.AssetName == subAsset.AssetValueOrSerialNo
                                && a.jobAssetAttribs.Count(b => b.Attribute == "Parent Asset" && b.AttributeValue == parentAsset.AssetValueOrSerialNo) > 0
                                );

                        if (jobSubAsset != null)
                        {
                            foreach (var childAssetAttribs in subAsset.AssetAttributes.Where(a => a.TaskItemOrderToPrePopulate != null).ToList())
                            {
                                foreach (var task in job.JobTasks.Where(a => a.JobAssetID != null && a.JobAssetID.Value == jobAsset.JobAssetID).ToList())
                                {
                                    foreach (var subTask in task.SubTasks.Where(a => a.JobAssetID == jobSubAsset.JobAssetID))
                                    {
                                        var jobtaskItem = subTask.JobTaskItems.FirstOrDefault(a => a.ItemOrder == childAssetAttribs.TaskItemOrderToPrePopulate.Value);
                                        if (jobtaskItem != null)
                                            jobtaskItem.Result = childAssetAttribs.AttributeValue;
                                    }
                                }
                            }
                        }

                    }

                    foreach (var subAsset in parentAsset.SubAssets.Where(a => a.JobTaskItemPrePopulations.Count() > 0).ToList())
                    {
                        var jobSubAsset = job.JobAssets.FirstOrDefault(
                                a => a.enumAssetType1.enumDesc == subAsset.AssetType
                                && a.AssetName == subAsset.AssetValueOrSerialNo
                                && a.jobAssetAttribs.Count(b => b.Attribute == "Parent Asset" && b.AttributeValue == parentAsset.AssetValueOrSerialNo) > 0
                                );

                        if (jobSubAsset != null)
                        {
                            //pre populate from JobTaskItemPrePopulations based on task Item Name
                            foreach (var assetJobTaskItem in subAsset.JobTaskItemPrePopulations.Where(a => a.TaskItemName != null).ToList())
                            {
                                foreach (var task in job.JobTasks.Where(a => a.JobAssetID != null && a.JobAssetID.Value == jobAsset.JobAssetID).ToList())
                                {
                                    foreach (var subTask in task.SubTasks.Where(a => a.JobAssetID == jobSubAsset.JobAssetID))
                                    {
                                        var jobtaskItem = subTask.JobTaskItems.FirstOrDefault(a => a.TaskItemName.Trim().ToUpper() == assetJobTaskItem.TaskItemName.Trim().ToUpper());
                                        if (jobtaskItem != null)
                                            jobtaskItem.Result = assetJobTaskItem.TaskItemValue;
                                    }
                                }
                            }

                            //pre populate from JobTaskItemPrePopulations based on task Item Order
                            foreach (var assetJobTaskItem in subAsset.JobTaskItemPrePopulations.Where(a => a.TaskItemOrder > 0).ToList())
                            {
                                foreach (var task in job.JobTasks.Where(a => a.JobAssetID != null && a.JobAssetID.Value == jobAsset.JobAssetID).ToList())
                                {
                                    foreach (var subTask in task.SubTasks.Where(a => a.JobAssetID == jobSubAsset.JobAssetID))
                                    {
                                        var jobtaskItem = subTask.JobTaskItems.FirstOrDefault(a => a.ItemOrder == assetJobTaskItem.TaskItemOrder);
                                        if (jobtaskItem != null)
                                            jobtaskItem.Result = assetJobTaskItem.TaskItemValue;
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }


        private void CreateTaskNotes(JobJob job, Guid jobTaskID, List<Note> notes)
        {
            EnumNoteType noteType = null;
            foreach (var note in notes.Where(a => a.NoteMessage.Trim().Length > 0).ToList())
            {
                noteType = GetEnumNoteType(note.NoteType);
                job.JobNotes.Add(
                    new JobNote
                    {
                        enumNoteType = noteType.enumNoteType,
                        Note = note.NoteMessage,
                        enumObjType = 4f,
                        JobID = job.JobID,
                        JobNoteID = GetNewID(),
                        NoteBy = job.wadtModifiedBy,
                        NoteOn = job.wadtModifiedOn,
                        NoteOnOffset = job.wadtModifiedOn.Value.ToString("zzzz"),
                        ObjID = jobTaskID,
                        rowguid = GetNewID(),
                        wadtDeleted = false,
                        wadtModifiedBy = job.wadtModifiedBy,
                        wadtModifiedOn = job.wadtModifiedOn,
                        wadtModifiedOnDTOffset = job.wadtModifiedOn.Value.ToString("zzzz"),
                        wadtRowID = GetNewID()
                    });
            }
        }

        private void CreateAssetLevelNotes(JobRequest jobRequest, JobJob job)
        {
            foreach (var parentAsset in jobRequest.JobAssets.ToList())
            {
                var jobAsset = job.JobAssets.FirstOrDefault(a => a.enumAssetType1.enumDesc == parentAsset.AssetType && a.AssetName == parentAsset.AssetValueOrSerialNo);
                if (jobAsset != null)
                {
                    if (parentAsset.Notes != null)
                    {
                        if (parentAsset.Notes.Count > 0)
                        {
                            foreach (var task in job.JobTasks.Where(a => a.JobAssetID != null && a.JobAssetID.Value == jobAsset.JobAssetID).ToList())
                            {
                                CreateTaskNotes(job, task.JobTaskID, parentAsset.Notes.ToList());
                            }
                        }
                    }

                    if (parentAsset.SubAssets != null)
                    {
                        foreach (var subAsset in parentAsset.SubAssets.Where(a => a.Notes.Count > 0))
                        {
                            var jobSubAsset = job.JobAssets.FirstOrDefault(
                                                         a => a.enumAssetType1.enumDesc == subAsset.AssetType
                                                         && a.AssetName == subAsset.AssetValueOrSerialNo
                                                         && a.jobAssetAttribs.Count(b => b.Attribute == "Parent Asset" && b.AttributeValue == parentAsset.AssetValueOrSerialNo) > 0
                                                         );
                            if (jobSubAsset != null)
                            {
                                foreach (var task in job.JobTasks.Where(a => a.JobAssetID != null && a.JobAssetID.Value == jobAsset.JobAssetID).ToList())
                                {
                                    foreach (var subTask in task.SubTasks.Where(a => a.JobAssetID != null && a.JobAssetID.Value == jobSubAsset.JobAssetID).ToList())
                                    {
                                        CreateTaskNotes(job, subTask.JobTaskID, subAsset.Notes.ToList());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        private static void PrePopulateBasedOnJobAttributes(JobRequest jobRequest, JobJob job)
        {
            foreach (var attrib in jobRequest.JobAttributes.Where(a => a.TaskItemNameToPrePopulate != null).ToList())
            {
                var task = job.JobTasks.FirstOrDefault(a => a.cfgWorkFlowTask.TaskName == attrib.TaskNameToPrePopulate);
                if (task != null)
                {
                    var jTaskItem = task.JobTaskItems.FirstOrDefault(a => a.TaskItemName == attrib.TaskItemNameToPrePopulate);
                    if (jTaskItem != null)
                        jTaskItem.Result = attrib.AttributeValue;
                }
            }

            foreach (var attrib in jobRequest.JobAttributes.Where(a => a.TaskItemOrderToPrePopulate != null).ToList())
            {
                var task = job.JobTasks.FirstOrDefault(a => a.cfgWorkFlowTask.TaskName == attrib.TaskNameToPrePopulate);
                if (task != null)
                {
                    var jTaskItem = task.JobTaskItems.FirstOrDefault(a => a.ItemOrder == attrib.TaskItemOrderToPrePopulate.Value);
                    if (jTaskItem != null)
                        jTaskItem.Result = attrib.AttributeValue;
                }
            }
        }

        private void PopulateJobTasks(JobRequest jobRequest, ApiRequest request, JobJob job, CfgContract contract)
        {
            List<WorkFlowTask> ParentTasks = contract.WorkFlow.WorkFlowTasks.Where(a => a.wadtDeleted == false && a.ParentTaskID == null).OrderBy(b => b.TaskOrder).ToList();
            short taskOrder = 1;
            foreach (WorkFlowTask wft in ParentTasks)
            {
                if (wft.AddByDefaultToJob == true)
                {
                    taskOrder = AddTaskToJob(jobRequest, wft, job, taskOrder);
                }
                else
                {
                    if (wft.SubWorkFlowTasks != null)
                        if (wft.SubWorkFlowTasks.Count() > 0)
                        {
                            taskOrder += short.Parse(wft.SubWorkFlowTasks.Count().ToString());
                        }
                }

                taskOrder++;
            }
        }

        private short AddTaskToJob(JobRequest jobRequest, WorkFlowTask wft, JobJob job, short taskOrder)
        {
            if (wft.isExistingAsset == false || (wft.isExistingAsset == true && wft.enumAssetType == 1f))
            {
                //Add Default Tasks that are not asset based
                JobTask parentJt = CreateJobTask(jobRequest, wft, job, null, null, taskOrder, null);

                CreateSubJobTasks(jobRequest, wft, job, parentJt, null);

                job.JobTasks.Add(parentJt);
            }
            else
            {
                //Asset based Tasks
                int sequenceOrder = 1;
                foreach (var asset in job.JobAssets.Where(a => a.enumAssetType == wft.enumAssetType.Value && a.wadtDeleted == false).OrderBy(b => b.AssetOrder).ToList())
                {
                    JobTask parentJt = CreateJobTask(jobRequest, wft, job, null, asset.JobAssetID, taskOrder, sequenceOrder);
                    CreateSubJobTasks(jobRequest, wft, job, parentJt, asset);
                    job.JobTasks.Add(parentJt);
                    sequenceOrder++;
                    taskOrder++;
                }
            }

            return taskOrder;
        }

        private void CreateSubJobTasks(JobRequest jobRequest, WorkFlowTask wft, JobJob job, JobTask parentJt, ConXApiRepository.JobAsset parentJobAsset)
        {
            short subTaskOrder = 1;
            foreach (var subTask in wft.SubWorkFlowTasks.Where(a => a.wadtDeleted == false && a.AddByDefaultToJob == true)
                .OrderBy(b => b.TaskOrder).ToList())
            {
                if (subTask.isExistingAsset == false || (subTask.isExistingAsset == true && subTask.enumAssetType == 1f))
                {
                    parentJt.SubTasks.Add(CreateJobTask(jobRequest, subTask, job, parentJt, null, subTaskOrder, null));
                }
                else
                {
                    //create sub tasks based on assets, if the asset type is the same as the Asset type of the parent WFT, only get the Asset with the same asset ID
                    List<ConXApiRepository.JobAsset> subAssets = null;
                    if (parentJt.JobAssetID != null)
                    {
                        if (parentJobAsset.enumAssetType == subTask.enumAssetType.Value)
                        {
                            subAssets = job.JobAssets.Where(a => a.enumAssetType == subTask.enumAssetType.Value && a.wadtDeleted == false && a.JobAssetID == parentJt.JobAssetID).OrderBy(b => b.AssetOrder).ToList();
                        }
                        else
                        {
                            subAssets = job.JobAssets.Where(a => a.enumAssetType == subTask.enumAssetType.Value && a.wadtDeleted == false && a.jobAssetAttribs.Count(x => x.Attribute == "Parent Asset" && x.AttributeValue == parentJobAsset.AssetName) > 0).OrderBy(b => b.AssetOrder).ToList();
                        }
                    }
                    else
                    {
                        subAssets = job.JobAssets.Where(a => a.enumAssetType == subTask.enumAssetType.Value && a.wadtDeleted == false).OrderBy(b => b.AssetOrder).ToList();
                    }

                    int sequenceOrder = 1;
                    foreach (var subAsset in subAssets)
                    {
                        parentJt.SubTasks.Add(CreateJobTask(jobRequest, subTask, job, parentJt, subAsset.JobAssetID, subTaskOrder, sequenceOrder));
                        sequenceOrder++;
                        subTaskOrder++;
                    }

                }
                subTaskOrder++;
            }
        }

        private JobTask CreateJobTask(JobRequest jobRequest, WorkFlowTask wft, JobJob job, JobTask parentJobTask, Guid? jobAssetId, short taskOrder, int? sequenceOrder)
        {
            JobTask jobTask = new JobTask
            {
                enumTaskQualifier = wft.Default_enumTaskQualifier,
                enumTaskStatus = 1f,
                enumTaskType = wft.enumWFTaskType,
                isTaskQualifierForced = wft.Default_isTaskQualifierForced,
                JobAssetID = jobAssetId,
                JobID = job.JobID,
                JobTaskID = GetNewID(),
                ParentJobTaskID = parentJobTask != null ? parentJobTask.JobTaskID : Guid.Empty,
                rowguid = GetNewID(),
                TaskDescription = sequenceOrder != null ? sequenceOrder.Value.ToString() : "",
                TaskID = wft.TaskID,
                TaskOrder = taskOrder,
                wadtDeleted = false,
                wadtModifiedBy = job.wadtModifiedBy,
                wadtModifiedOn = job.wadtModifiedOn,
                wadtModifiedOnDTOffset = job.wadtModifiedOnDTOffset,
                wadtRowID = GetNewID(),
                cfgWorkFlowTask = wft
            };

            if (jobAssetId != null)
            {
                string taskDescription = GetTaskDescriptionFromAssetNumber(job, jobAssetId.Value, jobRequest);

                if (taskDescription != "")
                    if (taskDescription == "No Task Description")
                        jobTask.TaskDescription = "";
                    else
                        jobTask.TaskDescription = taskDescription;
            }

            foreach (var workflowTaskItem in wft.WorkflowTaskItems.Where(a => a.wadtDeleted == false).OrderBy(b => b.TaskItemOrder).ToList())
            {
                jobTask.JobTaskItems.Add(CreateJobTaskItem(jobTask, workflowTaskItem));
            }

            return jobTask;
        }

        private string GetTaskDescriptionFromAssetNumber(JobJob job, Guid jobAssetId, JobRequest jobRequest)
        {
            string taskDescription = "";

            var jAsset = job.JobAssets.First(a => a.JobAssetID == jobAssetId);
            var jAssetParentAttribute = jAsset.jobAssetAttribs.First(a => a.Attribute == "Parent Asset");
            string parentAssetNo = "";

            if (jAssetParentAttribute != null)
                parentAssetNo = jAssetParentAttribute.AttributeValue;

            //check if Asset is a sub asset or a parent asset
            if (!string.IsNullOrWhiteSpace(parentAssetNo))
            {
                //Child Asset
                var sourceChildAsset = jobRequest.JobAssets.First(a => a.AssetValueOrSerialNo == parentAssetNo).SubAssets.First(b => b.AssetValueOrSerialNo == jAsset.AssetName);
                if (sourceChildAsset != null)
                {
                    if (sourceChildAsset.PopulateTaskDescriptionWithAssetValue == true)
                        taskDescription = jAsset.AssetName;
                    else
                        taskDescription = "No Task Description";
                }
            }
            else
            {
                //Parent Asset
                var sourceParentAsset = jobRequest.JobAssets.First(a => a.AssetValueOrSerialNo == jAsset.AssetName);
                if (sourceParentAsset != null)
                {
                    if (sourceParentAsset.PopulateTaskDescriptionWithAssetValue == true)
                        taskDescription = jAsset.AssetName;
                    else
                        taskDescription = "No Task Description";
                }
            }
            return taskDescription;
        }

        private ConXApiRepository.JobTaskItem CreateJobTaskItem(JobTask jt, WorkFlowTaskItem wfti)
        {
            return new ConXApiRepository.JobTaskItem
            {
                ItemOrder = wfti.TaskItemOrder,
                JobTaskID = jt.JobTaskID,
                JobTaskItemID = GetNewID(),
                rowguid = GetNewID(),
                TaskItemID = wfti.TaskItemID,
                TaskItemName = wfti.TaskItemName,
                wadtDeleted = false,
                wadtModifiedBy = jt.wadtModifiedBy,
                wadtModifiedOn = jt.wadtModifiedOn,
                wadtModifiedOnDTOffset = jt.wadtModifiedOnDTOffset,
                wadtRowID = GetNewID()
            };
        }

        private void PopulateJobAssets(JobRequest jobRequest, ApiRequest request, JobJob job)
        {
            int assetCounter = 1;
            foreach (var jAsset in jobRequest.JobAssets)
            {

                EnumAssetType enumAsstType = GetEnumAssetType(jAsset.AssetType, request.FSP);

                ConXApiRepository.JobAsset jobAsset = new ConXApiRepository.JobAsset
                {
                    AssetName = jAsset.AssetValueOrSerialNo,
                    AssetOrder = assetCounter,
                    enumAssetType = enumAsstType.enumAssetType,
                    JobAssetID = GetNewID(),
                    JobID = job.JobID,
                    rowguid = GetNewID(),
                    wadtDeleted = false,
                    wadtModifiedBy = "sys",
                    wadtModifiedOn = job.wadtModifiedOn,
                    wadtModifiedOnDTOffset = job.wadtModifiedOn.Value.ToString("zzzz"),
                    wadtRowID = GetNewID(),
                    enumAssetType1 = enumAsstType
                };
                //Create Static Assets
                jobAsset.jobAssetAttribs.Add(new ConXApiRepository.JobAssetAttribute
                {
                    aOrder = 0,
                    Attribute = "Parent Asset",
                    AttributeValue = "",
                    JobAssetAttribID = GetNewID(),
                    JobAssetID = jobAsset.JobAssetID,
                    rowguid = GetNewID(),
                    wadtDeleted = false,
                    wadtModifiedBy = "sys",
                    wadtModifiedOn = job.wadtModifiedOn,
                    wadtModifiedOnDTOffset = job.wadtModifiedOn.Value.ToString("zzzz"),
                    wadtRowID = GetNewID()
                });

                int attribCounter = 1;
                foreach (var jAssetAttribs in jAsset.AssetAttributes)
                {
                    jobAsset.jobAssetAttribs.Add(new ConXApiRepository.JobAssetAttribute
                    {
                        aOrder = attribCounter,
                        Attribute = jAssetAttribs.AttributeName,
                        AttributeValue = jAssetAttribs.AttributeValue,
                        JobAssetAttribID = GetNewID(),
                        JobAssetID = jobAsset.JobAssetID,
                        rowguid = GetNewID(),
                        wadtDeleted = false,
                        wadtModifiedBy = "sys",
                        wadtModifiedOn = job.wadtModifiedOn,
                        wadtModifiedOnDTOffset = job.wadtModifiedOn.Value.ToString("zzzz"),
                        wadtRowID = GetNewID()
                    });
                    attribCounter++;
                }

                job.JobAssets.Add(jobAsset);

                if (jAsset.SubAssets != null)
                {
                    foreach (var subAsset in jAsset.SubAssets)
                    {
                        assetCounter++;
                        EnumAssetType enumSubAsstType = GetEnumAssetType(subAsset.AssetType, request.FSP);

                        ConXApiRepository.JobAsset jobSubAsset = new ConXApiRepository.JobAsset
                        {
                            AssetName = subAsset.AssetValueOrSerialNo,
                            AssetOrder = assetCounter,
                            enumAssetType = enumSubAsstType.enumAssetType,
                            JobAssetID = GetNewID(),
                            JobID = job.JobID,
                            rowguid = GetNewID(),
                            wadtDeleted = false,
                            wadtModifiedBy = "sys",
                            wadtModifiedOn = job.wadtModifiedOn,
                            wadtModifiedOnDTOffset = job.wadtModifiedOn.Value.ToString("zzzz"),
                            wadtRowID = GetNewID(),
                            enumAssetType1 = enumSubAsstType
                        };

                        //Create Static Assets
                        jobSubAsset.jobAssetAttribs.Add(new ConXApiRepository.JobAssetAttribute
                        {
                            aOrder = 0,
                            Attribute = "Parent Asset",
                            AttributeValue = jAsset.AssetValueOrSerialNo,
                            JobAssetAttribID = GetNewID(),
                            JobAssetID = jobSubAsset.JobAssetID,
                            rowguid = GetNewID(),
                            wadtDeleted = false,
                            wadtModifiedBy = "sys",
                            wadtModifiedOn = job.wadtModifiedOn,
                            wadtModifiedOnDTOffset = job.wadtModifiedOn.Value.ToString("zzzz"),
                            wadtRowID = GetNewID()
                        });

                        int subAttribCounter = 1;
                        foreach (var jSubAssetAttribs in subAsset.AssetAttributes)
                        {
                            jobSubAsset.jobAssetAttribs.Add(new ConXApiRepository.JobAssetAttribute
                            {
                                aOrder = subAttribCounter,
                                Attribute = jSubAssetAttribs.AttributeName,
                                AttributeValue = jSubAssetAttribs.AttributeValue,
                                JobAssetAttribID = GetNewID(),
                                JobAssetID = jobSubAsset.JobAssetID,
                                rowguid = GetNewID(),
                                wadtDeleted = false,
                                wadtModifiedBy = "sys",
                                wadtModifiedOn = job.wadtModifiedOn,
                                wadtModifiedOnDTOffset = job.wadtModifiedOn.Value.ToString("zzzz"),
                                wadtRowID = GetNewID()
                            });
                            attribCounter++;
                        }

                        job.JobAssets.Add(jobSubAsset);
                    }
                }

                assetCounter++;
                //job.JobAssets.Add()
            }
        }

        private void PopulateJobScheduleAndRecurringAttributes(JobRequest jobRequest, ApiRequest request, int numberOfJobsToCreate, string JobSource, string SeriesSource, ref DateTime? PlanFrom, ref DateTime? PlanTo, int seriesCounter, JobJob job)
        {
            job.isAppointment = false;
            if (jobRequest.JobSchedule != null)
            {
                if (seriesCounter == 1)
                {
                    PlanFrom = jobRequest.JobSchedule.PlannedFrom;
                    PlanTo = jobRequest.JobSchedule.PlannedTo;
                }
                else
                {
                    PlanFrom = PlanFrom.Value.AddDays(jobRequest.JobSchedule.Frequency.Value);
                    if (PlanTo != null)
                        PlanTo = PlanTo.Value.AddDays(jobRequest.JobSchedule.Frequency.Value);
                }

                job.ScheduledForDate = PlanFrom;
                job.ScheduledForDate2 = PlanTo;

                if (!string.IsNullOrWhiteSpace(jobRequest.JobSchedule.ScheduleType))
                    job.enumDayPart = GetEnumDayPart(jobRequest.JobSchedule.ScheduleType).enumDayPart;

                if (!string.IsNullOrWhiteSpace(jobRequest.JobSchedule.TechUserName))
                    job.ScheduledForTech = jobRequest.JobSchedule.TechUserName;


                if (!string.IsNullOrWhiteSpace(jobRequest.JobSchedule.TechUserName) && job.ScheduledForDate != null)
                {
                    job.enumJobStatus = 3f; //Scheduled 
                }


                if (jobRequest.JobSchedule.AssignToDevice.HasValue)
                {
                    if (jobRequest.JobSchedule.AssignToDevice.Value)
                    {
                        if (!string.IsNullOrWhiteSpace(jobRequest.JobSchedule.TechUserName) && job.ScheduledForDate != null)
                        {
                            job.enumJobStatus = 4f; //assigned 
                        }
                    }
                }

                job.isAppointment = jobRequest.JobSchedule.RequiresAppointment.HasValue ? false : jobRequest.JobSchedule.RequiresAppointment;

                if (numberOfJobsToCreate > 1)
                {
                    job.JobJobAttributes.Add(new JobJobAttribute
                    {
                        aOrder = 1,
                        JobAttribID = GetNewID(),
                        JobID = job.JobID,
                        Attribute = "Series Seq",
                        AttributeValue = seriesCounter.ToString() + " of " + numberOfJobsToCreate.ToString(),
                        wadtDeleted = false,
                        wadtModifiedBy = "sys",
                        wadtModifiedOn = job.wadtModifiedOn,
                        wadtModifiedOnDTOffset = job.wadtModifiedOn.Value.ToString("zzzz"),
                        wadtRowID = GetNewID(),
                        rowguid = GetNewID()
                    });

                    job.JobJobAttributes.Add(new JobJobAttribute
                    {
                        aOrder = 1,
                        JobAttribID = GetNewID(),
                        JobID = job.JobID,
                        Attribute = "Series ID",
                        AttributeValue = SeriesSource,
                        wadtDeleted = false,
                        wadtModifiedBy = "sys",
                        wadtModifiedOn = job.wadtModifiedOn,
                        wadtModifiedOnDTOffset = job.wadtModifiedOn.Value.ToString("zzzz"),
                        wadtRowID = GetNewID(),
                        rowguid = GetNewID()
                    });
                }

                if (jobRequest.JobSchedule.NumberOfOccurrences.HasValue)
                {
                    job.JobJobAttributes.Add(new JobJobAttribute
                    {
                        aOrder = 1,
                        JobAttribID = GetNewID(),
                        JobID = job.JobID,
                        Attribute = "No of Occurrences",
                        AttributeValue = numberOfJobsToCreate.ToString(),
                        wadtDeleted = false,
                        wadtModifiedBy = "sys",
                        wadtModifiedOn = job.wadtModifiedOn,
                        wadtModifiedOnDTOffset = job.wadtModifiedOn.Value.ToString("zzzz"),
                        wadtRowID = GetNewID(),
                        rowguid = GetNewID()
                    });

                    if (jobRequest.JobSchedule.Frequency.HasValue)
                    {
                        JobJobAttribute jaFreq = job.JobJobAttributes.FirstOrDefault(a => a.Attribute == "Frequency");
                        if (jaFreq != null)
                            job.JobJobAttributes.Remove(jaFreq);

                        job.JobJobAttributes.Add(new JobJobAttribute
                        {
                            aOrder = 1,
                            JobAttribID = GetNewID(),
                            JobID = job.JobID,
                            Attribute = "Frequency",
                            AttributeValue = jobRequest.JobSchedule.Frequency.Value.ToString(),
                            wadtDeleted = false,
                            wadtModifiedBy = "sys",
                            wadtModifiedOn = job.wadtModifiedOn,
                            wadtModifiedOnDTOffset = job.wadtModifiedOn.Value.ToString("zzzz"),
                            wadtRowID = GetNewID(),
                            rowguid = GetNewID()
                        });
                    }

                    JobJobAttribute jaInsSched = job.JobJobAttributes.FirstOrDefault(a => a.Attribute == "Initial Scheduled Date");
                    if (jaInsSched != null)
                        job.JobJobAttributes.Remove(jaInsSched);

                    job.JobJobAttributes.Add(new JobJobAttribute
                    {
                        aOrder = 1,
                        JobAttribID = GetNewID(),
                        JobID = job.JobID,
                        Attribute = "Initial Scheduled Date",
                        AttributeValue = PlanFrom.Value.TimeOfDay.TotalSeconds == 0 ? PlanFrom.Value.ToString("yyyy-MM-dd") : PlanFrom.Value.ToString("yyyy-MM-dd HH:mm:ss"),
                        wadtDeleted = false,
                        wadtModifiedBy = "sys",
                        wadtModifiedOn = job.wadtModifiedOn,
                        wadtModifiedOnDTOffset = job.wadtModifiedOn.Value.ToString("zzzz"),
                        wadtRowID = GetNewID(),
                        rowguid = GetNewID()
                    });


                }


                if (seriesCounter != 1)
                {
                    job.JobJobAttributes.Add(new JobJobAttribute
                    {
                        aOrder = 1,
                        JobAttribID = GetNewID(),
                        JobID = job.JobID,
                        Attribute = "Source Job",
                        AttributeValue = JobSource,
                        wadtDeleted = false,
                        wadtModifiedBy = "sys",
                        wadtModifiedOn = job.wadtModifiedOn,
                        wadtModifiedOnDTOffset = job.wadtModifiedOn.Value.ToString("zzzz"),
                        wadtRowID = GetNewID(),
                        rowguid = GetNewID()
                    });
                }

            }
        }

        private void PopulateJobAddress(JobRequest jobRequest, ApiRequest request, JobJob job)
        {

            foreach (var address in jobRequest.JobAddresses)
            {
                float countryID = 1f;
                if (!string.IsNullOrWhiteSpace(address.Country))
                {
                    var country = _enumCountries.FirstOrDefault(a => a.enumDesc.Trim().ToUpper() == address.Country.Trim().ToUpper());
                    if (country != null)
                        countryID = country.enumCountry;
                }

                job.JobAddresses.Add(
                    new ConXApiRepository.JobAddress
                    {
                        Apartment = address.FlatAppartment,
                        City = address.City,
                        Description = address.AddressDescription,
                        enumAddressSource = 1f,
                        enumAddressType = 1f,
                        enumCountry = countryID,
                        JobAddressID = GetNewID(),
                        JobID = job.JobID,
                        Latitude = address.Latitude.HasValue ? address.Latitude.Value.ToString() : null,
                        Longitude = address.Longitude.HasValue ? address.Longitude.Value.ToString() : null,
                        PostCode = address.PostCode,
                        PrimaryAddress = address.PrimaryAddress,
                        rowguid = GetNewID(),
                        State = address.Region,
                        StreetName = address.StreetName,
                        StreetNo = address.StreetNumber,
                        Suburb = address.Suburb,
                        wadtDeleted = false,
                        wadtModifiedBy = "sys",
                        wadtModifiedOn = job.wadtModifiedOn,
                        wadtModifiedOnDTOffset = job.wadtModifiedOn.Value.ToString("zzzz"),
                        wadtRowID = GetNewID()
                    });
            }
        }

        private void PopulateJobNotes(JobRequest jobRequest, ApiRequest request, JobJob job)
        {
            EnumNoteType noteType = null;
            foreach (var note in jobRequest.JobNotes.Where(a => a.NoteMessage.Trim().Length > 0).ToList())
            {
                noteType = GetEnumNoteType(note.NoteType);
                job.JobNotes.Add(
                    new JobNote
                    {
                        enumNoteType = noteType.enumNoteType,
                        Note = note.NoteMessage,
                        enumObjType = 1f,
                        JobID = job.JobID,
                        JobNoteID = GetNewID(),
                        NoteBy = "sys",
                        NoteOn = job.wadtModifiedOn,
                        NoteOnOffset = job.wadtModifiedOn.Value.ToString("zzzz"),
                        ObjID = job.JobID,
                        rowguid = GetNewID(),
                        wadtDeleted = false,
                        wadtModifiedBy = "sys",
                        wadtModifiedOn = job.wadtModifiedOn,
                        wadtModifiedOnDTOffset = job.wadtModifiedOn.Value.ToString("zzzz"),
                        wadtRowID = GetNewID()
                    });
            }
        }

        private void PopulateJobContacts(JobRequest jobRequest, ApiRequest request, JobJob job)
        {
            foreach (var contact in jobRequest.JobContacts)
            {
                ConXApiRepository.JobContact c = new ConXApiRepository.JobContact
                {
                    JobID = job.JobID,
                    JobContactID = GetNewID(),
                    FirstName = string.IsNullOrWhiteSpace(contact.FirstName) ? "" : contact.FirstName.Length <= 50 ? contact.FirstName : contact.FirstName.Substring(0, 50),
                    MidName = string.IsNullOrWhiteSpace(contact.MiddleName) ? "" : contact.MiddleName,
                    LastName = string.IsNullOrWhiteSpace(contact.LastName) ? "" : contact.LastName,
                    Description = !string.IsNullOrWhiteSpace(contact.ContactDescription) ? contact.ContactDescription :
                    (
                    ((string.IsNullOrWhiteSpace(contact.FirstName) ? "" : contact.FirstName) + " " +
                    (string.IsNullOrWhiteSpace(contact.MiddleName) ? "" : contact.MiddleName) + " " +
                    (string.IsNullOrWhiteSpace(contact.LastName) ? "" : contact.LastName)).Trim()
                    ),
                    enumContactSource = 1f,
                    rowguid = GetNewID(),
                    wadtDeleted = false,
                    wadtModifiedBy = "sys",
                    wadtModifiedOn = job.wadtModifiedOn,
                    wadtModifiedOnDTOffset = job.wadtModifiedOn.Value.ToString("zzzz"),
                    wadtRowID = GetNewID()
                };

                foreach (var detail in contact.JobContactDetails)
                {
                    EnumPhoneType enumPhone = GetEnumPhoneType(detail.ContactType);
                    ConXApiRepository.jobContactPhoneDetail jobContactPhoneDetail = new ConXApiRepository.jobContactPhoneDetail();
                    jobContactPhoneDetail.JobContactID = c.JobContactID;
                    jobContactPhoneDetail.enumPhoneType = enumPhone.enumPhoneType;
                    jobContactPhoneDetail.JobContactPhoneID = GetNewID();
                    jobContactPhoneDetail.Phone = detail.ContactValue;
                    jobContactPhoneDetail.PreferedContact = detail.PreferredContact;
                    jobContactPhoneDetail.wadtDeleted = false;
                    jobContactPhoneDetail.wadtModifiedBy = "sys";
                    jobContactPhoneDetail.wadtModifiedOn = job.wadtModifiedOn;
                    jobContactPhoneDetail.wadtModifiedOnDTOffset = job.wadtModifiedOn.Value.ToString("zzzz");
                    jobContactPhoneDetail.wadtRowID = GetNewID();
                    c.JobContactPhoneDetails.Add(jobContactPhoneDetail);
                }

                job.JobContacts.Add(c);
            }
        }

        private void PopulateJobAttributes(JobRequest jobRequest, ApiRequest request, JobJob job)
        {
            int counter = 1;
            foreach (var attrib in jobRequest.JobAttributes)
            {
                job.JobJobAttributes.Add(
                    new JobJobAttribute
                    {
                        aOrder = counter,
                        JobAttribID = GetNewID(),
                        JobID = job.JobID,
                        Attribute = attrib.AttributeName,
                        AttributeValue = attrib.AttributeValue,
                        wadtDeleted = false,
                        wadtModifiedBy = "sys",
                        wadtModifiedOn = job.wadtModifiedOn,
                        wadtModifiedOnDTOffset = job.wadtModifiedOn.Value.ToString("zzzz"),
                        wadtRowID = GetNewID(),
                        rowguid = GetNewID()

                    });
                counter++;
            }
        }

        private void PopulateJobLevelProperties(JobRequest jobRequest, ApiRequest request, JobJob job, CfgContract contract)
        {

            job.JobID = GetNewID();
            job.JobNo = GetNewJobNo(request.FSP);
            job.FSP = request.FSP;
            job.wadtModifiedBy = "sys";
            job.wadtModifiedOn = DateTime.Now;
            job.StatusBy = "sys";
            job.StatusOn = DateTime.Now;
            job.COReferenceNo = jobRequest.JobReference;
            job.Description = jobRequest.Description;
            job.ContractID = contract.ContractID;
            job.WorkFlowID = contract.WorkFlowID.Value;
            job.OrgID = contract.cimContact.cimOrganisation.OrgID;
            job.enumFieldJobStatus = 6.0f;
            job.enumJobQualifier = null;
            job.enumJobSource = 2.4f;
            job.enumJobStatus = 1.0f;
            job.enumJobType = GetEnumJobType(jobRequest.JobType, request.FSP).enumJobType;
            job.enumRegions = GetEnumRegion(jobRequest.Region, request.FSP).enumRegions;
            job.FieldOutcome = "";
            job.FieldOutcomeReason = "";

            if (jobRequest.JobSequence.HasValue)
            {
                job.jOrder = jobRequest.JobSequence.Value;
            }
            else
            {
                job.jOrder = 0;
            }

            job.Group1 = jobRequest.Group1;
            job.Group2 = jobRequest.Group2;
            job.Group3 = jobRequest.Group3;
            job.wadtRowID = GetNewID();
            job.rowguid = GetNewID();
        }

        private Guid GetNewID()
        {
            return GenerateSequentialGuid.Default.GetNewSequentialGuid();
        }

        private string GetNewJobNo(string FSP) // add retry here...
        {
            List<JobNos> resultsData = _db.Database.SqlQuery<JobNos>(
               "Exec GetNewJobNo @fsp",
               new SqlParameter("@fsp", FSP)
           ).ToList();

            return resultsData.First().JobNo;
        }

        #region Validation Methods

        private bool ValidateStringValue(string value, string field, string reference, StringBuilder sb)
        {
            bool hasError = false;

            if (string.IsNullOrWhiteSpace(value))
            {
                sb.AppendLine(field + " for " + reference + " cannot be NULL or empty.");
                hasError = true;
            }

            return hasError;
        }

        private bool ValidateNumericValue()
        {
            bool hasError = false;

            return hasError;
        }

        private bool ValidateJobSchedulingData(JobSchedule jobSchedulingData, StringBuilder sb)
        {

            DateTime dt = DateTime.Now;
            bool hasError = false;
            string tabObject = "Job Scheduling";

            if (jobSchedulingData.PlannedFrom == null)
            {
                sb.AppendLine("\n" + tabObject + " value for Planned From Date cannot be NULL or empty.");
                hasError = true;
            }

            if (jobSchedulingData.Frequency < 0)
            {
                sb.AppendLine("\n" + tabObject + " value for Frequency cannot be negative.");
                hasError = true;
            }

            if (string.IsNullOrWhiteSpace(jobSchedulingData.TechUserName))
            {
                sb.AppendLine("\n" + tabObject + " value for Scheduled for Tech id cannot be NULL or empty.");
                hasError = true;
            }

            _log.Info("Finishing ValidateJobSchedulingData ...");

            return hasError;
        }


        private bool ValidateJobAddressesData(IList<ConXApiDtoModels.DataCreationRequest.JobAddress> jobAddresses, StringBuilder sb)
        {
            bool hasError = false;
            string tabObject = "Job Addresses";

            foreach (var address in jobAddresses.ToList())
            {
                if (string.IsNullOrWhiteSpace(address.Region))
                {
                    sb.AppendLine("\n" + tabObject + " value for Region cannot be NULL or empty.");
                    hasError = true;
                }

                if (string.IsNullOrWhiteSpace(address.City))
                {
                    sb.AppendLine("\n" + tabObject + " value for City cannot be NULL or empty.");
                    hasError = true;
                }

                if (string.IsNullOrWhiteSpace(address.StreetName))
                {
                    sb.AppendLine("\n" + tabObject + " value for Street Name cannot be NULL or empty.");
                    hasError = true;
                }
            }

            return hasError;
        }

        private bool ValidateJobContactsData(IList<ConXApiDtoModels.DataCreationRequest.JobContact> jobContats, StringBuilder sb)
        {
            bool hasError = false;
            string tabObject = "Job Contacts";

            foreach (var contact in jobContats.ToList())
            {
                if (string.IsNullOrWhiteSpace(contact.FirstName))
                {
                    sb.AppendLine("\n" + tabObject + " value for First Name cannot be NULL or empty.");
                    hasError = true;
                }

                if (contact.JobContactDetails == null)
                {
                    sb.AppendLine("\n" + tabObject + " value for Contact Details cannot be NULL.");

                    // Iterate through the object contact.JobContactDetails
                    // and make further validations
                    hasError = true;
                }

            }

            return hasError;
        }

        private bool ValidateJobAssetsData(IList<ConXApiDtoModels.DataCreationRequest.JobAsset> jobAssets, StringBuilder sb)
        {
            bool hasError = false;
            string tabObject = "Job Assets";

            foreach (var asset in jobAssets.ToList())
            {
                if (asset.AssetAttributes == null)
                {
                    sb.AppendLine("\n" + tabObject + " value for Asset Attributes cannot be NULL.");

                    // Iterate through the object asset.AssetAttributes
                    // and make further validations

                    hasError = true;
                }
            }

            return hasError;
        }

        private bool ValidateJobAttributeData(IList<ConXApiDtoModels.DataCreationRequest.JobAttribute> jobAttributes, StringBuilder sb)
        {
            bool hasError = false;
            string tabObject = "Job Attribute";

            foreach (var attribute in jobAttributes.ToList())
            {
                if (string.IsNullOrWhiteSpace(attribute.AttributeName))
                {
                    sb.AppendLine("\n" + tabObject + " value for Attribute Name cannot be NULL or empty.");
                    hasError = true;
                }

                if (string.IsNullOrWhiteSpace(attribute.AttributeValue))
                {
                    sb.AppendLine("\n" + tabObject + " value for Attribute Value cannot be NULL or empty.");
                    hasError = true;
                }

            }

            return hasError;
        }

        private bool ValidateNonAssetBasedTaskPrePopulationData(IList<ConXApiDtoModels.DataCreationRequest.NonAssetBasedTaskPrePopulation> nonAssetBasedTaskPrePopulations, StringBuilder sb)
        {
            bool hasError = false;
            string tabObject = "NonAssetBasedTaskPrePopulation";

            foreach (var notAssetBasedData in nonAssetBasedTaskPrePopulations.ToList())
            {
                if (notAssetBasedData.JobTaskItems == null)
                {
                    sb.AppendLine("\n" + tabObject + " value for Asset Attributes cannot be NULL.");
                    hasError = true;
                }
            }

            return hasError;
        }

        private bool ValidateJNotesData(IList<ConXApiDtoModels.DataCreationRequest.Note> notes, StringBuilder sb)
        {
            bool hasError = false;
            string tabObject = "Notes";

            foreach (var note in notes.ToList())
            {
                if (string.IsNullOrWhiteSpace(note.NoteType))
                {
                    sb.AppendLine("\n" + tabObject + " value for Note Type cannot be NULL or empty.");
                    hasError = true;
                }

                if (string.IsNullOrWhiteSpace(note.NoteMessage))
                {
                    sb.AppendLine("\n" + tabObject + " value for Note Message cannot be NULL or empty.");
                    hasError = true;
                }

            }

            return hasError;
        }


        #endregion

        #region Private methods accessing list

        private CfgContract GetContract(string contractName, string FSP)
        {

            CfgContract contract = _cfgContracts.FirstOrDefault(a => a.ContractName.Trim().ToUpper() == contractName.Trim().ToUpper() && a.FSP.Trim().ToUpper() == FSP.Trim().ToUpper() && a.wadtDeleted == false);

            if (contract == null)
            {

                contract = _db.cfgContracts
                .Include(workflow => workflow.WorkFlow.WorkFlowTasks.Select(workflowTask => workflowTask.WorkflowTaskItems))
                .Include(cimCont => cimCont.cimContact.cimOrganisation)
                .Include(xRef => xRef.cfgXreferences)
                .FirstOrDefault(a => a.ContractName.Trim().ToUpper() == contractName.Trim().ToUpper() && a.FSP.Trim().ToUpper() == FSP.Trim().ToUpper() && a.wadtDeleted == false);

                _cfgContracts.Add(contract);
            }

            return contract;

        }

        private EnumAssetType GetEnumAssetType(string assetType, string fsp)
        {

            return this._enumAssetTypes.FirstOrDefault(a => a.enumDesc.ToUpper().Trim() == assetType.ToUpper().Trim() && a.fsp.ToUpper().Trim() == fsp.ToUpper().Trim() && a.wadtDeleted == false);

        }

        private EnumPhoneType GetEnumPhoneType(string ContactType)
        {
            return this._enumPhoneTypes.FirstOrDefault(a => a.enumDesc.Trim().ToUpper() == ContactType.Trim().ToUpper());
        }

        private EnumNoteType GetEnumNoteType(string NoteType)
        {
            return this._enumNoteTypes.FirstOrDefault(a => a.enumDesc.Trim().ToUpper() == NoteType.Trim().ToUpper());

        }

        private EnumDayPart GetEnumDayPart(string ScheduleType)
        {
            return this._enumDayParts.FirstOrDefault(a => a.enumDesc.Trim().ToUpper() == ScheduleType.Trim().ToUpper());

        }

        private EnumJobType GetEnumJobType(string JobType, string FSP)
        {

            return this._enumJobTypes.FirstOrDefault(a => a.FSP.Trim().ToUpper() == FSP.Trim().ToUpper() && a.wadtDeleted == false && a.enumDesc.ToUpper().Trim() == JobType.Trim().ToUpper());

        }

        private EnumRegion GetEnumRegion(string Region, string FSP)
        {
            return this._enumRegions.FirstOrDefault(a => a.enumDesc.Trim().ToUpper() == Region.Trim().ToUpper() && a.FSP.Trim().ToUpper() == FSP.Trim().ToUpper() && a.wadtDeleted == false);

        }

        private ApiRequestStatus GetApiRequestStatus(string StatusDescription)
        {
            return this._apirRequestStatuses.FirstOrDefault(a => a.ApiRequestStatusDescription == StatusDescription);
        }

        private ApiRequestType GetApiRequestTypes(string StatusType)
        {
            return this._apiRequestTypes.FirstOrDefault(a => a.ApiRequestTypeDescription == StatusType);
        }

        #endregion

        #region Write logging

        // Logging of details, exceptions, errors on local machine.
        // For investigation purposes. 
        //private void WriteProcessLog(string logEvent)
        //{
        //    _processLog.AppendLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + logEvent);
        //    WriteLocalLogFile(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + logEvent);
        //}

        //private void WriteLocalLogFile(string content)
        //{
        //    string logFileName = this._logFileName;
        //    string ph = "C:\\JobAPILogs\\";

        //    try
        //    {
        //        if (File.Exists(ph + ".txt"))
        //            File.Delete(ph + ".txt");
        //    }
        //    catch { }

        //    if (string.IsNullOrWhiteSpace(logFileName) == true)
        //        return;


        //    if (Directory.Exists(ph) == false)
        //        Directory.CreateDirectory(ph);

        //    try
        //    {
        //        StreamWriter sw = new StreamWriter(ph + Path.GetFileNameWithoutExtension(logFileName) + ".txt", true);
        //        sw.WriteLine(content);
        //        sw.Close();
        //        sw.Dispose();
        //    }
        //    catch (Exception e)
        //    {
        //        _log.Info("Exception in WriteLocalLogFile method of JobCreation : " + e.InnerException.ToString());
        //    }

        //}

        #endregion
    }

    public class JobNos
    {
        public string JobNo { get; set; }
    }
}
