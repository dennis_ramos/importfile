﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConXApiDtoModels;
using ConXApiDtoRepository;
using System.Data.Entity;

namespace ConXApiMapper
{
    public class Map
    {
        private JobDtoModel _db;


        //public static IQueryable<T> WithIncludes<T>(this IQueryable<T> source, string[] associations)
        //{
        //    var query = (ObjectQuery<T>)source;

        //    foreach (var assoc in associations)
        //    {
        //        query = query.Include(assoc);
        //    }
        //}


        public Map()
        {
            _db = new JobDtoModel();
        }

        public IEnumerable<ConXApiDtoModels.Job> GetJobsByJobNumber(string jobNo, string FSP)
        {


            var jobs = from j in _db.Jobs//.Where(a => a.JobNo == jobNo && a.FSP == FSP)
                       where
                       j.JobNo == jobNo &&
                       j.FSP == FSP
                       select new ConXApiDtoModels.Job()
                       {
                           JobID = j.JobID,
                           JobNo = j.JobNo,
                           FSP = j.FSP,
                           COReferenceNo = j.COReferenceNo,
                           WorkStream = j.WorkStream,
                           OrganisationName = j.OrganisationName,
                           DeviceName = j.DeviceName,
                           Description = j.Description,
                           JobStatus = j.JobStatus,
                           StatusBy = j.StatusBy,
                           StatusOn = j.StatusOn,
                           IsCompleted = j.IsCompleted,
                           CompletedBy = j.CompletedBy,
                           CompletedOn = j.CompletedOn,
                           WorkFlowName = j.WorkFlowName,
                           JobSequenceOrder = j.JobSequenceOrder,
                           JobType = j.JobType,
                           TechUserName = j.TechUserName,
                           Region = j.Region,
                           ModifiedOn = j.ModifiedOn,
                           ModifiedBy = j.ModifiedBy,
                           Group1 = j.Group1,
                           Group2 = j.Group2,
                           Group3 = j.Group3,
                           FieldStatus = j.FieldStatus,
                           Outcome = j.Outcome,
                           OutcomeDescription = j.OutcomeDescription,
                           FieldOutcomeDate = j.FieldOutcomeDate,
                           JobQualifier = j.JobQualifier

                       };

            return jobs.ToList();

        }

        public IEnumerable<ConXApiDtoModels.Job> GetResultForJobsByJobNumber(string jobNo, string FSP)
        {
            var jobs = from j in _db.Jobs
                       .Include("JobTasks")
                       .Include("JobTaskItem")
                       where
                       j.JobNo == jobNo &&
                       j.FSP == FSP
                       select new ConXApiDtoModels.Job()
                       {
                           JobID = j.JobID,
                           JobNo = j.JobNo,
                           FSP = j.FSP,
                           COReferenceNo = j.COReferenceNo,
                           WorkStream = j.WorkStream,
                           OrganisationName = j.OrganisationName,
                           DeviceName = j.DeviceName,
                           Description = j.Description,
                           JobStatus = j.JobStatus,
                           StatusBy = j.StatusBy,
                           StatusOn = j.StatusOn,
                           IsCompleted = j.IsCompleted,
                           CompletedBy = j.CompletedBy,
                           CompletedOn = j.CompletedOn,
                           WorkFlowName = j.WorkFlowName,
                           JobSequenceOrder = j.JobSequenceOrder,
                           JobType = j.JobType,
                           TechUserName = j.TechUserName,
                           Region = j.Region,
                           ModifiedOn = j.ModifiedOn,
                           ModifiedBy = j.ModifiedBy,
                           Group1 = j.Group1,
                           Group2 = j.Group2,
                           Group3 = j.Group3,
                           FieldStatus = j.FieldStatus,
                           Outcome = j.Outcome,
                           OutcomeDescription = j.OutcomeDescription,
                           FieldOutcomeDate = j.FieldOutcomeDate,
                           JobQualifier = j.JobQualifier,

                           JobTasks = j.JobTasks.Select(x => new ConXApiDtoModels.JobTask
                           {
                               JobTaskID = x.JobTaskID,
                               JobID = x.JobID,
                               TaskDescription = x.TaskDescription,
                               TaskName = x.TaskName,
                               TaskID = x.TaskID,

                               JobTaskItems = x.JobTaskItems.Select(i => new ConXApiDtoModels.JobTaskItem
                               {
                                   JobTaskItemID = i.JobTaskItemID,
                                   TaskItemName = i.TaskItemName,
                                   TaskItemID = i.TaskItemID,
                                   Result = i.Result,
                                   EnteredBy = i.EnteredBy,
                                   EnteredOn = i.EnteredOn
                               }).ToList()

                           }).ToList()
                       };

            return jobs.ToList();
        }

        public IEnumerable<ConXApiDtoModels.Job> GetDetailedJobsByJobNumber(string jobNo, string FSP)
        {

            var jobs = from j in _db.Jobs
                           //.Include(i=> i.JobAddresses)
                       .Include("JobAddress")
                       where
                       j.JobNo == jobNo &&
                       j.FSP == FSP
                       select new ConXApiDtoModels.Job()
                       {
                           JobID = j.JobID,
                           JobNo = j.JobNo,
                           FSP = j.FSP,
                           COReferenceNo = j.COReferenceNo,
                           WorkStream = j.WorkStream,
                           OrganisationName = j.OrganisationName,
                           DeviceName = j.DeviceName,
                           Description = j.Description,
                           JobStatus = j.JobStatus,
                           StatusBy = j.StatusBy,
                           StatusOn = j.StatusOn,
                           IsCompleted = j.IsCompleted,
                           CompletedBy = j.CompletedBy,
                           CompletedOn = j.CompletedOn,
                           WorkFlowName = j.WorkFlowName,
                           JobSequenceOrder = j.JobSequenceOrder,
                           JobType = j.JobType,
                           TechUserName = j.TechUserName,
                           Region = j.Region,
                           ModifiedOn = j.ModifiedOn,
                           ModifiedBy = j.ModifiedBy,
                           Group1 = j.Group1,
                           Group2 = j.Group2,
                           Group3 = j.Group3,
                           FieldStatus = j.FieldStatus,
                           Outcome = j.Outcome,
                           OutcomeDescription = j.OutcomeDescription,
                           FieldOutcomeDate = j.FieldOutcomeDate,
                           JobQualifier = j.JobQualifier,

                           JobAddresses = j.JobAddresses.Select(x => new ConXApiDtoModels.JobAddress
                           {
                               JobAddressID = x.JobAddressID,
                               JobID = x.JobID,
                               AddressDescription = x.AddressDescription,
                               StreetNumber = x.StreetNumber,
                               FlatAppartment = x.FlatAppartment,
                               StreetName = x.StreetName,
                               Suburb = x.Suburb,
                               City = x.City,
                               Country = x.Country,
                               Latitude = x.Latitude,
                               Longitude = x.Longitude,
                               PrimaryAddress = x.PrimaryAddress,
                               Region = x.Region


                           }).ToList()
                       };

            return jobs.ToList();

        }


        public IEnumerable<ConXApiDtoModels.Job> GetJobsByWorkStreamName(
            string workStreamName,
            string jobStatus,
            DateTime modifiedFrom,
            DateTime modifiedTo,
            string FSP)
        {
            var jobs = from j in _db.Jobs//.Where(a => a.JobNo == jobNo && a.FSP == FSP)
                       where
                       j.WorkStream == workStreamName &&
                       j.FSP == FSP &&
                       (DbFunctions.TruncateTime(j.ModifiedOn) >= modifiedFrom.Date && DbFunctions.TruncateTime(j.ModifiedOn) <= modifiedTo.Date)
                       select new ConXApiDtoModels.Job()
                       {
                           JobID = j.JobID,
                           JobNo = j.JobNo,
                           FSP = j.FSP,
                           COReferenceNo = j.COReferenceNo,
                           WorkStream = j.WorkStream,
                           OrganisationName = j.OrganisationName,
                           DeviceName = j.DeviceName,
                           Description = j.Description,
                           JobStatus = j.JobStatus,
                           StatusBy = j.StatusBy,
                           StatusOn = j.StatusOn,
                           IsCompleted = j.IsCompleted,
                           CompletedBy = j.CompletedBy,
                           CompletedOn = j.CompletedOn,
                           WorkFlowName = j.WorkFlowName,
                           JobSequenceOrder = j.JobSequenceOrder,
                           JobType = j.JobType,
                           TechUserName = j.TechUserName,
                           Region = j.Region,
                           ModifiedOn = j.ModifiedOn,
                           ModifiedBy = j.ModifiedBy,
                           Group1 = j.Group1,
                           Group2 = j.Group2,
                           Group3 = j.Group3,
                           FieldStatus = j.FieldStatus,
                           Outcome = j.Outcome,
                           OutcomeDescription = j.OutcomeDescription,
                           FieldOutcomeDate = j.FieldOutcomeDate,
                           JobQualifier = j.JobQualifier

                       };

            return jobs.ToList();

        }

        public IEnumerable<ConXApiDtoModels.Job> GetDetailedJobsByWorkStreamName(
            string workStreamName,
            string jobStatus,
            DateTime modifiedFrom,
            DateTime modifiedTo,
            string FSP)
        {
            var jobs = from j in _db.Jobs
                       .Include(i => i.JobAddresses)

                       where
                       j.WorkStream == workStreamName &&
                       j.FSP == FSP &&
                       j.JobStatus == jobStatus &&
                       (DbFunctions.TruncateTime(j.ModifiedOn) >= modifiedFrom.Date && DbFunctions.TruncateTime(j.ModifiedOn) <= modifiedTo.Date)
                       select new ConXApiDtoModels.Job()
                       {
                           JobID = j.JobID,
                           JobNo = j.JobNo,
                           FSP = j.FSP,
                           COReferenceNo = j.COReferenceNo,
                           WorkStream = j.WorkStream,
                           OrganisationName = j.OrganisationName,
                           DeviceName = j.DeviceName,
                           Description = j.Description,
                           JobStatus = j.JobStatus,
                           StatusBy = j.StatusBy,
                           StatusOn = j.StatusOn,
                           IsCompleted = j.IsCompleted,
                           CompletedBy = j.CompletedBy,
                           CompletedOn = j.CompletedOn,
                           WorkFlowName = j.WorkFlowName,
                           JobSequenceOrder = j.JobSequenceOrder,
                           JobType = j.JobType,
                           TechUserName = j.TechUserName,
                           Region = j.Region,
                           ModifiedOn = j.ModifiedOn,
                           ModifiedBy = j.ModifiedBy,
                           Group1 = j.Group1,
                           Group2 = j.Group2,
                           Group3 = j.Group3,
                           FieldStatus = j.FieldStatus,
                           Outcome = j.Outcome,
                           OutcomeDescription = j.OutcomeDescription,
                           FieldOutcomeDate = j.FieldOutcomeDate,
                           JobQualifier = j.JobQualifier,

                           JobAddresses = j.JobAddresses.Select(x => new ConXApiDtoModels.JobAddress
                           {
                               JobAddressID = x.JobAddressID,
                               JobID = x.JobID,
                               AddressDescription = x.AddressDescription,
                               StreetNumber = x.StreetNumber,
                               FlatAppartment = x.FlatAppartment,
                               StreetName = x.StreetName,
                               Suburb = x.Suburb,
                               City = x.City,
                               Country = x.Country,
                               Latitude = x.Latitude,
                               Longitude = x.Longitude,
                               PrimaryAddress = x.PrimaryAddress,
                               Region = x.Region


                           }).ToList()
                       };

            return jobs.ToList();

        }


        public IEnumerable<ConXApiDtoModels.Job> GetJobsByWorkFlow(
            string workFlowName,
            string jobStatus,
            DateTime modifiedFrom,
            DateTime modifiedTo,
            string FSP
            )
        {
            var jobs = from j in _db.Jobs
                       where
                       j.WorkFlowName == workFlowName &&
                       j.FSP == FSP &&
                       j.JobStatus == jobStatus &&
                       (DbFunctions.TruncateTime(j.ModifiedOn) >= modifiedFrom.Date && DbFunctions.TruncateTime(j.ModifiedOn) <= modifiedTo.Date)
                       select new ConXApiDtoModels.Job()
                       {
                           JobID = j.JobID,
                           JobNo = j.JobNo,
                           FSP = j.FSP,
                           COReferenceNo = j.COReferenceNo,
                           WorkStream = j.WorkStream,
                           OrganisationName = j.OrganisationName,
                           DeviceName = j.DeviceName,
                           Description = j.Description,
                           JobStatus = j.JobStatus,
                           StatusBy = j.StatusBy,
                           StatusOn = j.StatusOn,
                           IsCompleted = j.IsCompleted,
                           CompletedBy = j.CompletedBy,
                           CompletedOn = j.CompletedOn,
                           WorkFlowName = j.WorkFlowName,
                           JobSequenceOrder = j.JobSequenceOrder,
                           JobType = j.JobType,
                           TechUserName = j.TechUserName,
                           Region = j.Region,
                           ModifiedOn = j.ModifiedOn,
                           ModifiedBy = j.ModifiedBy,
                           Group1 = j.Group1,
                           Group2 = j.Group2,
                           Group3 = j.Group3,
                           FieldStatus = j.FieldStatus,
                           Outcome = j.Outcome,
                           OutcomeDescription = j.OutcomeDescription,
                           FieldOutcomeDate = j.FieldOutcomeDate,
                           JobQualifier = j.JobQualifier,

                           JobAddresses = j.JobAddresses.Select(x => new ConXApiDtoModels.JobAddress
                           {
                               JobAddressID = x.JobAddressID,
                               JobID = x.JobID,
                               AddressDescription = x.AddressDescription,
                               StreetNumber = x.StreetNumber,
                               FlatAppartment = x.FlatAppartment,
                               StreetName = x.StreetName,
                               Suburb = x.Suburb,
                               City = x.City,
                               Country = x.Country,
                               Latitude = x.Latitude,
                               Longitude = x.Longitude,
                               PrimaryAddress = x.PrimaryAddress,
                               Region = x.Region


                           }).ToList()
                       };

            return jobs.ToList();
        }


    }
}
