﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using ConXApiDtoRepository;
using ConXApiDtoModels;
using ConXApiMapper;
using r = ConXApiDtoModels.DataCreationRequest;
using Newtonsoft.Json;


namespace TestConXApiMapper
{
    class Program
    {
        static void Main(string[] args)
        {
            //Map mapper = new Map();
            //List<Job> jobs = mapper.GetDetailedJobsByJobNumber("999999", "WELLS").ToList();

            //foreach (var job in jobs)
            //{
            //    foreach (var add in job.JobAddresses)
            //    {
            //        Console.Write(add.StreetName);
            //    }
            //}

            //List<ConXApiDtoRepository.Job> jobs = mapper.Jobs().ToList();
            //foreach (var job in jobs)
            //{
            //    Console.WriteLine(job.FSP);
            //}
            CallRequest();
        }

        private static void CallRequest()
        {
            r.Request request = new r.Request { RequestType = "Create Job", RequestedBy = "dramos", FSP = "WELLS", JobRequests = new List<r.JobRequest>() };
            var list = new List<r.JobRequest>();

            //r.JobRequest jRequest = ;

            request.JobRequests.Add(CreateJobRequestObject("D2000024150112"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150113"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150114"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150115"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150116"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150117"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150118"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150119"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150120"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150121"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150122"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150123"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150124"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150125"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150126"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150127"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150128"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150129"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150130"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150131"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150132"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150133"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150134"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150135"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150136"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150137"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150138"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150139"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150140"));
            request.JobRequests.Add(CreateJobRequestObject("D2000024150141"));
            
                        
            JobCreation jc = new JobCreation();
            

            string RequestData = JsonConvert.SerializeObject(request);
            Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            var response = jc.ProcessRequest(request, "Wells", "dramos");
            jc.ProcessRequestID(response.RequestID);
            Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            string dennis = "";
            dennis += "Success";
            Console.Read();


        }

        private static r.JobRequest CreateJobRequestObject(string jobReferenceNo)
        {
            r.JobRequest jRequest = CreateJobRequest(jobReferenceNo);

            jRequest.JobAddresses.Add(CreateAddress());
            jRequest.JobAttributes = CreateJobAttributes();
            jRequest.JobContacts.Add(CreateContact());

            jRequest.JobNotes.Add(new r.Note { NoteType = "Client", NoteMessage = "Next Reading Date: 2014-09-18 00:00:00.0  Reading Frequency: 20  Window Days After: 5  Window Days Prior: 5  Business Days: N" });
            jRequest.JobNotes.Add(new r.Note { NoteType = "Client", NoteMessage = "Test Note 2" });

            jRequest.JobAssets.Add(CreateMeterBoardAsset("Meter Board", "2024000", true));
            jRequest.JobAssets.Add(CreateMeterAsset("Old Meter", "57746", true));
            jRequest.JobAssets.Add(CreateRelayAsset("Relay", "8124876-DRR", true));

            jRequest.NonAssetBasedTaskPrePopulations.Add(CreatePrePopulationRequest());

            jRequest.JobSchedule =
                new r.JobSchedule
                {
                    CreateMultipleInstances = false,
                    RequiresAppointment = true,
                    TechUserName = "dramos",
                    PlannedFrom = DateTime.Now.Date.AddDays(1),
                    ScheduleType = "Scheduled For a Day"
                };
            return jRequest;
        }

        public static r.NonAssetBasedTaskPrePopulation CreatePrePopulationRequest()
        {
            r.NonAssetBasedTaskPrePopulation dd = new r.NonAssetBasedTaskPrePopulation
            {
                TaskName = "Safety Questionnaire",
                JobTaskItems = new List<r.JobTaskItem>()
            };

            dd.JobTaskItems.Add(new r.JobTaskItem { TaskItemOrder = 3, TaskItemValue = "No" });
            return dd;
        }

        private static r.JobRequest CreateJobRequest(string jobReferenceNo)
        {
            return new r.JobRequest
            {
                Description = "Dennis Test 1",
                Group1 = "GRP1",
                Group2 = "GRP2",
                Group3 = "0000004084TR6FE",
                JobReference = jobReferenceNo,
                Region = "Taranaki",
                WorkStream = "Metrix Cat1 Meter Installation B2B",
                JobType = "Cat1 Installation",
                JobSequence = null,
                JobAddresses = new List<r.JobAddress>(),
                JobAssets = new List<r.JobAsset>(),
                JobAttributes = new List<r.JobAttribute>(),
                JobContacts = new List<r.JobContact>(),
                JobNotes = new List<r.Note>(),
                JobSchedule = new r.JobSchedule(),
                NonAssetBasedTaskPrePopulations = new List<r.NonAssetBasedTaskPrePopulation>()
            };
        }
        private static r.JobAddress CreateAddress()
        {
            return new r.JobAddress
               {
                   AddressDescription = "96 Omata Road Marfell, New Plymouth",
                   City = "New Plymouth",
                   Country = "New Zealand",
                   FlatAppartment = "",
                   Latitude = null,
                   Longitude = null,
                   PrimaryAddress = true,
                   Region = "Taranaki",
                   StreetName = "Omata Road",
                   StreetNumber = "96",
                   Suburb = "Marfell"
               };
        }
        private static r.JobContact CreateContact()
        {
            r.JobContact cont = new r.JobContact
            {
                FirstName = "Dennis",
                LastName = "Ramos",
                MiddleName = "",
                JobContactDetails = new List<r.JobContactDetail>()
            };

            cont.JobContactDetails.Add(new r.JobContactDetail { ContactType = "Home", ContactValue = "7512746" });
            cont.JobContactDetails.Add(new r.JobContactDetail { ContactType = "Work", ContactValue = "7530347" });
            cont.JobContactDetails.Add(new r.JobContactDetail { ContactType = "Mobile", ContactValue = "0210347868" });
            cont.JobContactDetails.Add(new r.JobContactDetail { ContactType = "Email", ContactValue = "dennis.ramos@e-mergedata.com" });
            return cont;
        }

        private static List<r.JobAttribute> CreateJobAttributes()
        {
            List<r.JobAttribute> attribs = new List<r.JobAttribute>();
            attribs.Add(new r.JobAttribute { AttributeName = "ServiceRequestNumber", AttributeValue = "2000024150111" });
            attribs.Add(new r.JobAttribute { AttributeName = "ScheduledStartDate" });
            attribs.Add(new r.JobAttribute { AttributeName = "ScheduledEndDate" });
            attribs.Add(new r.JobAttribute { AttributeName = "AppointmentRequired", AttributeValue = "No" });
            attribs.Add(new r.JobAttribute { AttributeName = "AfterHoursManualRead", AttributeValue = "No" });
            attribs.Add(new r.JobAttribute { AttributeName = "SiteDisconnected", AttributeValue = "No" });
            attribs.Add(new r.JobAttribute { AttributeName = "Key", AttributeValue = "No" });
            attribs.Add(new r.JobAttribute { AttributeName = "Dog", AttributeValue = "No", TaskNameToPrePopulate = "Safety Questionnaire", TaskItemNameToPrePopulate = "" }); //
            attribs.Add(new r.JobAttribute { AttributeName = "MedicallyDependent", AttributeValue = "No", TaskNameToPrePopulate = "Safety Questionnaire", TaskItemNameToPrePopulate = "" }); //
            attribs.Add(new r.JobAttribute { AttributeName = "RMR", AttributeValue = "No" });
            attribs.Add(new r.JobAttribute { AttributeName = "MarketSegment" });
            attribs.Add(new r.JobAttribute { AttributeName = "BusinessDays", AttributeValue = "N" });
            attribs.Add(new r.JobAttribute { AttributeName = "ReadingFrequency", AttributeValue = "20" });
            attribs.Add(new r.JobAttribute { AttributeName = "NextReadingDate", AttributeValue = "2014-09-18 00:00:00.0" });
            attribs.Add(new r.JobAttribute { AttributeName = "ExclusionWindowDaysAfter", AttributeValue = "5" });
            attribs.Add(new r.JobAttribute { AttributeName = "ExclusionWindowDaysPrior", AttributeValue = "5" });
            attribs.Add(new r.JobAttribute { AttributeName = "HighestMeteringCategory", AttributeValue = "1" });
            attribs.Add(new r.JobAttribute { AttributeName = "GXP", AttributeValue = "GFD0331" });
            attribs.Add(new r.JobAttribute { AttributeName = "Trader", AttributeValue = "MEEN" });
            attribs.Add(new r.JobAttribute { AttributeName = "Network", AttributeValue = "VECT" });
            attribs.Add(new r.JobAttribute { AttributeName = "Region", AttributeValue = "Taranaki" });
            return attribs;

        }

        private static r.JobAsset CreateMeterBoardAsset(string assetType, string AssetNo, bool populateTaskDescriptionWithAssetValue)
        {
            r.JobAsset asset1 =
            new r.JobAsset
            {
                AssetType = assetType,
                AssetValueOrSerialNo = AssetNo,
                PopulateTaskDescriptionWithAssetValue = populateTaskDescriptionWithAssetValue,
                Notes = new List<r.Note>(),
                SubAssets = new List<r.JobAsset>(),
                AssetAttributes = new List<r.JobAssetAttribute>()

            };

            asset1.AssetAttributes.Add(new r.JobAssetAttribute
            {
                AttributeName = "GISLatitude",
                AttributeValue = ""

            });

            asset1.AssetAttributes.Add(new r.JobAssetAttribute
            {
                AttributeName = "GISLongitude",
                AttributeValue = ""

            });

            asset1.AssetAttributes.Add(new r.JobAssetAttribute
            {
                AttributeName = "InstallationNumber",
                AttributeValue = "1"

            });

            asset1.AssetAttributes.Add(new r.JobAssetAttribute
            {
                AttributeName = "LocationCode",
                AttributeValue = "IN"

            });

            asset1.AssetAttributes.Add(new r.JobAssetAttribute
            {
                AttributeName = "NumberOfComponents",
                AttributeValue = "2"

            });

       


            return asset1;
        }

        private static r.JobAsset CreateRelayAsset(string assetType, string AssetNo, bool populateTaskDescriptionWithAssetValue)
        {
            r.JobAsset asset1 =
            new r.JobAsset
            {
                AssetType = assetType,
                AssetValueOrSerialNo = AssetNo,
                PopulateTaskDescriptionWithAssetValue = populateTaskDescriptionWithAssetValue,
                Notes = new List<r.Note>(),
                SubAssets = new List<r.JobAsset>(),
                AssetAttributes = new List<r.JobAssetAttribute>()

            };
            //asset1.AssetAttributes.Add(new r.JobAssetAttribute { AttributeName = "Parent Asset", AttributeValue = "" });
            asset1.AssetAttributes.Add(new r.JobAssetAttribute { AttributeName = "Owner", AttributeValue = "NGCM" });
            asset1.AssetAttributes.Add(new r.JobAssetAttribute { AttributeName = "Metrix Job Number", AttributeValue = "2024000" });
            asset1.AssetAttributes.Add(new r.JobAssetAttribute { AttributeName = "RelayChannel", AttributeValue = "" });
            asset1.AssetAttributes.Add(new r.JobAssetAttribute { AttributeName = "RelaySwitchPosition", AttributeValue = "" });
            return asset1;
        }

        private static r.JobAsset CreateMeterAsset(string assetType, string AssetNo, bool populateTaskDescriptionWithAssetValue)
        {
            r.JobAsset asset1 =
            new r.JobAsset
            {
                AssetType = assetType,
                AssetValueOrSerialNo = AssetNo,
                PopulateTaskDescriptionWithAssetValue = populateTaskDescriptionWithAssetValue,
                Notes = new List<r.Note>(),
                SubAssets = new List<r.JobAsset>(),
                AssetAttributes = new List<r.JobAssetAttribute>()

            };
            
            asset1.AssetAttributes.Add(new r.JobAssetAttribute { AttributeName = "Owner", AttributeValue = "NGCM" });
            asset1.AssetAttributes.Add(new r.JobAssetAttribute { AttributeName = "Metrix Job Number", AttributeValue = "2024000" });
            asset1.AssetAttributes.Add(new r.JobAssetAttribute { AttributeName = "MeterTariff", AttributeValue = "" });
            asset1.AssetAttributes.Add(new r.JobAssetAttribute { AttributeName = "UsageType", AttributeValue = "" });
            asset1.AssetAttributes.Add(new r.JobAssetAttribute { AttributeName = "NumberOfRegisters", AttributeValue = "1" });
            asset1.AssetAttributes.Add(new r.JobAssetAttribute { AttributeName = "Model", AttributeValue = "" });
            asset1.AssetAttributes.Add(new r.JobAssetAttribute { AttributeName = "Phase", AttributeValue = "" });
            asset1.AssetAttributes.Add(new r.JobAssetAttribute { AttributeName = "MeteringCategory", AttributeValue = "1" });

            asset1.SubAssets.Add(CreateRegister1Asset("Register", "1", true));
            return asset1;
        }

        private static r.JobAsset CreateRegister1Asset(string assetType, string AssetNo, bool populateTaskDescriptionWithAssetValue)
        {
            r.JobAsset asset1 =
            new r.JobAsset
            {
                AssetType = assetType,
                AssetValueOrSerialNo = AssetNo,
                PopulateTaskDescriptionWithAssetValue = populateTaskDescriptionWithAssetValue,
                Notes = new List<r.Note>(),
                SubAssets = new List<r.JobAsset>(),
                AssetAttributes = new List<r.JobAssetAttribute>()

            };
            //asset1.AssetAttributes.Add(new r.JobAssetAttribute { AttributeName = "Parent Asset", AttributeValue = "57746" });
            asset1.AssetAttributes.Add(new r.JobAssetAttribute { AttributeName = "Read", AttributeValue = "" });
            asset1.AssetAttributes.Add(new r.JobAssetAttribute { AttributeName = "UOM", AttributeValue = "kWh" });
            asset1.AssetAttributes.Add(new r.JobAssetAttribute { AttributeName = "Config.", AttributeValue = "IN24" });
            asset1.AssetAttributes.Add(new r.JobAssetAttribute { AttributeName = "Dials", AttributeValue = "5" });
            asset1.AssetAttributes.Add(new r.JobAssetAttribute { AttributeName = "EnergyFlowDirection", AttributeValue = "X" });
            asset1.AssetAttributes.Add(new r.JobAssetAttribute { AttributeName = "RegisterNumber", AttributeValue = "1" });


            return asset1;
        }



    }
}
