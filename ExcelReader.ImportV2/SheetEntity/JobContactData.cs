﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConXApiRepository;

namespace ExcelReader.ImportV2.SheetEntity
{
    public class JobContactData
    {
        public int RowId { get; set; }
        public bool HasError { get; set; }
        public string ErrorDescription { get; set; }
        public string JobRef { get; set; }
        /// <summary>
        /// The unique ID for the contact, for the job. Use if you wish to nsert multiple contact types and values against same contact ID for a job
        /// </summary>
        public string ContactID { get; set; }
        public string ContactDescription { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactType { get; set; }
        public string ContactValue { get; set; }
        public StringBuilder er { get; set; }

        public JobContact jContact { get; set; }

        public JobContactData()
        {
            this.RowId = 0;
            this.HasError = false;
            this.ErrorDescription = "";
            this.JobRef = "";
            this.ContactID = "";
            this.ContactDescription = "";
            this.FirstName = "";
            this.LastName = "";
            this.ContactType = "";
            this.ContactValue = "";
            this.er = new StringBuilder();
        }
    }
}
