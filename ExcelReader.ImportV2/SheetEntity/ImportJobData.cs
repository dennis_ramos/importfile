﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReader.ImportV2.SheetEntity
{
    public class ImportJobData
    {
        public int RowId { get; set; }
        public bool HasError { get; set; }
        public string ErrorDescription { get; set; }
        public string FSP { get; set; }
        public string JobRef { get; set; }
        public string JobType { get; set; }
        public string WorkStreamName { get; set; }
        public string JobDescription { get; set; }
        public string Group1 { get; set; }
        public string Group2 { get; set; }
        public string Group3 { get; set; }
        public string ConXJobReference { get; set; }
        public string JobSeq { get; set; }
        public StringBuilder er { get; set; }
        
        public ImportJobData()
        {
            this.RowId = 0;
            this.HasError = false;
            this.ErrorDescription = "";
            this.FSP = "";
            this.JobRef = "";
            this.JobType = "";
            this.WorkStreamName = "";
            this.JobDescription = "";
            this.Group1 = "";
            this.Group2 = "";
            this.Group3 = "";
            this.ConXJobReference = "";
            this.JobSeq = "";
            er = new StringBuilder();
        }

        

    }
}
