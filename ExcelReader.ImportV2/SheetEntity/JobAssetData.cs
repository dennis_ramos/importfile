﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReader.ImportV2.SheetEntity
{
    public class JobAssetData
    {
        public int RowId { get; set; }
        public bool HasError { get; set; }
        public string ErrorDescription { get; set; }
        public string JobRef { get; set; }
        public string AssetType { get; set; }
        public string AssetValue { get; set; }
        public string AssetAttribute { get; set; }
        public string AssetAttributeValue { get; set; }
        public string NewTaskNameInWorkflow { get; set; }
        public string ParentTaskName { get; set; }
        public string ParentAssetValue { get; set; }
        public string AppendAssetValueToNewTask { get; set; }
        public StringBuilder er { get; set; }

        public JobAssetData()
        {
            this.RowId = 0;
            this.HasError = false;
            this.ErrorDescription = "";
            this.JobRef = "";
            this.AssetType = "";
            this.AssetValue = "";
            this.AssetAttribute = "";
            this.AssetAttributeValue = "";
            this.NewTaskNameInWorkflow = "";
            this.ParentTaskName = "";
            this.ParentAssetValue = "";
            this.AppendAssetValueToNewTask = "Yes";
            er = new StringBuilder();

        }
    }
}
