﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReader.ImportV2.SheetEntity
{
    public class JobAddressData
    {
        public int RowId { get; set; }
        public bool HasError { get; set; }
        public string ErrorDescription { get; set; }
        public string JobRef { get; set; }
        public string AddressDescription { get; set; }
        public string StreetNumber { get; set; }
        public string StreetName { get; set; }
        public string StreetUnit { get; set; }
        public string Suburb { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string lat { get; set; }
        public string lon { get; set; }
        public string isPrimaryAddress { get; set; }

        public StringBuilder er { get; set; }
        public JobAddressData()
        {
            this.RowId = 0;
            this.HasError = false;
            this.ErrorDescription = "";
            this.JobRef = "";
            this.AddressDescription = "";
            this.StreetNumber = "";
            this.StreetName = "";
            this.StreetUnit = "";
            this.Suburb = "";
            this.City = "";
            this.Region = "";
            this.lat = "";
            this.lon = "";
            this.isPrimaryAddress = "No";
            er = new StringBuilder();
        }

        

    }
}
