﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReader.ImportV2.SheetEntity
{
    public class JobNoteData
    {
        public int RowId { get; set; }
        public bool HasError { get; set; }
        public string ErrorDescription { get; set; }
        public string JobRef { get; set; }
        public string NoteType { get; set; }
        public string Note { get; set; }
        public StringBuilder er { get; set; }
        public string NoteTaskName { get; set; }
        public string NoteAssetValue { get; set; }
        public string ParentTaskName { get; set; }
        public string ParentAssetValue { get; set; }
        public JobNoteData()
        {
            this.RowId = 0;
            this.HasError = false;
            this.ErrorDescription = "";
            this.JobRef = "";
            this.NoteType = "";
            this.Note = "";
            this.NoteTaskName = "";
            this.NoteAssetValue = "";
            this.ParentTaskName = "";
            this.ParentAssetValue = "";
            er = new StringBuilder();
        }
    }
}
