﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReader.ImportV2.SheetEntity
{
    public class JobTaskItemData 
    {
        public int RowId { get; set; }
        public bool HasError { get; set; }
        public string ErrorDescription { get; set; }
        public string JobRef { get; set; }

        public string TaskName { get; set; }
        public string AssetValue { get; set; }
        public string TaskItemOrder { get; set; }
        public string TaskItemValue { get; set; }
        public string TaskItemFormat { get; set; }
        public string ParentTaskName { get; set; }
        public string ParentAssetValue { get; set; }
        public StringBuilder er { get; set; }

        public JobTaskItemData()
        {
            this.RowId = 0;
            this.HasError = false;
            this.ErrorDescription = "";
            this.JobRef = "";
            this.TaskName = "";
            this.AssetValue = "";
            this.TaskItemOrder = "";
            this.TaskItemValue = "";
            this.TaskItemFormat = "";
            this.ParentTaskName = "";
            this.ParentAssetValue = "";
            er = new StringBuilder();

        }
    }
}
