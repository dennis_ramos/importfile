﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReader.ImportV2.SheetEntity
{
    public class JobAttributeData
    {
        public int RowId { get; set; }
        public bool HasError { get; set; }
        public string ErrorDescription { get; set; }
        public string JobRef { get; set; }
        public string AttributeType { get; set; }
        public string AttributeValue { get; set; }
        public StringBuilder er { get; set; }
        public JobAttributeData()
        {
            this.RowId = 0;
            this.HasError = false;
            this.ErrorDescription = "";
            this.JobRef = "";
            this.AttributeType = "";
            this.AttributeValue = "";
            er = new StringBuilder();
        }
    }
}
