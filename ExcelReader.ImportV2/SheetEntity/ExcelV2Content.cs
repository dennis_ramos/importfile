﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReader.ImportV2.SheetEntity
{
    public class ExcelV2Content
    {
        public List<ImportJobData> ImportJobDataList { get; set; }
        public List<JobSchedulingData> JobSchedulingDataList { get; set; }
        public List<JobAssetData> JobAssetDataList { get; set; }
        public List<JobContactData> JobContactDataList { get; set; }
        public List<JobAddressData> JobAddressDataList { get; set; }
        public List<JobNoteData> JobNoteDataList { get; set; }
        public List<JobAttributeData> JobAttributeDataList { get; set; }
        public List<JobTaskItemData> JobTaskItemDataList { get; set; }

        public ExcelV2Content()
        {
            ImportJobDataList = new List<ImportJobData>();
            JobSchedulingDataList = new List<JobSchedulingData>();
            JobAssetDataList = new List<JobAssetData>();
            JobContactDataList = new List<JobContactData>();
            JobAddressDataList = new List<JobAddressData>();
            JobNoteDataList = new List<JobNoteData>();
            JobAttributeDataList = new List<JobAttributeData>();
            JobTaskItemDataList = new List<JobTaskItemData>();
        }
    }
}
