﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReader.ImportV2.SheetEntity
{
    public class JobSchedulingData
    {

        public int RowId { get; set; }
        public bool HasError { get; set; }
        public string ErrorDescription { get; set; }
        public string JobRef { get; set; }
        public string ScheduleType { get; set; }

        public string AppointmentType { get; set; }

        /// <summary>
        /// "dd/MM/yyyy"<para></para>
        /// Populate in job planned from date.<para></para>
        /// If planned from time or planned to date contain values, this must contain a value, else optional
        /// </summary>
        public string PlannedFromDate { get; set; }

        /// <summary>
        /// valid values are HH:MM, "AM" or "PM"<para></para>
        /// If planned to time contains a value, this must contain a value, else optional.
        /// </summary>
        public string PlannedFromTime { get; set; }

        /// <summary>
        /// "dd/MM/yyyy"<para></para>
        /// If planned to time contains a value, this must contain a value, else optional.
        /// </summary>
        public string PlannedToDate { get; set; }

        /// <summary>
        /// valid values are HH:MM, "AM" or "PM"<para></para>
        /// If planned from time AND planned to date contain values, this must contain a value, else optional. <para></para>
        /// If planned from time is AM or PM, this must NOT be populated
        /// </summary>
        public string PlannedToTime { get; set; }

        /// <summary>
        /// valid values = "yes" or blank<para></para>
        /// Flag job as appointment needed if YES.<para></para>
        /// </summary>
        public string AppointmentNeeded { get; set; }

        public string DateTimeConstraint { get; set; }
        public string PreferredUsername { get; set; }
        public string Frequency { get; set; }
        public string NumberOfOccurrences { get; set; }
        public string AssignToDevice { get; set; }
        public string ContractModel { get; set; }
        public StringBuilder er { get; set; }

        public JobSchedulingData()
        {
            this.RowId = 0;
            this.HasError = false;
            this.ErrorDescription = "";
            this.JobRef = "";
            this.AppointmentType = "";
            this.PlannedFromDate = "";
            this.PlannedFromTime = "";
            this.PlannedToDate = "";
            this.PlannedToTime = "";
            this.AppointmentNeeded = "";
            this.DateTimeConstraint = "";
            this.PreferredUsername = "";
            this.Frequency = "";
            this.NumberOfOccurrences = "";
            this.AssignToDevice = "No";
            this.ContractModel = "";
            er = new StringBuilder();
        }

        /// <summary>
        /// Populate in job planned from date.
        /// If PlannedFromDate is the only date/time field populated set Appointment type to "scheduled for day" if appointment needed is blank, or DAY if appointment needed=yes.<para></para>
        /// If PlannedFromDate contains a date, planned from time contains values,  planned from time and to time are blank,  set app type to "exact time"<para></para>
        /// If PlannedFromDate contains a date and planned from time is AM or PM, set app type to their respective values of AM or PM<para></para>
        /// If PlannedFromDate contains a date, planned to date is set but planned to and from times are blank, set app type to "scheduled date range" if appointmented needed is not set, or "date range" if appointment needed is set<para></para>
        /// If PlannedFromDate contains a date, planned from time contains a time, planned to date  and time contain values, set app type to "scheduled date time range"<para></para>
        /// 
        /// If blank but app type=exact time, AM or PM, set appointment needed flag for job anyway<para></para>
        /// If app type=scheduled date time range" and appointment needed=yes, ignore appointment needed flag if "yes"
        /// </summary>
        /// <param name="lstDayPart"></param>
        public void SetAppointmentRelated(List<EnumDayPart> lstDayPart)
        {
            if (this.PlannedFromDate != string.Empty && this.PlannedFromTime == string.Empty &&
                this.PlannedToDate == string.Empty && this.PlannedToTime == string.Empty)
            {
                if (this.AppointmentNeeded == string.Empty)
                    this.AppointmentType = lstDayPart[7].enumDesc;
                if (this.AppointmentNeeded.ToUpper() == "YES")
                    this.AppointmentType = lstDayPart[2].enumDesc;
            }

            if (this.PlannedFromDate != string.Empty && this.PlannedFromTime != string.Empty &&
                this.PlannedToDate == string.Empty && this.PlannedToTime == string.Empty)
            {
                this.AppointmentType = lstDayPart[3].enumDesc;
            }

            if (this.PlannedFromDate != string.Empty && this.PlannedFromTime.ToUpper().IndexOf("M") >= 0)
            {
                if (this.PlannedFromTime.ToUpper() == "AM")
                    this.AppointmentType = lstDayPart[0].enumDesc;
                if (this.PlannedFromTime.ToUpper() == "PM")
                    this.AppointmentType = lstDayPart[1].enumDesc;
            }

            if (this.PlannedFromDate != string.Empty && this.PlannedFromTime == string.Empty &&
                this.PlannedToDate != string.Empty && this.PlannedToTime == string.Empty)
            {
                if (this.AppointmentNeeded == string.Empty)
                    this.AppointmentType = lstDayPart[5].enumDesc;
                if (this.AppointmentNeeded.ToUpper() == "YES")
                    this.AppointmentType = lstDayPart[4].enumDesc;
            }

            if (this.PlannedFromDate != string.Empty && this.PlannedFromTime != string.Empty &&
                this.PlannedToDate != string.Empty && this.PlannedToTime != string.Empty)
            {
                if (this.PlannedFromTime.ToUpper() != "AM" && this.PlannedFromTime.ToUpper() != "PM")
                    this.AppointmentType = lstDayPart[6].enumDesc;
            }

            if (this.AppointmentType.ToLower() == "exact time" || this.AppointmentType.ToLower() == "am" ||
                this.AppointmentType.ToLower() == "pm")
                this.AppointmentNeeded = "YES";
            if (this.AppointmentType.ToLower() == "scheduled date time range")
                this.AppointmentNeeded = string.Empty;
        }

    }
}
