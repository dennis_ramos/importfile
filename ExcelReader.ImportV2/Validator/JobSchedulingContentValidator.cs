﻿using ConXApiRepository;
using ExcelReader.ImportV2.SheetEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReader.ImportV2.Validator
{
    public class JobSchedulingContentValidator
    {
        JobSchedulingData importJobSchedulingData;
        List<ImportJobData> listImportJobData;
        List<string> listOfUserNames;
        private string sheetName = "";

        public JobSchedulingContentValidator(JobSchedulingData pImportJobSchedulingData, List<ImportJobData> pListImportJobData, List<string> pListUserNames)
        {
            importJobSchedulingData = pImportJobSchedulingData;
            listImportJobData = pListImportJobData;
            listOfUserNames = pListUserNames;
            sheetName = SheetName.JobScheduling + ": ";
        }

        public void Validate()
        {
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobSchedulingData.JobRef, "Job Ref", 50, 1));

            if (listImportJobData.FirstOrDefault(ij => ij.JobRef == importJobSchedulingData.JobRef) == null)
            {
                importJobSchedulingData.HasError = true;
                importJobSchedulingData.er.AppendLine(sheetName +"Job Ref '" + importJobSchedulingData.JobRef + "' does not exist in the Job Data Sheet");
            }

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobSchedulingData.PlannedFromDate, "Planned from date", 50, 0));
            CheckDate(importJobSchedulingData.PlannedFromDate, "Planned from date");

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobSchedulingData.PlannedFromTime, "Planned from time", 50, 0));
            CheckTime(importJobSchedulingData.PlannedFromTime, "Planned from time");

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobSchedulingData.PlannedToDate, "Planned to date", 50, 0));
            CheckDate(importJobSchedulingData.PlannedToDate, "Planned to date");

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobSchedulingData.PlannedToTime, "Planned to time", 50, 0));
            CheckTime(importJobSchedulingData.PlannedToTime, "Planned to time");

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobSchedulingData.AppointmentNeeded, "Appointment Needed", 50, 0));

            if (importJobSchedulingData.AppointmentNeeded.Length > 0)
            {
                if (!(importJobSchedulingData.AppointmentNeeded.ToUpper() == "NO" || importJobSchedulingData.AppointmentNeeded.ToUpper() == "YES"))
                {
                    importJobSchedulingData.HasError = true;
                    importJobSchedulingData.er.AppendLine(sheetName +"Appointment Needed value should be yes or no ");
                }
            }

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobSchedulingData.PreferredUsername, "Preferred Username", 50, 0));

            if (importJobSchedulingData.PreferredUsername.Length > 0)
            {
                if (string.IsNullOrWhiteSpace(listOfUserNames.FirstOrDefault(a => a.ToUpper().Trim() == importJobSchedulingData.PreferredUsername.ToUpper())))
                {
                    importJobSchedulingData.HasError = true;
                    importJobSchedulingData.er.AppendLine(sheetName +"Preferred Username '" + importJobSchedulingData.PreferredUsername + "' is not defined in the system ");
                }
            }

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobSchedulingData.Frequency, "Frequency ", 50, 0));

            if (importJobSchedulingData.Frequency.Length > 0)
            {
                int fre = 0;
                if (!int.TryParse(importJobSchedulingData.Frequency, out fre))
                {
                    importJobSchedulingData.HasError = true;
                    importJobSchedulingData.er.AppendLine(sheetName +"Frequency '" + importJobSchedulingData.Frequency + "' could not be converted to an integer");
                }
            }

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobSchedulingData.NumberOfOccurrences, "Number of occurrences ", 50, 0));

            if (importJobSchedulingData.NumberOfOccurrences.Length > 0)
            {
                int fre = 0;
                if (!int.TryParse(importJobSchedulingData.NumberOfOccurrences, out fre))
                {
                    importJobSchedulingData.HasError = true;
                    importJobSchedulingData.er.AppendLine(sheetName +"Number Of Occurrences '" + importJobSchedulingData.NumberOfOccurrences + "' could not be converted to an integer");
                }
            }

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobSchedulingData.AssignToDevice, "Assign to device", 3, 0));

            if (importJobSchedulingData.AssignToDevice.Length > 0)
            {
                if (!(importJobSchedulingData.AssignToDevice.ToUpper() == "NO" || importJobSchedulingData.AssignToDevice.ToUpper() == "YES"))
                {
                    importJobSchedulingData.HasError = true;
                    importJobSchedulingData.er.AppendLine(sheetName +"Assign to device '" + importJobSchedulingData.AssignToDevice + "' should either be 'YES' or 'NO' ");
                }
            }

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobSchedulingData.ContractModel, "Contract Model", 25, 0));
            if (importJobSchedulingData.ContractModel.Length > 0)
            {
                if (!(importJobSchedulingData.ContractModel.ToUpper() == "OUTCOME" || importJobSchedulingData.ContractModel.ToUpper() == "FREQUENCY"))
                {
                    importJobSchedulingData.HasError = true;
                    importJobSchedulingData.er.AppendLine(sheetName +"Contract Model '" + importJobSchedulingData.ContractModel + "' should either be 'OUTCOME' or 'FREQUENCY' ");
                }
                else
                {
                    if (importJobSchedulingData.Frequency.ToIntValue() == 0 || importJobSchedulingData.PlannedToDate.ToDateValue() == null || importJobSchedulingData.PreferredUsername.Length == 0)
                    {
                        importJobSchedulingData.HasError = true;
                        importJobSchedulingData.er.AppendLine(sheetName +"Columns Frequency, Planned To Date and Preferred User must be populated when Contract Model has a value");
                    }
                }

            }

            if (importJobSchedulingData.HasError == false)
                CheckScheduleType();

            importJobSchedulingData.ErrorDescription = importJobSchedulingData.er.ToString();

        }

        private void CheckScheduleType()
        {
            if (importJobSchedulingData.NumberOfOccurrences.ToIntValue() > 0)
            {
                if (importJobSchedulingData.PlannedFromDate.ToDateValue() == null)
                {
                    importJobSchedulingData.HasError = true;
                    importJobSchedulingData.er.AppendLine(sheetName +"Planned From Date should not be empty when number of occurances is greater than 0 ");
                }

                if (importJobSchedulingData.Frequency.ToIntValue() == 0)
                {
                    importJobSchedulingData.HasError = true;
                    importJobSchedulingData.er.AppendLine(sheetName +"Frequency should be populated when number of occurances is greater than 0 ");
                }

            }


            string appointmentNeeded = string.IsNullOrWhiteSpace(importJobSchedulingData.AppointmentNeeded) ? "No" : importJobSchedulingData.AppointmentNeeded.Trim();



            if (!string.IsNullOrWhiteSpace(importJobSchedulingData.PlannedFromDate) && appointmentNeeded.Contains("N"))
            {
                importJobSchedulingData.ScheduleType = "Scheduled For a Day";
            }

            if (!string.IsNullOrWhiteSpace(importJobSchedulingData.PlannedFromDate) && appointmentNeeded.Contains("Y"))
            {
                importJobSchedulingData.ScheduleType = "DAY";
            }

            if (!string.IsNullOrWhiteSpace(importJobSchedulingData.PlannedFromDate) 
                && !string.IsNullOrWhiteSpace(importJobSchedulingData.PlannedToDate) 
                && appointmentNeeded.Contains("N"))
            {
                importJobSchedulingData.ScheduleType = "Scheduled Date Range";
            }


            if (!string.IsNullOrWhiteSpace(importJobSchedulingData.PlannedFromDate)
                && !string.IsNullOrWhiteSpace(importJobSchedulingData.PlannedFromTime)
                && appointmentNeeded.Contains("Y"))
            {
                importJobSchedulingData.ScheduleType = "Exact Time";
            }


            if (!string.IsNullOrWhiteSpace(importJobSchedulingData.PlannedFromDate)
                && !string.IsNullOrWhiteSpace(importJobSchedulingData.PlannedFromTime)
                && !string.IsNullOrWhiteSpace(importJobSchedulingData.PlannedToDate)
                && !string.IsNullOrWhiteSpace(importJobSchedulingData.PlannedToTime)
                && appointmentNeeded.Contains("N"))
            {
                importJobSchedulingData.ScheduleType = "Scheduled Date Time Range";
            }

            if (!string.IsNullOrWhiteSpace(importJobSchedulingData.PlannedFromDate)
                && !string.IsNullOrWhiteSpace(importJobSchedulingData.PlannedFromTime)
                && !string.IsNullOrWhiteSpace(importJobSchedulingData.PlannedToDate)
                && !string.IsNullOrWhiteSpace(importJobSchedulingData.PlannedToTime)
                && appointmentNeeded.Contains("Y"))
            {
                importJobSchedulingData.ScheduleType = "Date Range";
            }


            //if (importJobSchedulingData.PlannedFromDate.ToDateValue() != null &&
            //        (
            //            string.IsNullOrWhiteSpace(importJobSchedulingData.PlannedFromTime)
            //            && string.IsNullOrWhiteSpace(importJobSchedulingData.PlannedToDate)
            //            && string.IsNullOrWhiteSpace(importJobSchedulingData.PlannedToTime)
            //        )
            //    )
            //{
            //    importJobSchedulingData.ScheduleType = "Scheduled For a Day";
            //}


            //if (importJobSchedulingData.PlannedFromDate.ToDateValue() != null && ! string.IsNullOrWhiteSpace(importJobSchedulingData.PlannedToDate)
            //    &&
            //        (
            //            string.IsNullOrWhiteSpace(importJobSchedulingData.PlannedFromTime)
            //            && string.IsNullOrWhiteSpace(importJobSchedulingData.PlannedToTime)
            //        )
            //    )
            //{
            //    importJobSchedulingData.ScheduleType = "Scheduled Date Range";
            //}

        }

        private void Check(string error)
        {
            if (!string.IsNullOrWhiteSpace(error))
            {
                importJobSchedulingData.HasError = true;
                importJobSchedulingData.er.AppendLine(sheetName +error);
            }
        }

        private void CheckTime(string value, string columnName)
        {
            if (value.Length > 0)
            {
                if (value.ToDateValue() == null)
                {
                    importJobSchedulingData.HasError = true;
                    importJobSchedulingData.er.AppendLine(sheetName +"could not convert " + value + " to a Time");
                }
            }
        }
        private void CheckDate(string value, string columnName)
        {
            if (value.Length > 0)
            {
                if (value.Contains("/") || value.Contains("-") || value.Contains(":"))
                {
                    if (value.ToDateValue() == null)
                    {
                        importJobSchedulingData.HasError = true;
                        importJobSchedulingData.er.AppendLine(sheetName +"could not convert " + value + " to a Date");
                    }
                }
                else
                {
                    double db = 0;
                    if (double.TryParse(value, out db))
                    { 
                      value = DateTime.FromOADate(db).ToString("dd/MM/yyyy"); 
                    }
                }
            }
        }

    }

}
