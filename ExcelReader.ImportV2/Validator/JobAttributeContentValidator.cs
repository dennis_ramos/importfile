﻿using ExcelReader.ImportV2.SheetEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReader.ImportV2.Validator
{
    public class JobAttributeContentValidator
    {
        private JobAttributeData importJobAttributeData;
        private List<ImportJobData> listImportJobData;
        private string sheetName = "";

        public JobAttributeContentValidator(JobAttributeData pImportJobAttributeData, List<ImportJobData> pImportJobData)
        {
            listImportJobData = pImportJobData;
            importJobAttributeData = pImportJobAttributeData;
            sheetName = SheetName.JobAttributes + ": " ;
        }

        public void Validate()
        {
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobAttributeData.JobRef, "Job Ref", 50, 1));
            if (listImportJobData.FirstOrDefault(ij => ij.JobRef == importJobAttributeData.JobRef) == null)
            {
                importJobAttributeData.HasError = true;
                importJobAttributeData.er.AppendLine( sheetName + "Job Ref '" + importJobAttributeData.JobRef + "' does not exist in the Job Data Sheet");
            }
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobAttributeData.AttributeType, "Attribute Type", 50, 1));
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobAttributeData.AttributeValue, "Attribute Value", 200, 1));

        }

        private void Check(string error)
        {
            if (!string.IsNullOrWhiteSpace(error))
            {
                importJobAttributeData.HasError = true;
                importJobAttributeData.er.AppendLine(sheetName + error);
            }
        }
    }
}
