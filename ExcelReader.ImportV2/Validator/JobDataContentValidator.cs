﻿using ExcelReader.ImportV2.SheetEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReader.ImportV2.Validator
{
    public class JobDataContentValidator
    {
        ImportJobData importJobData;
        List<string> listJobTypes;
        List<string> listOfContracts;
        private string sheetName = "";

        public JobDataContentValidator(ImportJobData pImportJobData, List<string> plistJobTypes, List<string> plistOfContracts)
        {
            importJobData = pImportJobData;
            listJobTypes = plistJobTypes;
            listOfContracts = plistOfContracts;
            sheetName = SheetName.JobData + ": ";
        }

        public void Validate()
        {
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobData.JobRef, "Job Ref", 50, 1));
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobData.WorkStreamName, "Workstream Name", 50, 1));
            if (importJobData.WorkStreamName.Length > 0 && importJobData.WorkStreamName.Length <= 50)
            {
                if (string.IsNullOrWhiteSpace(listOfContracts.FirstOrDefault(a => a.ToUpper() == importJobData.WorkStreamName.ToUpper())))
                {
                    importJobData.HasError = true;
                    importJobData.er.AppendLine( sheetName + "Workstream Name '" + importJobData.WorkStreamName + "' is not defined in the system ");
                }
            }
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobData.JobDescription, "Job Description", 250, 1));
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobData.Group1, "Group 1 ", 50, 0));
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobData.Group1, "Group 2 ", 50, 0));
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobData.Group3, "Group 3 ", 50, 0));
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobData.ConXJobReference, "Con-X Job Reference", 50, 0));
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobData.JobSeq, "Job Seq", 50, 0));
            if (importJobData.JobSeq.Length > 0)
            {
                int d = 0;
                if (!int.TryParse(importJobData.JobSeq, out d))
                {
                    importJobData.HasError = true;
                    importJobData.er.AppendLine( sheetName + "Job Seq should be a valid integer number");
                }
            }
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobData.JobType, "Job Type", 25, 1));
            if (listJobTypes.IndexOf(importJobData.JobType.ToUpper()) == -1)
            {
                importJobData.HasError = true;
                importJobData.er.AppendLine( sheetName + "Job Type Name '" + importJobData.JobType + "' is not defined in the system ");
            }

            importJobData.ErrorDescription = importJobData.er.ToString();
        }

        public void Check(string error)
        {
            if (!string.IsNullOrWhiteSpace(error))
            {
                importJobData.HasError = true;
                importJobData.er.AppendLine( sheetName + error);
            }
        }
    }
}
