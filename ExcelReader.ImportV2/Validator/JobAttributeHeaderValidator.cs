﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Common;

namespace ExcelReader.ImportV2.Validator
{
    public class JobAttributeHeaderValidator
    {
        public List<string> ValidateJobAttributeColumnHeaders(DbDataReader oleExcelReader )
        {
            List<string> errorList = new List<string>();
            try
            {
                
                #region Parce Column Headers of Job Attributes Sheet

                for (int columnNumber = 0; columnNumber < 3; columnNumber++)
                {

                    string columnName = oleExcelReader.IsDBNull(columnNumber) ? "" : oleExcelReader.GetString(columnNumber);

                    switch (columnNumber)
                    {
                        case 0:
                            if (columnName.Trim().ToUpper() != "Job Ref".ToUpper())
                            {

                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Job Ref", SheetName.JobAttributes));
                            }
                            break;
                        case 1:
                            if (columnName.Trim().ToUpper() != "Attribute type".ToUpper())
                            {

                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Attribute type", SheetName.JobAttributes));
                            }
                            break;
                        case 2:
                            if (columnName.Trim().ToUpper() != "Attribute Value".ToUpper())
                            {

                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Attribute Value", SheetName.JobAttributes));
                            }
                            break;
                    }

                }

                #endregion
            }
            catch (Exception ex)
            {
                errorList.Add("Error Parsing Sheet: Job Attributes Column Headers: Error was" + ex.Message);
            }

            return errorList;
        }
    }
}
