﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReader.ImportV2.Validator
{
    public class JobDataHeaderValidator
    {
        public List<string> ValidateJobDataColumnHeaders(DbDataReader oleExcelReader)
        {
            List<string> errorList = new List<string>();
            try
            {
                #region Parce Column Headers of Job Data Sheet

                for (int columnNumber = 0; columnNumber < 10; columnNumber++)
                {

                    string columnName = "";

                    //if (excelWorksheet.Cells[1, columnNumber].Value != null)
                    //    columnName = ((string)excelWorksheet.Cells[1, columnNumber].Value).Trim();

                    columnName = oleExcelReader.IsDBNull(columnNumber) ? "" : oleExcelReader.GetString(columnNumber);

                    switch (columnNumber)
                    {
                        case 0:
                            if (columnName.Trim().ToUpper() != "FSP".ToUpper())
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "FSP", SheetName.JobData));
                            break;
                        case 1:
                            if (columnName.Trim().ToUpper() != "Job Ref".ToUpper())
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Job Ref", SheetName.JobData));
                            break;
                        case 2:
                            if (columnName.Trim().ToUpper() != "Workstream Name".ToUpper())
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Workstream Name", SheetName.JobData));
                            break;
                        case 3:
                            if (columnName.Trim().ToUpper() != "Job Description".ToUpper())
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Job Description", SheetName.JobData));
                            break;
                        case 4:
                            if (columnName.Trim().ToUpper() != "Group 1".ToUpper())
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Group 1", SheetName.JobData));
                            break;
                        case 5:
                            if (columnName.Trim().ToUpper() != "Group 2".ToUpper())
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Group 2", SheetName.JobData));
                            break;
                        case 6:
                            if (columnName.Trim().ToUpper() != "Group 3".ToUpper())
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Group 3", SheetName.JobData));
                            break;
                        case 7:
                            if (columnName.Trim().ToUpper() != "Con-X Job Reference".ToUpper())
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Con-X Job Reference", SheetName.JobData));
                            break;
                        case 8:
                            if (columnName.Trim().ToUpper() != "Job Seq".ToUpper())
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Job Seq", SheetName.JobData));
                            break;
                        case 9:
                            if (columnName.Trim().ToUpper() != "Job Type".ToUpper())
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Job Type", SheetName.JobData));
                            break;
                    }
                }

                #endregion
            }
            catch (Exception ex)
            {
                errorList.Add("Error Parsing Sheet: Job Data Column Headers: Error was" + ex.Message);
            }

            return errorList;
        }

    }
}
