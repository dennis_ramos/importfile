﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Common;

namespace ExcelReader.ImportV2.Validator
{
    public class JobSchedulingHeaderValidator
    {
        public List<string> ValidateJobSchedulingColumnHeaders(DbDataReader oleExcelReader)
        {
            List<string> errorList = new List<string>();
            try
            {
                #region Parce Column Headers of Job Scheduling Sheet

                for (int columnNumber =0; columnNumber < 10; columnNumber++)
                {

                    string columnName = oleExcelReader.IsDBNull(columnNumber) ? "" : oleExcelReader.GetString(columnNumber);
                    
                    switch (columnNumber)
                    {
                        case 0:
                            if (columnName.Trim().ToUpper() != "Job Ref".ToUpper())
                            {

                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Job Ref", SheetName.JobScheduling));
                            }
                            break;
                        case 1:
                            if (columnName.Trim().ToUpper() != "Planned from date".ToUpper())
                            {

                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Planned from date", SheetName.JobScheduling));
                            }
                            break;
                        case 2:
                            if (columnName.Trim().ToUpper() != "Planned from time".ToUpper())
                            {

                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Planned from time", SheetName.JobScheduling));
                            }
                            break;
                        case 3:
                            if (columnName.Trim().ToUpper() != "Planned to date".ToUpper())
                            {

                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Planned to date", SheetName.JobScheduling));
                            }
                            break;
                        case 4:
                            if (columnName.Trim().ToUpper() != "Planned to time".ToUpper())
                            {

                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Planned to time", SheetName.JobScheduling));
                            }
                            break;
                        case 5:
                            if (columnName.Trim().ToUpper() != "Appointment Needed".ToUpper())
                            {

                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Appointment Needed", SheetName.JobScheduling));
                            }
                            break;
                        case 6:
                            if (columnName.Trim().ToUpper() != "Preferred Username".ToUpper())
                            {

                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Preferred Username", SheetName.JobScheduling));
                            }
                            break;
                        case 7:
                            if (columnName.Trim().ToUpper() != "Frequency".ToUpper())
                            {
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Frequency", SheetName.JobScheduling));

                            }
                            break;
                        case 8:
                            if (columnName.Trim().ToUpper() != "Number of occurrences".ToUpper())
                            {

                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Number of occurrences", SheetName.JobScheduling));

                            }
                            break;
                        case 9:
                            if (columnName.Trim().ToUpper() != "Assign to device".ToUpper())
                            {
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Assign to device", SheetName.JobScheduling));
                            }
                            break;
                        case 10:
                            if (columnName.Trim().ToUpper() != "Contract Model".ToUpper())
                            {
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Contract Model", SheetName.JobScheduling));
                            }
                            break;

                    }

                }

                #endregion
            }
            catch (Exception ex)
            {
                errorList.Add("Error Parsing Sheet: Job Scheduling Column Headers: Error was" + ex.Message);
            }

            return errorList;
        }
    }
}
