﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Common;

namespace ExcelReader.ImportV2.Validator
{
    public class JobTaskItemHeaderValidator
    {
        public List<string> ValidateJobTaskItemColumnHeaders(DbDataReader oleExcelReader)
        {
            List<string> errorList = new List<string>();
            try
            {
                #region Parce Column Headers of Job Task Items Sheet

                for (int columnNumber = 0; columnNumber < 6; columnNumber++)
                {

                    string columnName = oleExcelReader.IsDBNull(columnNumber) ? "" : oleExcelReader.GetString(columnNumber);

                    switch (columnNumber)
                    {
                        case 0:
                            if (columnName.Trim().ToUpper() != "Job Ref".ToUpper())
                            {
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Job Ref", SheetName.JobTaskItems));
                            }
                            break;
                        case 1:
                            if (columnName.Trim().ToUpper() != "Task Name".ToUpper())
                            {
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Task Name", SheetName.JobTaskItems));
                            }
                            break;
                        case 2:
                            if (columnName.Trim().ToUpper() != "Asset Value".ToUpper())
                            {
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Asset Value", SheetName.JobTaskItems));
                            }
                            break;
                        case 3:
                            if (columnName.Trim().ToUpper() != "Job Task Item Order".ToUpper())
                            {
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Job Task Item Order", SheetName.JobTaskItems));
                            }
                            break;
                        case 4:
                            if (columnName.Trim().ToUpper() != "Task item Value".ToUpper())
                            {
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Task item Value", SheetName.JobTaskItems));
                            }
                            break;
                        case 5:
                            if (columnName.Trim().ToUpper() != "Task item Format".ToUpper())
                            {
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Task item Format", SheetName.JobTaskItems));
                            }
                            break;
                    }
                }

                #endregion
            }
            catch (Exception ex)
            {
                errorList.Add("Error Parsing Sheet: Job Task Items Column Headers: Error was" + ex.Message);
            }

            return errorList;
        }
    }
}
