﻿using ConXApiRepository;
using ExcelReader.ImportV2.SheetEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReader.ImportV2.Validator
{
    public class JobNoteContentValidator
    {
        JobNoteData importJobNoteData;
        List<ImportJobData> listImportJobData;
        List<EnumNoteType> listEnumNoteType;
        List<WorkFlowData> listWorkFlowData;
        List<JobAssetData> listJobAssetData;
        private string sheetName = "";

        public JobNoteContentValidator(JobNoteData pImportJobNoteData,
                                       List<ImportJobData> pListImportJobData,
                                       List<EnumNoteType> pListEnumNoteType,
                                       List<WorkFlowData> pListWorkFlowData,
                                       List<JobAssetData> pListJobAssetData)
        {
            importJobNoteData = pImportJobNoteData;
            listImportJobData = pListImportJobData;
            listEnumNoteType = pListEnumNoteType;
            listWorkFlowData = pListWorkFlowData;
            listJobAssetData = pListJobAssetData;
            sheetName = SheetName.JobNotes + ": ";

        }

        public void Validate()
        {

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobNoteData.JobRef, "Job Ref", 50, 1));

            if (listImportJobData.FirstOrDefault(ij => ij.JobRef == importJobNoteData.JobRef) == null)
            {
                importJobNoteData.HasError = true;
                importJobNoteData.er.AppendLine(sheetName + "Job Ref '" + importJobNoteData.JobRef + "' does not exist in the Job Data Sheet");
            }

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobNoteData.NoteType, "Note Type", 50, 1));

            if (importJobNoteData.NoteType.Length > 0)
            {
                if (listEnumNoteType.FirstOrDefault(a => a.enumDesc.ToUpper() == importJobNoteData.NoteType.ToUpper()) == null)
                {
                    importJobNoteData.HasError = true;
                    importJobNoteData.er.AppendLine(sheetName + "Note Type '" + importJobNoteData.NoteType + "' is not defined in the system");
                }
            }

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobNoteData.Note, "Note", 1000, 0));
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobNoteData.NoteTaskName, "Note Task Name", 50, 0));
            if (importJobNoteData.NoteTaskName.Length > 0)
            {
                string ws = "";
                try
                {
                    ws = listImportJobData.FirstOrDefault(a => a.JobRef == importJobNoteData.JobRef).WorkStreamName;
                }
                catch { }

                if (listWorkFlowData.FirstOrDefault(b => b.WorkStream == ws && b.TaskName.Trim().ToUpper() == importJobNoteData.NoteTaskName.Trim().ToUpper()) == null)
                {
                    importJobNoteData.HasError = true;
                    importJobNoteData.er.AppendLine(sheetName + "Note Task Name '" + importJobNoteData.NoteTaskName + "' is not defined in the system");
                }
            }


            Check(JobDataStringValue.ValidateJobDataStringValue(importJobNoteData.NoteAssetValue, "Note Asset Value", 50, 0));

            if (importJobNoteData.NoteAssetValue.Length > 0)
            {

                if (listJobAssetData.FirstOrDefault(a=> a.JobRef == importJobNoteData.JobRef && a.AssetValue == importJobNoteData.NoteAssetValue) == null)
                {
                    importJobNoteData.HasError = true;
                    importJobNoteData.er.AppendLine(sheetName + "Note Asset Value '" + importJobNoteData.NoteAssetValue +
                        "' does not exist for job ref '" + importJobNoteData.JobRef + "'");
                }
            }


            Check(JobDataStringValue.ValidateJobDataStringValue(importJobNoteData.ParentTaskName, "Parent Task Name", 50, 0));
            if (importJobNoteData.ParentTaskName.Length > 0)
            {

                string ws = "";
                try
                {
                    ws = listImportJobData.FirstOrDefault(a => a.JobRef == importJobNoteData.JobRef).WorkStreamName;
                }
                catch { }

                if (listWorkFlowData.FirstOrDefault(b => b.WorkStream == ws && b.TaskName.ToUpper().Trim() == importJobNoteData.ParentTaskName.ToUpper().Trim()) == null)
                {
                    importJobNoteData.HasError = true;
                    importJobNoteData.er.AppendLine(sheetName + "Parent Task Name '" + importJobNoteData.NoteTaskName + "' is not defined in the system");
                }
            }


            Check(JobDataStringValue.ValidateJobDataStringValue(importJobNoteData.ParentAssetValue, "Parent Asset Value", 50, 0));
            if (importJobNoteData.ParentAssetValue.Length > 0)
            {
                if (listJobAssetData.FirstOrDefault(ja => ja.JobRef == importJobNoteData.JobRef && ja.AssetValue == importJobNoteData.ParentAssetValue) == null)
                {
                    importJobNoteData.HasError = true;
                    importJobNoteData.er.AppendLine(sheetName + "Parent Asset Value '" + importJobNoteData.ParentAssetValue +
                        "' does not exist for job ref '" + importJobNoteData.JobRef + "'");
                }
            }


            importJobNoteData.ErrorDescription = importJobNoteData.er.ToString();

        }

        private void Check(string error)
        {
            if (!string.IsNullOrWhiteSpace(error))
            {
                importJobNoteData.HasError = true;
                importJobNoteData.er.AppendLine(sheetName + error);
            }
        }
    }
}
