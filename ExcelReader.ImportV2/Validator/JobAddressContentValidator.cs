﻿using ConXApiRepository;
using ExcelReader.ImportV2.SheetEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReader.ImportV2.Validator
{
    public class JobAddressContentValidator
    {
        JobAddressData importJobAddressData;
        List<EnumRegion> listEnumRegions;
        List<ImportJobData> importJobData;
        string sheetName = "";

        public JobAddressContentValidator(JobAddressData pImportJobAddressData, List<EnumRegion> pListEnumRegions, List<ImportJobData> pImportJobData)
        {
            importJobData = pImportJobData; 
            importJobAddressData = pImportJobAddressData;
            listEnumRegions = pListEnumRegions;
            sheetName = SheetName.JobAddresses + ": ";
        }

        public void Validate()
        {
            

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobAddressData.JobRef, "Job Ref", 50, 1));
            if (importJobData.FirstOrDefault(a => a.JobRef == importJobAddressData.JobRef) == null)
            {
                importJobAddressData.HasError = true;
                importJobAddressData.er.AppendLine("Job Ref '" + importJobAddressData.JobRef + "' does not exist in the Job Data Sheet");
            }
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobAddressData.AddressDescription, "Address description", 250, 1));
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobAddressData.StreetUnit, "Street Unit", 20, 0));
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobAddressData.StreetNumber, "Street Number", 20, 0));
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobAddressData.StreetName, "Street Name", 50, 0));
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobAddressData.Suburb, "Suburb", 50, 0));
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobAddressData.City, "City", 50, 0));
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobAddressData.Region, "Region", 50, 0));

            if (importJobAddressData.Region.Length > 0)
            {
                if (listEnumRegions.FirstOrDefault(a=> a.enumDesc.ToUpper() == importJobAddressData.Region.ToUpper()) == null)
                {
                    importJobAddressData.HasError = true;
                    importJobAddressData.er.AppendLine(sheetName + "Region '" + importJobAddressData.Region + "' is not defined in the system ");
                }
            }

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobAddressData.lat, "lat", 50, 0));
            if (importJobAddressData.lat.Length > 0)
            {
                float lat = 0f;
                if (float.TryParse(importJobAddressData.lat, out lat) == false)
                {
                    importJobAddressData.HasError = true;
                    importJobAddressData.er.AppendLine(sheetName + "Lat '" + importJobAddressData.lat + "' could not be converted to a float");
                }
                else
                {
                    if (lat < -90f || lat > 90f)
                    {
                        importJobAddressData.HasError = true;
                        importJobAddressData.er.AppendLine(sheetName + "Lat '" + importJobAddressData.lat + "' should be between -90 and 90 ");
                    }
                }
            }

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobAddressData.lon, "lon", 50, 0));
            if (importJobAddressData.lon.Length > 0)
            {
                float lon = 0f;
                if (float.TryParse(importJobAddressData.lon, out lon) == false)
                {
                    importJobAddressData.HasError = true;
                    importJobAddressData.er.AppendLine(sheetName + "Lon '" + importJobAddressData.lon + "' could not be converted to a float ");
                }
                else
                {
                    if (lon < -180f || lon > 180f)
                    {
                        importJobAddressData.HasError = true;
                        importJobAddressData.er.AppendLine(sheetName + "Lon '" + importJobAddressData.lon + "' should be between -180 and 180 ");
                    }
                }
            }

            if ((importJobAddressData.lon.Length != 0 && importJobAddressData.lat.Length == 0) ||
                (importJobAddressData.lon.Length == 0 && importJobAddressData.lat.Length != 0))
            {
                importJobAddressData.HasError = true;
                importJobAddressData.er.AppendLine(sheetName + "Lon and Lat columns should be populated if either one of them is populated ");
            }

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobAddressData.isPrimaryAddress, "Is primary address (YES/NO)", 3, 0));
            if (importJobAddressData.isPrimaryAddress.Length > 0)
            {
                if (!(importJobAddressData.isPrimaryAddress.ToUpper() == "NO" ||
                  importJobAddressData.isPrimaryAddress.ToUpper() == "YES"))
                {
                    importJobAddressData.HasError = true;
                    importJobAddressData.er.AppendLine(sheetName + "Is primary address (YES/NO) '" + importJobAddressData.isPrimaryAddress + "' must be Yes or No ");
                }
            }
            else
                importJobAddressData.isPrimaryAddress = "No";

        }

        public void Check(string error)
        {
            if (!string.IsNullOrWhiteSpace(error))
            {
                importJobAddressData.HasError = true;
                importJobAddressData.er.AppendLine(sheetName + error);
            }
        }
    }
}
