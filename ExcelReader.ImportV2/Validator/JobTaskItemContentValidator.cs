﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ConXApiRepository;
using ExcelReader.ImportV2.SheetEntity;

namespace ExcelReader.ImportV2.Validator
{
    public class JobTaskItemContentValidator
    {
        JobTaskItemData importJobTaskItemData;
        List<WorkFlowData> listWorkFlowData;
        List<ImportJobData> listImportJobData;
        List<JobAssetData> listJobAssetData;
        string sheetName = "";

        public JobTaskItemContentValidator(JobTaskItemData pImportJobTaskItemData, List<ImportJobData> pListImportJobData, List<WorkFlowData> pListWorkFlowData, List<JobAssetData> pListJobAssetData)
        {
            importJobTaskItemData = pImportJobTaskItemData;
            listWorkFlowData = pListWorkFlowData;
            listImportJobData = pListImportJobData;
            listJobAssetData = pListJobAssetData;
            sheetName = SheetName.JobTaskItems + ": ";
        }

        public void Validate()
        {
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobTaskItemData.JobRef, "Job Ref", 50, 1));

            if (listImportJobData.FirstOrDefault(ij => ij.JobRef == importJobTaskItemData.JobRef) == null)
            {
                importJobTaskItemData.HasError = true;
                importJobTaskItemData.er.AppendLine(sheetName +"Job Ref '" + importJobTaskItemData.JobRef + "' does not exist in the Job Data Sheet");
            }

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobTaskItemData.TaskName, "Task Name", 50, 1));

            if (importJobTaskItemData.TaskName.Length > 0)
            {
                string ws = "";
                try
                {
                    ws = listImportJobData.FirstOrDefault(a => a.JobRef == importJobTaskItemData.JobRef).WorkStreamName;
                }
                catch { }
                if (listWorkFlowData.FirstOrDefault(b => b.WorkStream == ws && b.TaskName.ToUpper().Trim() == importJobTaskItemData.TaskName.ToUpper().Trim()) == null)
                {
                    importJobTaskItemData.HasError = true;
                    importJobTaskItemData.er.AppendLine(sheetName +"Task Name '" + importJobTaskItemData.TaskName + "' is not defined in the system ");
                }
            }

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobTaskItemData.AssetValue, "Asset Value", 50, 0));
            if (importJobTaskItemData.AssetValue.Length > 0)
            {
                if (listJobAssetData.FirstOrDefault(ja => ja.JobRef == importJobTaskItemData.JobRef && ja.AssetValue == importJobTaskItemData.AssetValue) == null)
                {
                    importJobTaskItemData.HasError = true;
                    importJobTaskItemData.er.AppendLine(sheetName +"Asset Value '" + importJobTaskItemData.AssetValue + "' does not exist for job ref '" + importJobTaskItemData.JobRef + "'");
                }
            }

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobTaskItemData.TaskItemOrder, "Job Task Item Order", 50, 1));
            if (importJobTaskItemData.TaskItemOrder.Length > 0)
            {
                int o = 0;
                if (int.TryParse(importJobTaskItemData.TaskItemOrder, out o) == false)
                {
                    importJobTaskItemData.HasError = true;
                    importJobTaskItemData.er.AppendLine(sheetName +"Task Item Order '" + importJobTaskItemData.TaskItemOrder + "' could not be converted to an integer");
                }
            }

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobTaskItemData.TaskItemValue, "Task item Value", 50, 1));
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobTaskItemData.TaskItemFormat, "Task item format", 50, 0));
            CheckContentAndFormat();
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobTaskItemData.ParentTaskName, "Parent Task Name", 50, 0));

            if (importJobTaskItemData.ParentTaskName.Length > 0)
            {
                string ws = "";
                try
                {
                    ws = listImportJobData.FirstOrDefault(a => a.JobRef == importJobTaskItemData.JobRef).WorkStreamName;
                }
                catch { }

                if (listWorkFlowData.FirstOrDefault(b => b.WorkStream == ws && b.TaskName.ToUpper().Trim() == importJobTaskItemData.ParentTaskName.ToUpper().Trim()) == null)
                {
                    importJobTaskItemData.HasError = true;
                    importJobTaskItemData.er.AppendLine(sheetName +"Parent Task Name '" + importJobTaskItemData.ParentTaskName + "' is not defined in the system");
                }
            }

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobTaskItemData.ParentAssetValue, "Parent Asset Value", 50, 0));

            if (importJobTaskItemData.ParentAssetValue.Length > 0)
            {
                if (listJobAssetData.FirstOrDefault(a => a.JobRef == importJobTaskItemData.JobRef && a.AssetValue == importJobTaskItemData.ParentAssetValue) == null)
                {
                    importJobTaskItemData.HasError = true;
                    importJobTaskItemData.er.AppendLine(sheetName +"Parent Asset Value '" + importJobTaskItemData.ParentAssetValue + "' does not exist or is defined below this row");
                }
            }

            importJobTaskItemData.ErrorDescription = importJobTaskItemData.er.ToString();

        }

        private void CheckContentAndFormat()
        {
            string fieldValue = importJobTaskItemData.TaskItemValue;

            if (importJobTaskItemData.TaskItemFormat.Length == 0)
                return;
            switch (importJobTaskItemData.TaskItemFormat.ToUpper())
            {
                case "STRING":
                    break;
                case "DATE":
                    try
                    {


                        if (fieldValue.ToDateValue() == null)
                        {
                            importJobTaskItemData.HasError = true;
                            importJobTaskItemData.er.AppendLine(sheetName +"Task Item Format is Date but value of '" + fieldValue + "' could not be converted to Date");
                        }
                        else
                        {
                            importJobTaskItemData.TaskItemValue = fieldValue;
                        }

                    }
                    catch
                    {
                        importJobTaskItemData.HasError = true;
                        importJobTaskItemData.er.AppendLine(sheetName +"Task Item Format is Date but value of '" + fieldValue + "' could not be converted to Date");
                    }

                    break;
                case "INTEGER":
                    try
                    {

                        int outInt = 0;
                        if (int.TryParse(fieldValue, out outInt) == false)
                        {
                            importJobTaskItemData.HasError = true;
                            importJobTaskItemData.er.AppendLine(sheetName +"Task Item Format is Integer but value of '" + fieldValue + "' could not be converted to Integer");
                        }
                        else
                        {
                            importJobTaskItemData.TaskItemValue = fieldValue;
                        }

                    }
                    catch
                    {
                        importJobTaskItemData.HasError = true;
                        importJobTaskItemData.er.AppendLine(sheetName +"Task Item Format is Integer but value of '" + fieldValue + "' could not be converted to Integer");
                    }

                    break;
                case "FLOAT":
                    try
                    {

                        float outfloat = 0;
                        if (float.TryParse(fieldValue, out outfloat) == false)
                        {
                            importJobTaskItemData.HasError = true;
                            importJobTaskItemData.er.AppendLine(sheetName +"Task Item Format is float but value of '" + fieldValue + "' could not be converted to float");
                        }
                        else
                        {
                            importJobTaskItemData.TaskItemValue = fieldValue;
                        }

                    }
                    catch
                    {
                        importJobTaskItemData.HasError = true;
                        importJobTaskItemData.er.AppendLine(sheetName +"Task Item Format is float but value of '" + fieldValue + "' could not be converted to float");
                    }
                    break;
                case "BOOLEAN":

                    if (!(importJobTaskItemData.TaskItemValue.ToUpper() == "NO" || importJobTaskItemData.TaskItemValue.ToUpper() == "YES"))
                    {
                        importJobTaskItemData.HasError = true;
                        importJobTaskItemData.er.AppendLine(sheetName +"Task Item Format is Boolean but value of '" + fieldValue + "' must be Yes or No ");
                    }
                    break;
                default:
                    importJobTaskItemData.HasError = true;
                    importJobTaskItemData.er.AppendLine(sheetName +"Task Item Format '" + importJobTaskItemData.TaskItemFormat + "' should be one of the following (String, Date, integer, float, boolean)");
                    break;
            }
        }

        private void Check(string error)
        {
            if (!string.IsNullOrWhiteSpace(error))
            {
                importJobTaskItemData.HasError = true;
                importJobTaskItemData.er.AppendLine(sheetName +error);
            }
        }
    }
}
