﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Common;


namespace ExcelReader.ImportV2.Validator
{
    public class JobContactHeaderValidator
    {
        public List<string> ValidateJobContactColumnHeaders(DbDataReader oleExcelReader)
        {
            List<string> errorList = new List<string>();
            try
            {
                #region Parce Column Headers of Job Contacts Sheet

                for (int columnNumber = 0; columnNumber < 6; columnNumber++)
                {

                    string columnName = oleExcelReader.IsDBNull(columnNumber) ? "" : oleExcelReader.GetString(columnNumber);
                    
                    switch (columnNumber)
                    {
                        case 0:
                            if (columnName.Trim().ToUpper() != "Job Ref".ToUpper())
                            {
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Job Ref", SheetName.JobContacts));
                            }
                            break;
                        case 1:
                            if (columnName.Trim().ToUpper() != "Contact Description".ToUpper())
                            {
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Contact Description", SheetName.JobContacts));
                            }
                            break;
                        case 2:
                            if (columnName.Trim().ToUpper() != "First Name".ToUpper())
                            {
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "First Name", SheetName.JobContacts));
                            }
                            break;
                        case 3:
                            if (columnName.Trim().ToUpper() != "Last name".ToUpper())
                            {
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Last name", SheetName.JobContacts));
                            }
                            break;
                        case 4:
                            if (columnName.Trim().ToUpper() != "contact type".ToUpper())
                            {
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "contact type", SheetName.JobContacts));
                            }
                            break;
                        case 5:
                            if (columnName.Trim().ToUpper() != "contact value".ToUpper())
                            {
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "contact value", SheetName.JobContacts));
                            }
                            break;
                    }

                }

                #endregion
            }
            catch (Exception ex)
            {
                errorList.Add("Error Parsing Sheet: Job Contacts Column Headers: Error was" + ex.Message);
            }

            return errorList;
        }
    }
}
