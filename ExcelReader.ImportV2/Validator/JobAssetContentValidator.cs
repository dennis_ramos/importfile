﻿using ExcelReader.ImportV2.SheetEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReader.ImportV2.Validator
{
    public class JobAssetContentValidator
    {
        JobAssetData importJobAssetData;
        List<WorkFlowData> listWorkFlowData;
        List<ImportJobData> importJobData;
        List<AssetTypes> listAssetTypes;
        string sheetName = "";

        public JobAssetContentValidator(JobAssetData pImportJobAssetData, List<WorkFlowData> pListWorkFlowData, List<ImportJobData> pImportJobData, List<AssetTypes> pListAssetTypes)
        {
            importJobAssetData = pImportJobAssetData;
            listWorkFlowData = pListWorkFlowData;
            importJobData = pImportJobData;
            listAssetTypes = pListAssetTypes;
            sheetName = SheetName.JobAssets + ": ";
        }

        public void Validate()
        {
            

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobAssetData.JobRef, "Job Ref", 50, 1));
            if (importJobData.FirstOrDefault(a => a.JobRef == importJobAssetData.JobRef) == null)
            {
                importJobAssetData.HasError = true;
                importJobAssetData.er.AppendLine(sheetName +"Job Ref '" + importJobAssetData.JobRef + "' does not exist in the Job Data Sheet");
            }

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobAssetData.AssetType, "Asset Type", 50, 1));
            if (importJobAssetData.AssetType.Length > 0)
            {
                if (listAssetTypes.FirstOrDefault(a => a.enumDesc.ToUpper().Trim() == importJobAssetData.AssetType.ToUpper().Trim() || a.enumSDesc.ToUpper().Trim() == importJobAssetData.AssetType.ToUpper().Trim()) == null)
                {
                    importJobAssetData.HasError = true;
                    importJobAssetData.er.AppendLine(sheetName +"Asset Type '" + importJobAssetData.AssetType + "' is not defined in the system");
                }
            }
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobAssetData.AssetValue, "Asset Value", 50, 1));
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobAssetData.AssetAttribute, "Asset Attribute", 50, 0));
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobAssetData.AssetAttributeValue, "Asset Attribute Value", 200, 0));

            if (importJobAssetData.AssetAttribute.Length > 0 && importJobAssetData.AssetAttributeValue.Length == 0)
            {
                importJobAssetData.HasError = true;
                importJobAssetData.er.AppendLine(sheetName +"Asset Attribute Value Value must be provided when Asset Attribute is supplied");
            }

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobAssetData.NewTaskNameInWorkflow, "New Task Name in workflow", 50, 0));
            
            if (importJobAssetData.NewTaskNameInWorkflow.Length > 0)
            {
                string ws = "";
                try
                {
                    ws = importJobData.FirstOrDefault(a => a.JobRef == importJobAssetData.JobRef).WorkStreamName;
                }
                catch { }

                if (listWorkFlowData.FirstOrDefault(b => b.WorkStream == ws && b.TaskName.ToUpper().Trim() == importJobAssetData.NewTaskNameInWorkflow.ToUpper().Trim()) == null)
                {
                    importJobAssetData.HasError = true;
                    importJobAssetData.er.AppendLine(sheetName +"New Task Name in workflow '" + importJobAssetData.NewTaskNameInWorkflow + "' is not defined in the system");
                }
            }


            Check(JobDataStringValue.ValidateJobDataStringValue(importJobAssetData.ParentTaskName, "Parent Task Name", 50, 0));
            if (importJobAssetData.ParentTaskName.Length > 0)
            {
                string ws = "";
                try
                {
                    ws = importJobData.FirstOrDefault(a => a.JobRef == importJobAssetData.JobRef).WorkStreamName;
                }
                catch { }
                WorkFlowData wfd = listWorkFlowData.FirstOrDefault(b => b.WorkStream == ws && b.TaskName.Trim().ToUpper() == importJobAssetData.ParentTaskName.Trim().ToUpper());

                if (wfd == null)
                {
                    importJobAssetData.HasError = true;
                    importJobAssetData.er.AppendLine(sheetName +"Parent Task Name '" + importJobAssetData.ParentTaskName + "' is not defined in the system");
                }
            }

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobAssetData.ParentAssetValue, "Parent Asset Value", 50, 0));
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobAssetData.AppendAssetValueToNewTask, "Append asset value to new task", 3, 0));
            if (importJobAssetData.AppendAssetValueToNewTask.Length > 0)
            {
                if (!(importJobAssetData.AppendAssetValueToNewTask.ToUpper() == "NO" ||
                  importJobAssetData.AppendAssetValueToNewTask.ToUpper() == "YES"))
                {
                    importJobAssetData.HasError = true;
                    importJobAssetData.er.AppendLine(sheetName +"Append AssetValue To New Task '" + importJobAssetData.AppendAssetValueToNewTask + "' must be Yes or No");
                }
            }
            else
                importJobAssetData.AppendAssetValueToNewTask = "No";



        }
        public void Check(string error)
        {
            if (!string.IsNullOrWhiteSpace(error))
            {
                importJobAssetData.HasError = true;
                importJobAssetData.er.AppendLine(sheetName +error);
            }
        }
    }
}
