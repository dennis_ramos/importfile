﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Common;

namespace ExcelReader.ImportV2.Validator
{
    public class JobAssetHeaderValidator
    {
        public List<string> ValidateJobAssetColumnHeaders(DbDataReader oleExcelReader)
        {
            List<string> errorList = new List<string>();
            try
            {
                #region Parce Column Headers of Job Assets Sheet

                for (int columnNumber = 0; columnNumber < 9; columnNumber++)
                {

                    string columnName = oleExcelReader.IsDBNull(columnNumber) ? "" : oleExcelReader.GetString(columnNumber);

                    switch (columnNumber)
                    {
                        case 0:
                            if (columnName.Trim().ToUpper() != "Job Ref".ToUpper())
                            {

                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Job Ref", SheetName.JobAssets));
                            }
                            break;
                        case 1:
                            if (columnName.Trim().ToUpper() != "Asset Type".ToUpper())
                            {

                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Asset Type", SheetName.JobAssets));
                            }
                            break;
                        case 2:
                            if (columnName.Trim().ToUpper() != "Asset Value".ToUpper())
                            {

                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Asset Value", SheetName.JobAssets));
                            }
                            break;
                        case 3:
                            if (columnName.Trim().ToUpper() != "Asset Attribute".ToUpper())
                            {

                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Asset Attribute", SheetName.JobAssets));
                            }
                            break;
                        case 4:
                            if (columnName.Trim().ToUpper() != "Asset Attribute Value".ToUpper())
                            {
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Asset Attribute Value", SheetName.JobAssets));

                            }
                            break;
                        case 5:
                            if (columnName.Trim().ToUpper() != "New Task Name in workflow".ToUpper())
                            {

                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "New Task Name in workflow", SheetName.JobAssets));

                            }
                            break;
                        case 6:
                            if (columnName.Trim().ToUpper() != "Parent Task Name".ToUpper())
                            {
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Parent Task Name", SheetName.JobAssets));
                            }
                            break;
                        case 7:
                            if (columnName.Trim().ToUpper() != "Parent Asset Value".ToUpper())
                            {

                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Parent Asset Value", SheetName.JobAssets));
                            }
                            break;
                        case 8:
                            if (columnName.Trim().ToUpper() != "Append asset value to new task".ToUpper())
                            {

                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Append asset value to new task", SheetName.JobAssets));
                            }
                            break;
                    }

                }

                #endregion
            }
            catch (Exception ex)
            {
                errorList.Add("Error Parsing Sheet: Job Assets Column Headers: Error was" + ex.Message);
            }

            return errorList;
        }
    }
}
