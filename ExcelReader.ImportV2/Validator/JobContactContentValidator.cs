﻿using ExcelReader.ImportV2.SheetEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReader.ImportV2.Validator
{
    public class JobContactContentValidator
    {
        private JobContactData importJobContactData;
        private List<ImportJobData> listImportJobData;
        private List<EnumPhoneType> listEnumPhoneType;
        string sheetName = "";
        public JobContactContentValidator(JobContactData pImportJobContactData, List<ImportJobData> pListImportJobData, List<EnumPhoneType> pListEnumPhoneType)
        {
            importJobContactData = pImportJobContactData;
            listImportJobData = pListImportJobData;
            listEnumPhoneType = pListEnumPhoneType;
            sheetName = SheetName.JobContacts + ": ";
        }

        public void Validate()
        {
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobContactData.JobRef, "Job Ref", 50, 1));

            if (listImportJobData.FirstOrDefault(ij => ij.JobRef == importJobContactData.JobRef) == null)
            {
                importJobContactData.HasError = true;
                importJobContactData.er.AppendLine("Job Ref '" + importJobContactData.JobRef + "' does not exist in the Job Data Sheet");
            }
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobContactData.ContactDescription, "Contact Description", 250, 1));
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobContactData.FirstName, "First Name", 50, 0));
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobContactData.LastName, "Last name", 50, 0));
            Check(JobDataStringValue.ValidateJobDataStringValue(importJobContactData.ContactType, "contact type ", 50, 0));
            
            if (importJobContactData.ContactType.Length > 0)
            {
                if ( listEnumPhoneType.FirstOrDefault(a=> a.enumDesc.ToUpper() == importJobContactData.ContactType.ToUpper()) == null)
                {
                    importJobContactData.HasError = true;
                    importJobContactData.er.AppendLine(sheetName +  "Contact Type '" + importJobContactData.ContactType + "' is not defined in the system ");
                }
            }

            Check(JobDataStringValue.ValidateJobDataStringValue(importJobContactData.ContactValue, "contact value", 50, 0));
            if (importJobContactData.ContactType.Length > 0 && importJobContactData.ContactValue.Length == 0)
            {
                importJobContactData.HasError = true;
                importJobContactData.er.AppendLine(sheetName + "Contact Value must be provided when Contact Type is supplied");
            }


        }

        private void Check(string error)
        {
            if (!string.IsNullOrWhiteSpace(error))
            {
                importJobContactData.HasError = true;
                importJobContactData.er.AppendLine(sheetName + error);
            }
        }
    }
}
