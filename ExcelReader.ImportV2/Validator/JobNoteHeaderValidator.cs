﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Common;


namespace ExcelReader.ImportV2.Validator
{
    public class JobNoteHeaderValidator
    {
        public List<string> ValidateJobNoteColumnHeaders(DbDataReader oleExcelReader)
        {
            List<string> errorList = new List<string>();

            try
            {
                #region Parce Column Headers of Job Notes Sheet

                for (int columnNumber = 0; columnNumber < 3; columnNumber++)
                {

                    string columnName = oleExcelReader.IsDBNull(columnNumber) ? "" : oleExcelReader.GetString(columnNumber);
                    
                    switch (columnNumber)
                    {
                        case 0:
                            if (columnName.Trim().ToUpper() != "Job Ref".ToUpper())
                            {

                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Job Ref", SheetName.JobNotes));
                            }
                            break;
                        case 1:
                            if (columnName.Trim().ToUpper() != "Note Type".ToUpper())
                            {

                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Note Type", SheetName.JobNotes));
                            }
                            break;
                        case 2:
                            if (columnName.Trim().ToUpper() != "Note".ToUpper())
                            {

                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Note", SheetName.JobNotes));
                            }
                            break;
                    }

                }

                #endregion
            }
            catch (Exception ex)
            {
                errorList.Add("Error Parsing Sheet: Job Notes Column Headers: Error was" + ex.Message);
            }

            return errorList;
        }
    }
}
