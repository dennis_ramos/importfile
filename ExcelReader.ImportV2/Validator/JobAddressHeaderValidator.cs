﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReader.ImportV2.Validator
{
    public class JobAddressHeaderValidator
    {
        public List<string> ValidateJobAddressColumnHeaders(DbDataReader oleExcelReader)
        {
            List<string> errorList = new List<string>();
            try
            {
                #region Parce Column Headers of Job Addresses Sheet

                for (int columnNumber = 0; columnNumber < 11; columnNumber++)
                {
                    string columnName = oleExcelReader.IsDBNull(columnNumber) ? "" : oleExcelReader.GetString(columnNumber);
                    switch (columnNumber)
                    {
                        case 0:
                            
                            if (columnName.Trim().ToUpper() != "Job Ref".ToUpper())
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Job Ref", SheetName.JobAddresses));

                            break;
                        case 1:
                            if (columnName.Trim().ToUpper() != "Address description".ToUpper())
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Address description", SheetName.JobAddresses));

                            break;
                        case 2:
                            if (columnName.Trim().ToUpper() != "Street Unit".ToUpper())
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Street Unit", SheetName.JobAddresses));

                            break;
                        case 3:
                            if (columnName.Trim().ToUpper() != "Street Number".ToUpper())
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Street Number", SheetName.JobAddresses));

                            break;
                        case 4:
                            if (columnName.Trim().ToUpper() != "Street Name".ToUpper())
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Street Name", SheetName.JobAddresses));

                            break;
                        case 5:
                            if (columnName.Trim().ToUpper() != "Suburb".ToUpper())
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Suburb", SheetName.JobAddresses));

                            break;
                        case 6:
                            if (columnName.Trim().ToUpper() != "City".ToUpper())
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "City", SheetName.JobAddresses));

                            break;
                        case 7:
                            if (columnName.Trim().ToUpper() != "Region".ToUpper())
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "Region", SheetName.JobAddresses));

                            break;
                        case 8:
                            if (columnName.Trim().ToUpper() != "lat".ToUpper())
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "lat", SheetName.JobAddresses));

                            break;
                        case 9:
                            if (columnName.Trim().ToUpper() != "lon".ToUpper())
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), "lon", SheetName.JobAddresses));

                            break;
                        case 10:
                            if (columnName.Trim().ToUpper() != @"Is primary address (YES/NO)".ToUpper())
                                errorList.Add(HeaderErrorString.errorHeader((columnNumber+1).ToString(), @"Is primary address (YES/NO)", SheetName.JobAddresses));

                            break;
                    }

                }

                #endregion
            }
            catch (Exception ex)
            {
                errorList.Add("Error Parsing Sheet: Job Addresses Column Headers: Error was" + ex.Message);
            }

            return errorList;
        }

    }
}
