﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReader.ImportV2.Validator
{
    public class JobDataStringValue
    {
        public static string ValidateJobDataStringValue(string value, string columnName, int maxLength, int minLength)
        {
            StringBuilder error = new StringBuilder();

            if (value.Length > maxLength)
            {
                error.AppendLine(columnName + " length is greater than " + maxLength.ToString());
            }

            if (value.Length < minLength)
            {
                error.AppendLine(columnName + " length is less than " + minLength.ToString());
            }

            if (error.ToString().Length > 0)
                return error.ToString();
            else
                return "";
        }
    }
}
