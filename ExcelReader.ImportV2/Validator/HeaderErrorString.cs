﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReader.ImportV2.Validator
{
    public class HeaderErrorString
    {
        public static string errorHeader(string columnNumber, string shoulBeColumnName, string SheetName)
        {
            return "Sheet: " + SheetName + " Column header for column number " + columnNumber + " Should be " + shoulBeColumnName;
        }
    }
}
