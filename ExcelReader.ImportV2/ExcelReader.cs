﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExcelReader.ImportV2.SheetEntity;
using ConXApiRepository;
using System.IO;
using System.Data.Entity;
using log4net;
using System.Reflection;

namespace ExcelReader.ImportV2
{
    public class ExcelReader
    {

        List<string> listOfContracts;
        List<string> listOfUserNames;
        List<string> listAppointmentTypes;
        List<string> listContractModel;
        List<AssetTypes> listAssetTypes;
        List<EnumDayPart> listAppointmentType;
        List<EnumPhoneType> listEnumPhoneType;
        List<EnumRegion> listEnumRegions;
        List<EnumNoteType> listEnumNoteType;
        List<string> listJobTypes;
        List<WorkFlowData> listWorkFlowData;

        private ExcelV2Content _excelV2Content;
        private string _fileName;
        private string _fsp;
        private string _connectionString;
        private List<string> _errorList;

        ConXApiRepository.ConXApiModels _db;

        private StringBuilder _sb;
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public ExcelReader(ExcelV2Content excelV2Content, string fileName, List<string> errorList, StringBuilder sb, string fsp)
        {
            _fsp = fsp;
            _sb = sb;

            _errorList = errorList;
            _excelV2Content = excelV2Content;
            _fileName = fileName;
            _connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + _fileName + ";Extended Properties=\"Excel 12.0;HDR=No;IMEX=1\"";
            _db = new ConXApiRepository.ConXApiModels();
            _db.Configuration.LazyLoadingEnabled = true;
            _db.Configuration.ProxyCreationEnabled = true;

            listOfContracts = new List<string>();
            listOfUserNames = new List<string>();

            listAppointmentTypes = new List<string>();
            listContractModel = new List<string>();
            listAssetTypes = new List<AssetTypes>(); ;
            listAppointmentType = new List<EnumDayPart>(); ;
            listEnumPhoneType = new List<EnumPhoneType>();
            listEnumRegions = new List<EnumRegion>();
            listEnumNoteType = new List<EnumNoteType>();
            listJobTypes = new List<string>();
            List<WorkFlowData> listWorkFlowData = new List<WorkFlowData>();


        }

        public bool MandatorySheetCheck(List<string> sheetNameList)
        {

            bool jobSheetFound = !string.IsNullOrWhiteSpace(sheetNameList.FirstOrDefault(a => a.ToUpper().Contains("JOB DATA")));
            bool jobAddSheetFound = !string.IsNullOrWhiteSpace(sheetNameList.Find(a => a.ToUpper().Contains("JOB ADDRESSES")));
            bool jobContactsSheetFound = !string.IsNullOrWhiteSpace(sheetNameList.Find(a => a.ToUpper().Contains("JOB CONTACTS")));

            return (jobSheetFound && jobAddSheetFound && jobContactsSheetFound);
        }

        public void PopulateList()
        {

            try
            {
                listOfContracts.AddRange(_db.cfgContracts.Where(a => a.FSP.Trim().ToUpper() == _fsp.Trim().ToUpper() && a.wadtDeleted == false).Select(b => b.ContractName).ToList());
                listOfUserNames.AddRange(_db.cfgUsers.Where(a => a.FSP.Trim().ToUpper() == _fsp.Trim().ToUpper() && a.wadtDeleted == false).Select(b => b.UserName).ToList());
                listJobTypes.AddRange(_db.enumJobTypes.Where(a => a.FSP.Trim().ToUpper() == _fsp.Trim().ToUpper() && a.wadtDeleted == false).Select(b => b.enumDesc.ToUpper()).ToList());

                listAppointmentTypes.AddRange(_db.enumDayParts.Select(b => b.enumDesc).ToList());
                listContractModel.Add("Outcome");
                listContractModel.Add("Frequency");
                listAssetTypes.AddRange(_db.enumAssetTypes.Where(a => a.fsp.Trim().ToUpper() == _fsp.Trim().ToUpper() && a.wadtDeleted == false)
                                                            .Select(b => new AssetTypes { enumAssetType = b.enumAssetType, enumDesc = b.enumDesc, enumSDesc = b.enumSDesc }).ToList());

                listEnumPhoneType = new List<EnumPhoneType>();
                listEnumPhoneType.Add(new EnumPhoneType(0f, "Email"));
                listEnumPhoneType.Add(new EnumPhoneType(1f, "Home"));
                listEnumPhoneType.Add(new EnumPhoneType(2f, "Work"));
                listEnumPhoneType.Add(new EnumPhoneType(3f, "Mobile"));
                listEnumPhoneType.Add(new EnumPhoneType(4f, "Fax"));
                listEnumRegions = _db.enumRegions.Where(a => a.FSP.Trim().ToUpper() == _fsp.Trim().ToUpper() && a.wadtDeleted == false).ToList();
                listEnumNoteType = _db.enumNoteTypes.ToList();
            }
            catch (Exception ex)
            {
                LogException(ex);
                _errorList.Add("Error populating lookups");
                _errorList.Add("Exception: " + ex.Message);
                if (ex.InnerException != null)
                    _errorList.Add("Inner Exception: " + ex.InnerException.Message);
            }

        }

        private void LogException(Exception ex)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Exception Caught:");
            sb.Append("Exception = " + ex.Message);
            if (ex.InnerException != null)
                sb.AppendLine("Inner Exception = " + ex.InnerException.Message);
            sb.AppendLine("Stack Trace: " + ex.StackTrace);
            _log.Error(sb.ToString());
        }

        private void PopulateWorkFlowDataList()
        {
            try
            {
                listWorkFlowData = new List<WorkFlowData>();
                IEnumerable<string> workStreamNames = _excelV2Content.ImportJobDataList.Select(x => x.WorkStreamName).Distinct();

                foreach (string ws in workStreamNames)
                {
                    CfgContract ct = _db.cfgContracts.FirstOrDefault(a => a.ContractName == ws && a.FSP == _fsp && a.wadtDeleted == false);
                    if (ct != null)
                    {
                        WorkFlow wf = _db.cfgWorkFlows.Include(b => b.WorkFlowTasks).FirstOrDefault(a => a.WorkFlowID == ct.WorkFlowID && a.wadtDeleted == false);

                        if (wf != null)
                        {
                            List<WorkFlowTask> parentTaskList = wf.WorkFlowTasks.Where(t => t.ParentTaskID == null && t.wadtDeleted == false).ToList();
                            foreach (WorkFlowTask task in parentTaskList)
                            {
                                listWorkFlowData.Add(new WorkFlowData(ws, wf.WorkFlowName, task.TaskName));
                            }

                            parentTaskList = wf.WorkFlowTasks.Where(t => t.ParentTaskID != null && t.wadtDeleted == false).ToList();
                            foreach (WorkFlowTask task in parentTaskList)
                            {
                                listWorkFlowData.Add(new WorkFlowData(ws, wf.WorkFlowName, task.TaskName, task.ParentWorkFlowTask.TaskName));
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
                _errorList.Add("Error populating Workflow Table lookups");
            }
        }

        public async Task ReadExcelFile()
        {
            WriteProcessLog("Reading File " + _fileName);

            PopulateList();
            if (_errorList.Count > 0) return;

            if (!File.Exists(_fileName))
            {
                _errorList.Add("File " + _fileName + " cannot be found or access denied ");
                return;
            }

            List<string> sheetNameList = new List<string>();
            try
            {
                _log.Info("Opening OleDbConnection: " + _connectionString);
                using (OleDbConnection oleExcelConnection = new OleDbConnection(_connectionString))
                {

                    await oleExcelConnection.OpenAsync();

                    DataTable dtTablesList = default(DataTable);
                    dtTablesList = oleExcelConnection.GetSchema("Tables");

                    if (dtTablesList.Rows.Count > 0)
                    {
                        for (var counter = 0; counter < dtTablesList.Rows.Count; counter++)
                        {
                            sheetNameList.Add(dtTablesList.Rows[counter]["TABLE_NAME"].ToString());
                        }
                    }
                    dtTablesList.Dispose();

                    if (!MandatorySheetCheck(sheetNameList))
                    {
                        _errorList.Add("Mandatory Sheets was not found in the file :" + _fileName);
                        return;
                    }
                    else
                    {
                        //Made the process of the Job Sheet not multi treaded as we need this done before the rest are processed
                        ProcessJobDataSheet(oleExcelConnection);
                        PopulateWorkFlowDataList();
                        if (_errorList.Count > 0) return;


                        //Mandatory Sheets
                        await ProcessJobContactsSheet(oleExcelConnection);
                        await ProcessJobAddressesSheet(oleExcelConnection);

                        //Optional Sheets
                        if (!string.IsNullOrWhiteSpace(sheetNameList.FirstOrDefault(a => a.ToUpper().Contains("JOB ASSETS"))))
                            await ProcessJobAssetSheet(oleExcelConnection);

                        if (!string.IsNullOrWhiteSpace(sheetNameList.FirstOrDefault(a => a.ToUpper().Contains("JOB NOTES"))))
                            await ProcessJobNoteSheet(oleExcelConnection);

                        if (!string.IsNullOrWhiteSpace(sheetNameList.FirstOrDefault(a => a.ToUpper().Contains("JOB ATTRIBUTES"))))
                            await ProcessJobAttributeSheet(oleExcelConnection);

                        if (!string.IsNullOrWhiteSpace(sheetNameList.FirstOrDefault(a => a.ToUpper().Contains("JOB SCHEDULING"))))
                            await ProcessJobSchedulingSheet(oleExcelConnection);

                        if (!string.IsNullOrWhiteSpace(sheetNameList.FirstOrDefault(a => a.ToUpper().Contains("JOB TASK ITEMS"))))
                            await ProcessJobTaskItemSheet(oleExcelConnection);

                    }
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
                _errorList.Add("Exception : " + ex.Message);
                if (ex.InnerException != null)
                    _errorList.Add("Inner Exception :" + ex.InnerException.Message);

                _errorList.Add("Exception Stack Trace: " + ex.StackTrace);

            }
        }

        private void WriteProcessLog(string logEvent)
        {
            _sb.AppendLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + logEvent);
            _log.Info(logEvent);
        }

        private void ProcessJobDataSheet(OleDbConnection oleExcelConnection)
        {
            WriteProcessLog("Processing Job Data Sheet");
            using (OleDbCommand oleExcelCommand = new OleDbCommand("Select * From [Job Data$]", oleExcelConnection))
            {
                ReadJobDataSheet(oleExcelCommand);
            }
        }

        private async Task ProcessJobContactsSheet(OleDbConnection oleExcelConnection)
        {
            WriteProcessLog("Processing Job Contacts Sheet");
            using (OleDbCommand oleExcelCommand = new OleDbCommand("Select * From [Job Contacts$]", oleExcelConnection))
            {
                await ReadJobContactSheet(oleExcelCommand);
            }
        }

        private async Task ProcessJobAddressesSheet(OleDbConnection oleExcelConnection)
        {
            WriteProcessLog("Processing Job Addresses Sheet");

            using (OleDbCommand oleExcelCommand = new OleDbCommand("Select * From [Job Addresses$]", oleExcelConnection))
            {
                await ReadAddressSheet(oleExcelCommand);
            }
        }

        private async Task ProcessJobAssetSheet(OleDbConnection oleExcelConnection)
        {
            WriteProcessLog("Processing Job Assets Sheet");
            using (OleDbCommand oleExcelCommand = new OleDbCommand("Select * From [Job Assets$]", oleExcelConnection))
            {
                await ReadJobAssetSheet(oleExcelCommand);
            }
        }

        private async Task ProcessJobTaskItemSheet(OleDbConnection oleExcelConnection)
        {
            WriteProcessLog("Processing Job Task Items Sheet");
            using (OleDbCommand oleExcelCommand = new OleDbCommand("Select * From [Job Task Items$]", oleExcelConnection))
            {
                await ReadJobTaskItemSheet(oleExcelCommand);
            }
        }

        private async Task ProcessJobNoteSheet(OleDbConnection oleExcelConnection)
        {
            WriteProcessLog("Processing Job Notes Sheet");

            using (OleDbCommand oleExcelCommand = new OleDbCommand("Select * From [Job Notes$]", oleExcelConnection))
            {
                await ReadJobNoteSheet(oleExcelCommand);
            }
        }

        private async Task ProcessJobAttributeSheet(OleDbConnection oleExcelConnection)
        {
            WriteProcessLog("Processing Job Attributes Sheet");
            using (OleDbCommand oleExcelCommand = new OleDbCommand("Select * From [Job Attributes$]", oleExcelConnection))
            {
                await ReadJobAttributeSheet(oleExcelCommand);
            }
        }

        private async Task ProcessJobSchedulingSheet(OleDbConnection oleExcelConnection)
        {
            WriteProcessLog("Processing Job Scheduling Sheet");

            using (OleDbCommand oleExcelCommand = new OleDbCommand("Select * From [Job Scheduling$]", oleExcelConnection))
            {
                await ReadJobSchedulingSheet(oleExcelCommand);
            }
        }

        private void ReadJobDataSheet(OleDbCommand oleExcelCommand)
        {
            using (DbDataReader reader = oleExcelCommand.ExecuteReader())
            {
                while (reader.Read())
                {
                    GetJobData(reader);
                }

            }
        }

        private async Task ReadAddressSheet(OleDbCommand oleExcelCommand)
        {
            using (DbDataReader reader = await oleExcelCommand.ExecuteReaderAsync())
            {
                while (await reader.ReadAsync())
                {
                    GetAddressData(reader);
                }

            }
        }

        private async Task ReadJobContactSheet(OleDbCommand oleExcelCommand)
        {
            using (DbDataReader reader = await oleExcelCommand.ExecuteReaderAsync())
            {
                while (await reader.ReadAsync())
                {
                    GetJobContactData(reader);
                }
            }
        }

        private async Task ReadJobTaskItemSheet(OleDbCommand oleExcelCommand)
        {
            using (DbDataReader reader = await oleExcelCommand.ExecuteReaderAsync())
            {
                while (await reader.ReadAsync())
                {
                    GetJobTaskItemData(reader);
                }

            }
        }

        private async Task ReadJobAssetSheet(OleDbCommand oleExcelCommand)
        {
            using (DbDataReader reader = await oleExcelCommand.ExecuteReaderAsync())
            {
                while (await reader.ReadAsync())
                {
                    GetJobAssetData(reader);
                }

            }
        }

        private async Task ReadJobNoteSheet(OleDbCommand oleExcelCommand)
        {
            using (DbDataReader reader = await oleExcelCommand.ExecuteReaderAsync())
            {
                while (await reader.ReadAsync())
                {
                    GetJobNoteData(reader);
                }

            }
        }

        private async Task ReadJobAttributeSheet(OleDbCommand oleExcelCommand)
        {
            using (DbDataReader reader = await oleExcelCommand.ExecuteReaderAsync())
            {
                while (await reader.ReadAsync())
                {
                    GetJobAttributeData(reader);
                }

            }
        }

        private async Task ReadJobSchedulingSheet(OleDbCommand oleExcelCommand)
        {
            using (DbDataReader reader = await oleExcelCommand.ExecuteReaderAsync())
            {
                while (await reader.ReadAsync())
                {
                    GetJobSchedulingData(reader);
                }

            }
        }

        private void GetAddressData(DbDataReader oleExcelReader)
        {
            try
            {
                if (oleExcelReader.IsDBNull(0)) return;

                if (!string.IsNullOrWhiteSpace(oleExcelReader.GetString(0)))
                {
                    if (oleExcelReader.GetString(0).Contains("Job Ref"))
                    {
                        Validator.JobAddressHeaderValidator jobAddressHeaderValidator = new Validator.JobAddressHeaderValidator();
                        _errorList.AddRange(jobAddressHeaderValidator.ValidateJobAddressColumnHeaders(oleExcelReader));
                        return;
                    }

                    JobAddressData address = new JobAddressData();
                    address.JobRef = oleExcelReader.IsDBNull(0) ? "" : oleExcelReader.GetString(0);
                    address.AddressDescription = oleExcelReader.IsDBNull(1) ? "" : oleExcelReader.GetString(1);
                    address.StreetUnit = oleExcelReader.IsDBNull(2) ? "" : oleExcelReader.GetString(2);
                    address.StreetNumber = oleExcelReader.IsDBNull(3) ? "" : oleExcelReader.GetString(3);
                    address.StreetName = oleExcelReader.IsDBNull(4) ? "" : oleExcelReader.GetString(4);
                    address.Suburb = oleExcelReader.IsDBNull(5) ? "" : oleExcelReader.GetString(5);
                    address.City = oleExcelReader.IsDBNull(6) ? "" : oleExcelReader.GetString(6);
                    address.Region = oleExcelReader.IsDBNull(7) ? "" : oleExcelReader.GetString(7);
                    address.lat = oleExcelReader.IsDBNull(8) ? "" : oleExcelReader.GetString(8);
                    address.lon = oleExcelReader.IsDBNull(9) ? "" : oleExcelReader.GetString(9);
                    address.isPrimaryAddress = oleExcelReader.IsDBNull(10) ? "" : oleExcelReader.GetString(10);


                    Validator.JobAddressContentValidator JobAddressContentValidator = new Validator.JobAddressContentValidator(address, listEnumRegions, _excelV2Content.ImportJobDataList);
                    JobAddressContentValidator.Validate();

                    _excelV2Content.JobAddressDataList.Add(address);
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
                _errorList.Add("Error Reading JobAddress data for Job Ref  " + (oleExcelReader.IsDBNull(0) ? "" : oleExcelReader.GetString(0)) + " Error Message:" + ex.Message);
            }
        }

        private void GetJobData(DbDataReader oleExcelReader)
        {
            try
            {
                if (oleExcelReader.IsDBNull(0)) return;

                if (!string.IsNullOrWhiteSpace(oleExcelReader.GetString(0)))
                {
                    if (oleExcelReader.GetString(0).Contains("FSP"))
                    {
                        Validator.JobDataHeaderValidator jobDataHeaderValidator = new Validator.JobDataHeaderValidator();
                        _errorList.AddRange(jobDataHeaderValidator.ValidateJobDataColumnHeaders(oleExcelReader));
                        return;
                    }

                    ImportJobData importJobData = new ImportJobData();
                    importJobData.FSP = _fsp;
                    importJobData.JobRef = oleExcelReader.IsDBNull(1) ? "" : oleExcelReader.GetString(1);
                    importJobData.WorkStreamName = oleExcelReader.IsDBNull(2) ? "" : oleExcelReader.GetString(2);
                    importJobData.JobDescription = oleExcelReader.IsDBNull(3) ? "" : oleExcelReader.GetString(3);
                    importJobData.Group1 = oleExcelReader.IsDBNull(4) ? "" : oleExcelReader.GetString(4);
                    importJobData.Group2 = oleExcelReader.IsDBNull(5) ? "" : oleExcelReader.GetString(5);
                    importJobData.Group3 = oleExcelReader.IsDBNull(6) ? "" : oleExcelReader.GetString(6);
                    importJobData.ConXJobReference = oleExcelReader.IsDBNull(7) ? "" : oleExcelReader.GetString(7);
                    importJobData.JobSeq = oleExcelReader.IsDBNull(8) ? "" : oleExcelReader.GetString(8);
                    importJobData.JobType = oleExcelReader.IsDBNull(9) ? "" : oleExcelReader.GetString(9);


                    Validator.JobDataContentValidator JobDataContentValidator = new Validator.JobDataContentValidator(importJobData, listJobTypes, listOfContracts);
                    JobDataContentValidator.Validate();

                    _excelV2Content.ImportJobDataList.Add(importJobData);

                }
            }
            catch (Exception ex)
            {
                LogException(ex);
                _errorList.Add("Error Reading Job data for Job Ref  " + (oleExcelReader.IsDBNull(1) ? "" : oleExcelReader.GetString(1)) + " Error Message:" + ex.Message);
            }
        }

        private void GetJobContactData(DbDataReader oleExcelReader)
        {
            try
            {
                if (oleExcelReader.IsDBNull(0)) return;

                if (!string.IsNullOrWhiteSpace(oleExcelReader.GetString(0)))
                {
                    if (oleExcelReader.GetString(0).Contains("Job Ref"))
                    {
                        Validator.JobContactHeaderValidator JobContactHeaderValidator = new Validator.JobContactHeaderValidator();
                        _errorList.AddRange(JobContactHeaderValidator.ValidateJobContactColumnHeaders(oleExcelReader));
                        return;
                    }

                    JobContactData importJobContactData = new JobContactData();
                    importJobContactData.JobRef = oleExcelReader.IsDBNull(0) ? "" : oleExcelReader.GetString(0);
                    importJobContactData.ContactDescription = oleExcelReader.IsDBNull(1) ? "" : oleExcelReader.GetString(1);
                    importJobContactData.FirstName = oleExcelReader.IsDBNull(2) ? "" : oleExcelReader.GetString(2);
                    importJobContactData.LastName = oleExcelReader.IsDBNull(3) ? "" : oleExcelReader.GetString(3);
                    importJobContactData.ContactType = oleExcelReader.IsDBNull(4) ? "" : oleExcelReader.GetString(4);
                    importJobContactData.ContactValue = oleExcelReader.IsDBNull(5) ? "" : oleExcelReader.GetString(5);

                    Validator.JobContactContentValidator JobContactContentValidator = new Validator.JobContactContentValidator(importJobContactData, _excelV2Content.ImportJobDataList, listEnumPhoneType);
                    JobContactContentValidator.Validate();
                    _excelV2Content.JobContactDataList.Add(importJobContactData);
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
                _errorList.Add("Error Reading Job Contact Data for Job Ref  " + (oleExcelReader.IsDBNull(0) ? "" : oleExcelReader.GetString(0)) + " Error Message:" + ex.Message);
            }
        }

        private void GetJobAssetData(DbDataReader oleExcelReader)
        {
            try
            {
                if (oleExcelReader.IsDBNull(0)) return;

                if (!string.IsNullOrWhiteSpace(oleExcelReader.GetString(0)))
                {
                    if (oleExcelReader.GetString(0).Contains("Job Ref"))
                    {
                        Validator.JobAssetHeaderValidator JobAssetHeaderValidator = new Validator.JobAssetHeaderValidator();
                        _errorList.AddRange(JobAssetHeaderValidator.ValidateJobAssetColumnHeaders(oleExcelReader));
                        return;
                    }

                    JobAssetData importJobAssetData = new JobAssetData();
                    importJobAssetData.JobRef = oleExcelReader.IsDBNull(0) ? "" : oleExcelReader.GetString(0);
                    importJobAssetData.AssetType = oleExcelReader.IsDBNull(1) ? "" : oleExcelReader.GetString(1);
                    importJobAssetData.AssetValue = oleExcelReader.IsDBNull(2) ? "" : oleExcelReader.GetString(2);
                    importJobAssetData.AssetAttribute = oleExcelReader.IsDBNull(3) ? "" : oleExcelReader.GetString(3);
                    importJobAssetData.AssetAttributeValue = oleExcelReader.IsDBNull(4) ? "" : oleExcelReader.GetString(4);
                    importJobAssetData.NewTaskNameInWorkflow = oleExcelReader.IsDBNull(5) ? "" : oleExcelReader.GetString(5);
                    importJobAssetData.ParentTaskName = oleExcelReader.IsDBNull(6) ? "" : oleExcelReader.GetString(6).Trim();
                    importJobAssetData.ParentAssetValue = oleExcelReader.IsDBNull(7) ? "" : oleExcelReader.GetString(7);
                    importJobAssetData.AppendAssetValueToNewTask = oleExcelReader.IsDBNull(8) ? "" : oleExcelReader.GetString(8);


                    Validator.JobAssetContentValidator JobAssetContentValidator = new Validator.JobAssetContentValidator(importJobAssetData, listWorkFlowData, _excelV2Content.ImportJobDataList, listAssetTypes);
                    JobAssetContentValidator.Validate();

                    _excelV2Content.JobAssetDataList.Add(importJobAssetData);
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
                _errorList.Add("Error Reading Job Asset Data for Job Ref  " + (oleExcelReader.IsDBNull(0) ? "" : oleExcelReader.GetString(0)) + " Error Message:" + ex.Message);
            }
        }

        private void GetJobTaskItemData(DbDataReader oleExcelReader)
        {
            try
            {
                if (oleExcelReader.IsDBNull(0)) return;

                if (!string.IsNullOrWhiteSpace(oleExcelReader.GetString(0)))
                {
                    if (oleExcelReader.GetString(0).Contains("Job Ref"))
                    {
                        Validator.JobTaskItemHeaderValidator JobTaskItemHeaderValidator = new Validator.JobTaskItemHeaderValidator();
                        _errorList.AddRange(JobTaskItemHeaderValidator.ValidateJobTaskItemColumnHeaders(oleExcelReader));
                        return;
                    }

                    JobTaskItemData importJobTaskItemData = new JobTaskItemData();
                    importJobTaskItemData.JobRef = oleExcelReader.IsDBNull(0) ? "" : oleExcelReader.GetString(0);
                    importJobTaskItemData.TaskName = oleExcelReader.IsDBNull(1) ? "" : oleExcelReader.GetString(1).Trim();
                    importJobTaskItemData.AssetValue = oleExcelReader.IsDBNull(2) ? "" : oleExcelReader.GetString(2);
                    importJobTaskItemData.TaskItemOrder = oleExcelReader.IsDBNull(3) ? "" : oleExcelReader.GetString(3);
                    importJobTaskItemData.TaskItemValue = oleExcelReader.IsDBNull(4) ? "" : oleExcelReader.GetString(4);
                    importJobTaskItemData.TaskItemFormat = oleExcelReader.IsDBNull(5) ? "" : oleExcelReader.GetString(5);
                    importJobTaskItemData.ParentTaskName = oleExcelReader.IsDBNull(6) ? "" : oleExcelReader.GetString(6).Trim();
                    importJobTaskItemData.ParentAssetValue = oleExcelReader.IsDBNull(7) ? "" : oleExcelReader.GetString(7);


                    Validator.JobTaskItemContentValidator JobTaskItemContentValidator = new Validator.JobTaskItemContentValidator(importJobTaskItemData, _excelV2Content.ImportJobDataList, listWorkFlowData, _excelV2Content.JobAssetDataList);
                    JobTaskItemContentValidator.Validate();

                    _excelV2Content.JobTaskItemDataList.Add(importJobTaskItemData);
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
                _errorList.Add("Error Reading JobTaskItem Data for Job Ref  " + (oleExcelReader.IsDBNull(0) ? "" : oleExcelReader.GetString(0)) + " Error Message:" + ex.Message);

            }
        }

        private void GetJobNoteData(DbDataReader oleExcelReader)
        {
            try
            {
                if (oleExcelReader.IsDBNull(0)) return;

                if (!string.IsNullOrWhiteSpace(oleExcelReader.GetString(0)))
                {
                    if (oleExcelReader.GetString(0).Contains("Job Ref"))
                    {
                        Validator.JobNoteHeaderValidator JobNoteHeaderValidator = new Validator.JobNoteHeaderValidator();
                        _errorList.AddRange(JobNoteHeaderValidator.ValidateJobNoteColumnHeaders(oleExcelReader));
                        return;
                    }

                    JobNoteData importJobNoteData = new JobNoteData();
                    importJobNoteData.JobRef = oleExcelReader.IsDBNull(0) ? "" : oleExcelReader.GetString(0);
                    importJobNoteData.NoteType = oleExcelReader.IsDBNull(1) ? "" : oleExcelReader.GetString(1);
                    importJobNoteData.Note = oleExcelReader.IsDBNull(2) ? "" : oleExcelReader.GetString(2);
                    importJobNoteData.NoteTaskName = oleExcelReader.IsDBNull(3) ? "" : oleExcelReader.GetString(3).Trim();
                    importJobNoteData.NoteAssetValue = oleExcelReader.IsDBNull(4) ? "" : oleExcelReader.GetString(4);
                    importJobNoteData.ParentTaskName = oleExcelReader.IsDBNull(5) ? "" : oleExcelReader.GetString(5).Trim();
                    importJobNoteData.ParentAssetValue = oleExcelReader.IsDBNull(6) ? "" : oleExcelReader.GetString(6);


                    Validator.JobNoteContentValidator JobNoteContentValidator = new Validator.JobNoteContentValidator(importJobNoteData, _excelV2Content.ImportJobDataList, listEnumNoteType, listWorkFlowData, _excelV2Content.JobAssetDataList);
                    JobNoteContentValidator.Validate();
                    _excelV2Content.JobNoteDataList.Add(importJobNoteData);
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
                _errorList.Add("Error Reading Job Note Data for Job Ref  " + (oleExcelReader.IsDBNull(0) ? "" : oleExcelReader.GetString(0)) + " Error Message:" + ex.Message);
            }
        }

        private void GetJobAttributeData(DbDataReader oleExcelReader)
        {
            try
            {
                if (oleExcelReader.IsDBNull(0)) return;

                if (!string.IsNullOrWhiteSpace(oleExcelReader.GetString(0)))
                {
                    if (oleExcelReader.GetString(0).Contains("Job Ref"))
                    {
                        Validator.JobAttributeHeaderValidator JobAttributeHeaderValidator = new Validator.JobAttributeHeaderValidator();
                        _errorList.AddRange(JobAttributeHeaderValidator.ValidateJobAttributeColumnHeaders(oleExcelReader));
                        return;
                    }

                    JobAttributeData importJobAttributeData = new JobAttributeData();
                    importJobAttributeData.JobRef = oleExcelReader.IsDBNull(0) ? "" : oleExcelReader.GetString(0);
                    importJobAttributeData.AttributeType = oleExcelReader.IsDBNull(1) ? "" : oleExcelReader.GetString(1);
                    importJobAttributeData.AttributeValue = oleExcelReader.IsDBNull(2) ? "" : oleExcelReader.GetString(2);

                    Validator.JobAttributeContentValidator JobAttributeContentValidator = new Validator.JobAttributeContentValidator(importJobAttributeData, _excelV2Content.ImportJobDataList);
                    JobAttributeContentValidator.Validate();

                    _excelV2Content.JobAttributeDataList.Add(importJobAttributeData);
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
                _errorList.Add("Error Reading Job Attribute Data for Job Ref  " + (oleExcelReader.IsDBNull(0) ? "" : oleExcelReader.GetString(0)) + " Error Message:" + ex.Message);
            }
        }

        private void GetJobSchedulingData(DbDataReader oleExcelReader)
        {
            try
            {
                if (oleExcelReader.IsDBNull(0)) return;

                if (!string.IsNullOrWhiteSpace(oleExcelReader.GetString(0)))
                {
                    if (oleExcelReader.GetString(0).Contains("Job Ref"))
                    {
                        Validator.JobSchedulingHeaderValidator JobSchedulingHeaderValidator = new Validator.JobSchedulingHeaderValidator();
                        _errorList.AddRange(JobSchedulingHeaderValidator.ValidateJobSchedulingColumnHeaders(oleExcelReader));
                        return;
                    }

                    JobSchedulingData importJobSchedulingData = new JobSchedulingData();
                    importJobSchedulingData.JobRef = oleExcelReader.IsDBNull(0) ? "" : oleExcelReader.GetString(0);
                    importJobSchedulingData.PlannedFromDate = oleExcelReader.IsDBNull(1) ? "" : oleExcelReader.GetString(1);

                    if (importJobSchedulingData.PlannedFromDate.Length > 0)
                        if (!(
                            importJobSchedulingData.PlannedFromDate.Contains("/") ||
                            importJobSchedulingData.PlannedFromDate.Contains("-") ||
                            importJobSchedulingData.PlannedFromDate.Contains(":")
                            ))
                        {
                            double db = 0;
                            if (double.TryParse(importJobSchedulingData.PlannedFromDate, out db))
                            {
                                importJobSchedulingData.PlannedFromDate = DateTime.FromOADate(db).ToString("dd/MM/yyyy");
                            }
                        }


                    importJobSchedulingData.PlannedFromTime = oleExcelReader.IsDBNull(2) ? "" : oleExcelReader.GetString(2);
                    importJobSchedulingData.PlannedToDate = oleExcelReader.IsDBNull(3) ? "" : oleExcelReader.GetString(3); //.ToString("yyyy-MM-dd"); ;

                    if (importJobSchedulingData.PlannedToDate.Length > 0)
                        if (!(
                            importJobSchedulingData.PlannedToDate.Contains("/") ||
                            importJobSchedulingData.PlannedToDate.Contains("-") ||
                            importJobSchedulingData.PlannedToDate.Contains(":")
                            ))
                        {
                            double db = 0;
                            if (double.TryParse(importJobSchedulingData.PlannedFromDate, out db))
                            {
                                importJobSchedulingData.PlannedToDate = DateTime.FromOADate(db).ToString("dd/MM/yyyy");
                            }
                        }

                    importJobSchedulingData.PlannedToTime = oleExcelReader.IsDBNull(4) ? "" : oleExcelReader.GetString(4);
                    importJobSchedulingData.AppointmentNeeded = oleExcelReader.IsDBNull(5) ? "" : oleExcelReader.GetString(5);
                    importJobSchedulingData.PreferredUsername = oleExcelReader.IsDBNull(6) ? "" : oleExcelReader.GetString(6);
                    importJobSchedulingData.Frequency = oleExcelReader.IsDBNull(7) ? "" : oleExcelReader.GetString(7);
                    importJobSchedulingData.NumberOfOccurrences = oleExcelReader.IsDBNull(8) ? "" : oleExcelReader.GetString(8);
                    importJobSchedulingData.AssignToDevice = oleExcelReader.IsDBNull(9) ? "" : oleExcelReader.GetString(9);
                    importJobSchedulingData.ContractModel = oleExcelReader.IsDBNull(10) ? "" : oleExcelReader.GetString(10);


                    Validator.JobSchedulingContentValidator JobSchedulingContentValidator = new Validator.JobSchedulingContentValidator(importJobSchedulingData, _excelV2Content.ImportJobDataList, listOfUserNames);
                    JobSchedulingContentValidator.Validate();

                    _excelV2Content.JobSchedulingDataList.Add(importJobSchedulingData);
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
                _errorList.Add("Error Reading Job Scheduling Data for Job Ref  " + (oleExcelReader.IsDBNull(0) ? "" : oleExcelReader.GetString(0)) + " Error Message:" + ex.Message);
            }
        }


    }

    #region Classes

    public class AssetTypes
    {
        public float enumAssetType { get; set; }
        public string enumDesc { get; set; }
        public string enumSDesc { get; set; }

        public AssetTypes()
        {
            this.enumDesc = "";
            this.enumSDesc = "";
        }
    }


    public class EnumDayPart
    {
        public float enumDayPart { get; set; }
        public string enumDesc { get; set; }

        public EnumDayPart(float _enumDayPart, string _enumDesc)
        {
            enumDayPart = _enumDayPart;
            enumDesc = _enumDesc;
        }
    }

    public class EnumPhoneType
    {
        public float enumPhoneType { get; set; }
        public string enumDesc { get; set; }

        public EnumPhoneType(float _enumPhoneType, string _enumDesc)
        {
            this.enumPhoneType = _enumPhoneType;
            this.enumDesc = _enumDesc;
        }
    }

    public class WorkFlowData
    {
        public string WorkStream { get; set; }
        public string WorkFlowName { get; set; }
        public string TaskName { get; set; }
        public string ParentTaskName { get; set; }

        public WorkFlowData(string workstream, string workflowname, string taskname)
        {
            this.WorkStream = workstream;
            this.WorkFlowName = workflowname;
            this.TaskName = taskname;
            this.ParentTaskName = "";
        }

        public WorkFlowData(string workstream, string workflowname, string taskname, string parentTaskName)
        {
            this.WorkStream = workstream;
            this.WorkFlowName = workflowname;
            this.TaskName = taskname;
            this.ParentTaskName = parentTaskName;
        }

    }



    #endregion
}

