﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReader.ImportV2
{
    public class SheetName
    {
        public const string JobData = "Job Data";
        public const string JobContacts = "Job Contacts";
        public const string JobAddresses = "Job Addresses";
        public const string JobNotes = "Job Notes";
        public const string JobAssets = "Job Assets";
        public const string JobAttributes = "Job Attributes";
        public const string JobScheduling = "Job Scheduling";
        public const string JobTaskItems = "Job Task Items";

        public const string JobDataUpper = "JOB DATA";
        public const string JobContactsUpper = "JOB CONTACTS";
        public const string JobAddressesUpper = "JOB ADDRESSES";
        public const string JobNotesUpper = "JOB NOTES";
        public const string JobAssetsUpper = "JOB ASSETS";
        public const string JobAttributesUpper = "JOB ATTRIBUTES";
        public const string JobSchedulingUpper = "JOB SCHEDULING";
        public const string JobTaskItemsUpper = "JOB TASK ITEMS";

    }
}
