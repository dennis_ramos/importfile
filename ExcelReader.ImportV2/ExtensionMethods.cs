﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelReader.ImportV2
{
    public static class ExtensionMethods
    {
        public static DateTime? ToDateValue(this string dateString)
        {
            CultureInfo provider = new CultureInfo("en-NZ");
            try
            {
                return DateTime.Parse(dateString, provider);
            }
            catch
            {
                try
                {
                    return DateTime.ParseExact(dateString, "dd/MM/yyyy HH:mm:ss", provider);
                }
                catch
                {
                    return null;
                }
            }

        }

        public static int ToIntValue(this string intString)
        {
            int returnInt = 0;
            int.TryParse(intString, out returnInt);

            return returnInt; 
        }
    }
}
