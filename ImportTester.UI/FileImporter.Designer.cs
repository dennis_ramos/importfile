﻿namespace ImportTester.UI
{
    partial class FileImporter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.FSPComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.FileTextBox = new System.Windows.Forms.TextBox();
            this.ProcessButton = new System.Windows.Forms.Button();
            this.ImportOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.SearchButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "FSP:";
            // 
            // FSPComboBox
            // 
            this.FSPComboBox.FormattingEnabled = true;
            this.FSPComboBox.Items.AddRange(new object[] {
            "CITYPARKS",
            "RECSERV",
            "REGRESSION",
            "SPENSHAW",
            "TENIX",
            "WELLS"});
            this.FSPComboBox.Location = new System.Drawing.Point(88, 22);
            this.FSPComboBox.Name = "FSPComboBox";
            this.FSPComboBox.Size = new System.Drawing.Size(187, 21);
            this.FSPComboBox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "File Location:";
            // 
            // FileTextBox
            // 
            this.FileTextBox.Location = new System.Drawing.Point(89, 59);
            this.FileTextBox.Name = "FileTextBox";
            this.FileTextBox.ReadOnly = true;
            this.FileTextBox.Size = new System.Drawing.Size(451, 20);
            this.FileTextBox.TabIndex = 3;
            // 
            // ProcessButton
            // 
            this.ProcessButton.Enabled = false;
            this.ProcessButton.Location = new System.Drawing.Point(507, 115);
            this.ProcessButton.Name = "ProcessButton";
            this.ProcessButton.Size = new System.Drawing.Size(75, 23);
            this.ProcessButton.TabIndex = 4;
            this.ProcessButton.Text = "Process";
            this.ProcessButton.UseVisualStyleBackColor = true;
            this.ProcessButton.Click += new System.EventHandler(this.ProcessButton_Click);
            // 
            // ImportOpenFileDialog
            // 
            this.ImportOpenFileDialog.FileName = "openFileDialog1";
            this.ImportOpenFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // SearchButton
            // 
            this.SearchButton.Location = new System.Drawing.Point(546, 57);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(35, 23);
            this.SearchButton.TabIndex = 5;
            this.SearchButton.Text = "...";
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // FileImporter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(594, 150);
            this.Controls.Add(this.SearchButton);
            this.Controls.Add(this.ProcessButton);
            this.Controls.Add(this.FileTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.FSPComboBox);
            this.Controls.Add(this.label1);
            this.Name = "FileImporter";
            this.Text = "Import File Tester";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox FSPComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox FileTextBox;
        private System.Windows.Forms.Button ProcessButton;
        private System.Windows.Forms.OpenFileDialog ImportOpenFileDialog;
        private System.Windows.Forms.Button SearchButton;
    }
}

