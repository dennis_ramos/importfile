﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using r = ConXApiDtoModels.DataCreationRequest;
using ImportV2.ApiMapper;
using ConXApiRepository;

namespace ImportTester.UI
{
    public partial class FileImporter : Form
    {
        public FileImporter()
        {
            InitializeComponent();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            ImportOpenFileDialog.Filter = "Excel File (.xls)|*.xls";
            ImportOpenFileDialog.FilterIndex = 1;
            ImportOpenFileDialog.Multiselect = false;
            var result = ImportOpenFileDialog.ShowDialog();

            if( result == DialogResult.OK )
            {
                FileTextBox.Text = ImportOpenFileDialog.FileName;
                if( !( string.IsNullOrWhiteSpace(FSPComboBox.Text) || string.IsNullOrWhiteSpace(FileTextBox.Text) ) )
                {
                    this.ProcessButton.Enabled = true;
                }


            }

        }

        private async Task ProcessFile(string filePath)
        {
            var mapper = new Mapper(filePath, this.FSPComboBox.Text, "dramos", "emerge.developers@e-mergedata.com");
            var success = await mapper.ProcessFile().ConfigureAwait(false);
        }

        private void ProcessButton_Click(object sender, EventArgs e)
        {

            ProcessButton.Enabled = false;
            ProcessFile(ImportOpenFileDialog.FileName).Wait();
            ProcessButton.Enabled = true;
        }
    }
}
